#!/usr/bin/env python

import rospy, sys
from naoqi import ALProxy, ALModule, ALBroker
from optparse import OptionParser
NAO_IP = "127.0.0.1"
memory = None
postureProxy = None
FallManager = None

class FallManagerModule(ALModule):

    def __init__(self, name):
	global memory, postureProxy

	ALModule.__init__(self,name)
	
	try:
	    memory = ALProxy('ALMemory', '127.0.0.1', 9559)
	    postureProxy = ALProxy('ALRobotPosture', '127.0.0.1', 9559)
	except Exception, e:
	    print "Could not create proxy ", e
	    sys.exit(1)
	memory.subscribeToEvent("robotHasFallen", "FallManager", "onFall")

    def onFall(self, *_args):
	memory.unsubscribeToEvent("robotHasFallen", "FallManager")
	postureProxy.goToPosture("StandInit", 1.0)
	memory.subscribeToEvent("robotHasFallen", "FallManager", "onFall")
	
def main():
    rospy.init_node('nao_fall_stand')

    parser = OptionParser()
    parser.add_option("--pip",
	help="Parent broker port. The IP address or your robot",
	dest="pip")
    parser.add_option("--pport",
	help="Parent broker port. The port NAOqi is listening to",
	dest="pport",
	type="int")
    parser.set_defaults(
	pip=NAO_IP,
	pport=9559)

    (opts, args_) = parser.parse_args()
    pip   = opts.pip
    pport = opts.pport

    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    myBroker = ALBroker("myBroker",
	"0.0.0.0",   # listen to anyone
	0,           # find a free port and use it
	pip,         # parent broker IP
	pport)       # parent broker port

    global FallManager
    FallManager = FallManagerModule("FallManager")
    rospy.spin()

if __name__ == "__main__":
    main()

