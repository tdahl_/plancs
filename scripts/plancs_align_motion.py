#!/usr/bin/env python
import rospy, time, plancs, sys, os
from plancs.msg import ObjectDimensions
from sensor_msgs.msg import JointState
from std_srvs.srv import Empty
from plancs import *

motionProxy = PALProxy("ALMotion")

def align():
    global ball, goal, goalTimeStamp, joints, ballTimeStamp
    goalTimeStamp = 0.0
    ballTimeStamp = 0.0
    ball = None
    goal = None
    moving = False
    sub_names = ["plancs_align_trigger", "opponent_goal", "/joint_states"]
    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(sub_names)):
	    sub_names[x] = sub_names[x] + "_" + argv[1]
    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(sub_names[0], ObjectDimensions, ballData)
    mynode.add_subscriber(sub_names[1], ObjectDimensions, opponentGoalData)
    mynode.add_subscriber(sub_names[2], JointState, jointStates)
    motionProxy.setStiffnesses("Body", 1.0)
    while not rospy.is_shutdown():
        if time.time() > goalTimeStamp + 3:
           goal = None
        if time.time() > ballTimeStamp + 3:
           ball = None
        if ball != None:
            if ball.x != 0.0:
                if goal == None:
                    if ball.y < 140:
                        motionProxy.move(0.02, 0.0, 0.0)
                        mynode.sleep(0.1)
                    else:
                        # Look up and straighten head.
                        motionProxy.setAngles(["HeadYaw"], [0.0], 1)
                        motionProxy.setAngles(["HeadPitch"], [0.0], 1)
                        if goal == None:
                            motionProxy.move(0.0, -1.00, 0.355)
                            # Sleep 3 seconds
                            rospy.sleep(3)
                            # Stop moving
                            motionProxy.killMove()
                            # Look down
                            motionProxy.setAngles(["HeadPitch"], [0.5149], 1)

                if goal != None:
                    if ball.y < 160:
                        motionProxy.move(0.02, 0.0, 0.0)
                        mynode.sleep(0.1)
                    else:
                        # Look down
                        motionProxy.setAngles(["HeadPitch"], [0.5149], 1)
                        if ball.y >= 160:
                            ballMidPoint = (ball.x + (ball.width/2))
                            if ballMidPoint < 80.0:    # If ball is left.
                                motionProxy.move(0.0, 0.2, 0.0)  # positive is left
                            elif ballMidPoint > 230.0:    
                                motionProxy.move(0.0, -0.2, 0.0)  # If ball is right.
                            elif ballMidPoint > 145.0 and ballMidPoint < 165.0:    # If ball is between the legs.
                                motionProxy.move(0.0, 0.2, 0.0)
                            else:
                                motionProxy.killMove()

                        # Look up and straighten head.
                        motionProxy.setAngles(["HeadYaw"], [0.0], 1)
                        motionProxy.setAngles(["HeadPitch"], [0.0], 1)
                        mynode.sleep(0.1)
        rospy.sleep(0.1)

def jointStates(jointValues):
    global joints
    joints = jointValues

def ballData(bl):
    global ball, ballTimeStamp
    ballTimeStamp = time.time()
    ball = bl

def opponentGoalData(gl):
    global goal, goalTimeStamp
    goalTimeStamp = time.time()
    goal = gl

if __name__ == '__main__':
    align()
