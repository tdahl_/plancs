#!/usr/bin/env python

import cv, time, sys, rospy, os, plancs
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import *
from plancs.msg import ObjectDimensions

bridge = CvBridge()
imageData = None

def get_image():
    global imageData, done, count
    count = 1
    done = False

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["nao_top_camera", "top_vision_ball_dimensions", "goal_dimensions", "waistband_dimensions", "nao_top_camera_overlay"]
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >=2:
        name = name + "_" + argv[1]
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    sub = mynode.add_subscriber(topic_names[0], Image, callback)
    ballOD = ObjectDimensions()
    goalOD = ObjectDimensions()
    waistbandOD = ObjectDimensions()
    ballPub = rospy.Publisher(topic_names[1], ObjectDimensions)
    goalPub = rospy.Publisher(topic_names[2], ObjectDimensions)
    waistbandPub = rospy.Publisher(topic_names[3], ObjectDimensions)
    overlayImgPub = rospy.Publisher(topic_names[4], Image)
    while not rospy.is_shutdown():
	if imageData != None:
	    if done == False:
		"""By adding a step to skip searching pixels in the for 
		loops we can speed up the function. The search area can be narrowed
		down by adjusting the range in the for loops. img[width, height] e.g: img[640, 480]"""
		#b = time.clock()   #------Function speed test
		img = bridge.imgmsg_to_cv(imageData, "bgr8")
	    	(cols, rows) = cv.GetSize(img)
		ballxMin = cols
		ballxMax = 0.0
		ballyMin = rows
		ballyMax = 0.0
		topLeftGoalx = cols
		topLeftGoaly = rows
		bottomRightGoalx = 0.0
		bottomRightGoaly = 0.0
		waistbandxMin = cols
		waistbandyMin = rows
		waistbandxMax = 0.0
		waistbandyMax = 0.0
		previousY = 0.0
                previousxl = 0.0
                previousxr = cols
                leftPost = []
                rightPost = []
		for y in xrange(0, rows-1):		# Add a third parameter to skip pixels. EG (0, rows-1, 2)
		    for x in xrange(0, cols-1):		# Remove the third parameter to return to full scan.

			# Search for the ball
			#--------------------

			if img[y, x][0] >= 0.0 and img[y, x][0] <= 80.0:
			    if img[y, x][1] >= 80.0 and img[y, x][1] <= 170.0:
				if img[y, x][2] >= 210.0 and img[y, x][2] <= 255.0:
				    if x < ballxMin: ballxMin = x
				    if x > ballxMax: ballxMax = x
				    if y < ballyMin: ballyMin = y
				    if y > ballyMax: ballyMax = y
                                    img[y, x] = [0.0, 0.0, 0.0]
					
			# Search for the goals and goal posts
			#------------------------------------

			if img[y, x][0] >= 75.0 and img[y, x][0] <= 150.0:
			    if img[y, x][1] >= 160.0 and img[y, x][1] <= 255.0:
				if img[y, x][2] >= 180.0 and img[y, x][2] <= 255.0:
				    if previousY == 0.0: previousY = y
				    if y < (previousY+5):
                                        if x < topLeftGoalx + 5.0 and x > topLeftGoalx: 
                                            if previousxl > x + 10: 
                                                leftPost = []
                                            leftPost.append(y)
                                            previousxl = x
					if x < topLeftGoalx: topLeftGoalx = x
					if y < topLeftGoaly: topLeftGoaly = y
                                        if x > bottomRightGoalx - 5.0 and x < bottomRightGoalx: 
                                            if x > previousxr + 10:    
                                                rightPost = []
                                            rightPost.append(y)
                                            previousxr = x
					if x > bottomRightGoalx: bottomRightGoalx = x
				       	if y > bottomRightGoaly: bottomRightGoaly = y
					previousY = y
                                        img[y, x] = [0.0, 0.0, 255.0]
			
			# Search for the waistbands
			#--------------------------

			if img[y, x][0] >= 160.0 and img[y, x][0] <= 255.0:
			    if img[y, x][1] >= 0.0 and img[y, x][1] <= 70.0:
				if img[y, x][2] >= 0.0 and img[y, x][2] <= 70.0:
				    if x < waistbandxMin: waistbandxMin = x
				    if x > waistbandxMax: waistbandxMax = x
				    if y < waistbandyMin: waistbandyMin = y
				    if y > waistbandyMax: waistbandyMax = y
                                    img[y, x] = [0.0, 255.0, 0.0]

		ballOD.height = ballyMax - ballyMin
		ballOD.width = ballxMax - ballxMin
		ballOD.x = ballxMin
		ballOD.y = ballyMin
		if ballOD.height < 0.0: ballOD.height = 0.0
		if ballOD.width < 0.0: ballOD.width = 0.0
		if ballOD.x == cols: ballOD.x = 0.0
		if ballOD.y == rows: ballOD.y = 0.0

		goalOD.width = bottomRightGoalx - topLeftGoalx
		goalOD.height = bottomRightGoaly - topLeftGoaly
		goalOD.x = topLeftGoalx
		goalOD.y = topLeftGoaly
		if goalOD.width < 0.0: goalOD.width = 0.0
		if goalOD.height < 0.0: goalOD.height = 0.0
		if goalOD.x == cols: goalOD.x = 0.0
		if goalOD.y == rows: goalOD.y = 0.0
                if previousxr - previousxl > 50:    # The distance between the posts has to be over 50 pixels otherwise it is not the posts.
                    if len(leftPost) > 1:
                        leftPostHeight = leftPost[len(leftPost) - 1] - leftPost[0]
                        if leftPostHeight >= 35:       # Post should be higher than 35 pixels at the furtherest distance.
                            goalOD.LPH = leftPostHeight
                    if len(rightPost) > 1:
                        rightPostHeight = rightPost[len(rightPost) - 1] - rightPost[0]
                        if rightPostHeight >= 35:      # Post should be higher than 35 pixels at the furtherest distance.
                            goalOD.RPH = rightPostHeight

		waistbandOD.height = waistbandyMax - waistbandyMin
		waistbandOD.width = waistbandxMax - waistbandxMin
		waistbandOD.x = waistbandxMin
		waistbandOD.y = waistbandyMin
		if waistbandOD.x == cols: waistbandOD.x = 0.0
		if waistbandOD.y == rows: waistbandOD.y = 0.0
		if waistbandOD.width < 0.0: waistbandOD.width = 0.0
		if waistbandOD.height < 0.0: waistbandOD.height = 0.0

		print ("Ball x: %.3f, Ball y: %.3f, Ball width: %.3f, Ball height: %.3f" % (ballOD.x, ballOD.y, ballOD.width, ballOD.height))
		print ("Goal x: %.3f, Goal y: %.3f, Goal width: %.3f, Goal height: %.3f, Goal left post height: %.3f, Goal right post height: %.3f" % (goalOD.x, goalOD.y, goalOD.width, goalOD.height, goalOD.LPH, goalOD.RPH))
		print ("Waistband x: %.3f, Waistband y: %.3f, Waistband width: %.3f, Waistband height %.3f" % (waistbandOD.x, waistbandOD.y, waistbandOD.width, waistbandOD.height))
		print
		ballPub.publish(ballOD)
		goalPub.publish(goalOD)
		waistbandPub.publish(waistbandOD)
                overlayImg = bridge.cv_to_imgmsg(img, "bgr8")
                overlayImgPub.publish(overlayImg)
		#a = time.clock()   #------Function speed test
		#print a-b	    #------Function speed test
		done = True

def callback(data):
    global imageData, done, count
    if count == 1:
	imageData = data
        count += 1
    if done == True:
	imageData = data
	done = False

if __name__ == '__main__':
    get_image()
