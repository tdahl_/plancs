#!/usr/bin/env python

import rospy, plancs, time, sys, os
from plancs.msg import ObjectDimensions
from sensor_msgs.msg import JointState
from std_msgs.msg import Empty

def get_values():
    global ball, neck, init, ballTimeStamp
    ball = None
    neck = None
    ballDone = True

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ['bottom_vision_ball_dimensions', 'joint_states', 'plancs_align_trigger', ]
    inhibit_names = ['plancs_approach_left_trigger', 'plancs_approach_center_trigger', 'plancs_approach_right_trigger', 'plancs_search_trigger']
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "_" + argv[1] + "/plancs_inhibit"
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]
    else:
        for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "/plancs_inhibit"

    ballTimeStamp = time.time()
    mynode = plancs.Plancs_Node(name, quiet=True)
    mynode.set_decay_period(1.0)
    bottomBallSub = mynode.add_subscriber(topic_names[0], ObjectDimensions, getBallData)
    neckStateSub = mynode.add_subscriber(topic_names[1], JointState, getNeckData)
    ballPub = rospy.Publisher(topic_names[2], ObjectDimensions)
    inhibLeft = rospy.Publisher(inhibit_names[0], Empty)
    inhibCenter = rospy.Publisher(inhibit_names[1], Empty)
    inhibRight = rospy.Publisher(inhibit_names[2], Empty)
    inhibSearch = rospy.Publisher(inhibit_names[3], Empty)

    while not rospy.is_shutdown():
        if time.time() > ballTimeStamp + 3:
           ball = None
	if neck != None:
            if ball != None: 
	        if neck.position[1] >= 0.3 and neck.position[0] < 0.01 and neck.position[0] > -0.01:
		    if ball.y >= 50.0:
		        # Ball is in position
		    	ballPub.publish(ball)
			inhibLeft.publish()
			inhibCenter.publish()
			inhibRight.publish()
			inhibSearch.publish()
                        mynode.sleep(0.2)
        rospy.sleep(0.1)

def getBallData(bl):
    global ball, ballTimeStamp
    ball = bl
    ballTimeStamp = time.time()

def getNeckData(nk):
    global neck
    neck = nk

if __name__ == '__main__':
    get_values()
