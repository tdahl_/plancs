#!/usr/bin/env python

import rospy, time, plancs, os, sys
import motion, time, math
#from naoqi import ALProxy
from std_msgs.msg import Bool
from std_srvs.srv import Empty

from plancs.srv import SwbEnableBalanceConstraint, SwbGoToBalance, SwbEnable, SwbFootState, SpositionInterpolation, \
						 SwbEnableEffectorOptimization, SgoToPosture

def main():
    global kickState

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["plancs_kick_left_trigger"]
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(topic_names[0], Bool, kickLeft)
    kickState = False
    while not rospy.is_shutdown():
        if kickState:
	    setStiffnesses = rospy.ServiceProxy("body_stiffness/enable", Empty)
	    goToPosture = rospy.ServiceProxy("nao_goToPosture", SgoToPosture)
	    wbEnable = rospy.ServiceProxy("nao_wbEnable", SwbEnable)
	    wbFootState = rospy.ServiceProxy("nao_wbFootState", SwbFootState)
	    wbEnableBalanceConstraint = rospy.ServiceProxy("nao_wbEnableBalanceConstraint", SwbEnableBalanceConstraint)
	    wbGoToBalance = rospy.ServiceProxy("nao_wbGoToBalance", SwbGoToBalance)
	    positionInterpolation = rospy.ServiceProxy("nao_positionInterpolation", SpositionInterpolation)
	    wbEnableEffectorOptimization = rospy.ServiceProxy("nao_wbEnableEffectorOptimization", SwbEnableEffectorOptimization)

	    #motionProxy.setStiffnesses("Body", 1.0)
	    setStiffnesses()

	    #postureProxy.goToPosture("StandInit", 0.5)
	    goToPosture("StandInit", 0.5)

	    # Activate Whole Body Balancer
	    isEnabled  = True
	    #motionProxy.wbEnable(isEnabled)
	    wbEnable(isEnabled)

	    # Legs are constrained fixed
	    stateName  = "Fixed"
	    supportLeg = "Legs"
	    #motionProxy.wbFootState(stateName, supportLeg)
	    wbFootState(stateName, supportLeg)

	    # Constraint Balance Motion
	    isEnable   = True
	    supportLeg = "Legs"
	    #motionProxy.wbEnableBalanceConstraint(isEnable, supportLeg)
	    wbEnableBalanceConstraint(isEnable, supportLeg)

	    # Com go to RLeg
	    supportLeg = "RLeg"
	    duration   = 2.0					# (DY edit. Original 2.0)
	    #motionProxy.wbGoToBalance(supportLeg, duration)
	    wbGoToBalance(supportLeg, duration)

	    # LLeg is free
	    stateName  = "Free"
	    supportLeg = "LLeg"
	    #motionProxy.wbFootState(stateName, supportLeg)
	    wbFootState(stateName, supportLeg)

	    # LLeg is optimized
	    effectorName = "LLeg"
	    axisMask	 = 63
	    space = motion.SPACE_NAO

	    # Motion of the LLeg
	    dx	  = 0.05				 # translation axis X (meters) 	(DY edit. Original: 0.05)
	    dz	  = 0.05				  # translation axis Z (meters) 	(DY edit. Original: 0.05)
	    dwy	 = 5.0*math.pi/180.0	# rotation axis Y (radian)		(DY edit. Original: 5.0*math.pi/180.0)
	    times   = [1.0, 1.8, 2.5]
	    isAbsolute = False		

	    targetList = [
	    [-dx, 0.0, dz, 0.0, +dwy, 0.0],
	    [+dx, 0.0, dz, 0.0, 0.0, 0.0],
	    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]
            mynewthing = str(times)
            path = str(targetList)
	    #motionProxy.positionInterpolation(effectorName, space, targetList, axisMask, times, isAbsolute)
	    positionInterpolation(effectorName, space, path, axisMask, mynewthing, isAbsolute)

	    # Example showing how to Enable Effector Control as an Optimization
	    isActive	 = False
	    #motionProxy.wbEnableEffectorOptimization(effectorName, isActive)
	    wbEnableEffectorOptimization(effectorName, isActive)
	    time.sleep(0.2)			# (DY edit. Original: 1.0)

	    # Deactivate Head tracking
	    isEnabled	= False
	    #motionProxy.wbEnable(isEnabled)
	    wbEnable(isEnabled)

	    # send robot to Pose Init
	    #postureProxy.goToPosture("StandInit", 0.5)
	    goToPosture("StandInit", 0.5)
            kickState = False
        rospy.sleep(0.1)

def kickLeft(boole):
    global kickState
    kickState = boole.data

if __name__ == "__main__":
    main()
