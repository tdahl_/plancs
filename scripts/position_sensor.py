#!/usr/bin/env python

import sys, rospy, plancs, os
from plancs.msg import ObjectDimensions
from std_msgs.msg import String



def getData():
    global leftPost, rightPost, init, done
    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    subName = "opponent_goal"
    pubName = "position_sensor"
    argv = rospy.myargv(argv=sys.argv)

    if len(argv) >= 2:
	pubName = pubName + "_" + argv[1]
	name = name + "_" + argv[1]
	subName = subName + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    posPub = rospy.Publisher(pubName, String)
    sub = mynode.add_subscriber(subName, ObjectDimensions, callback)

    leftPost = None
    rightPost = None
    position = None
    init = True
    done = False
    while not rospy.is_shutdown():
	if leftPost != None and rightPost != None:
	    if done == False:
		
		if leftPost > rightPost:
		    leftDiff = leftPost - rightPost
		    if leftPost <= 48 and leftDiff > 2:
			position = "A1"
		    elif leftPost > 48 and leftPost <= 70 and leftDiff > 5:
			position = "A2"
		    elif leftPost > 70 and leftPost <= 125 and leftDiff > 5:
			position = "A3"	
		    elif leftPost > 125 and leftDiff > 12:
			position = "A4"

		   

		    elif leftPost <= 48 and leftDiff <= 3:
		    	position = "B1"
		    elif leftPost > 48 and leftPost <= 81 and leftDiff <= 3:
		    	position = "B2"
		    elif leftPost > 81 and leftDiff <= 3:
		    	position = "B3"			

		if rightPost > leftPost:
		    rightDiff = rightPost - leftPost
		    if rightPost <= 48 and rightDiff > 2:
			position = "C1"
		    elif rightPost > 48 and rightPost <= 70 and rightDiff > 5:
			position = "C2"
		    elif rightPost > 70 and rightPost <= 125 and rightDiff > 5:
			position = "C3"	
		    elif rightPost > 125 and rightDiff > 12:
			position = "C4"

		    elif rightPost <= 48 and rightDiff < 3:
		    	position = "B1"
		    elif rightPost > 48 and rightPost <= 81 and rightDiff < 3:
		    	position = "B2"
		    elif rightPost > 81 and rightDiff < 3:
		    	position = "B3"

		posPub.publish(position)
		done = True

def callback(data):
    global leftPost, rightPost, init, done
    if init == True:
	leftPost = data.LPH
	rightPost = data.RPH
	init = False
    if done == True:
	leftPost = data.LPH
	rightPost = data.RPH
	done = False

if __name__ == '__main__':
    getData()
