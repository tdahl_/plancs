#!/usr/bin/env python

import rospy, plancs
import os			# Needed to get the filepath used in name generation.
import sys			# Needed to control arguments being passed in.
from std_msgs.msg import Bool	# Message type to be received in the subscriber.

def main():
    global callbackBlocker
    callbackBlocker = False

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]	# Takes the file path and extracts the name
    mynode = plancs.Plancs_Node(name)				# Initialises this node as a PLANCS node

    subscriberName = "plancs_search_trigger"		# Our simplest example of a node publishing data:  bool("True")
    subscriber = mynode.add_subscriber(subscriberName, Bool, callback)	# Calls the callback every time data is received. 
    
    while not rospy.is_shutdown():		# Main loop.
	
	if callbackBlocker == True:	# This will only pass if we've received "True" from the subscription to plancs_search_trigger.
	    print "Waiting for callback"	# Main code would go here.
	    callbackBlocker = False	# Setting the blocker to false by default - in essence this is a check for inhibition.
	    mynode.sleep(0.2)		# Allows a 0.2 seconds window for the callback to set callbackBlocker back to true.
					# This stops the main loop from running more than once per callback.
					# If inhibited, callbackBlocker will remain false until excitation.

def callback(subInput):			# Runs when the subscriber receives data.
    global callbackBlocker	
    callbackBlocker = subInput.data	# Saving published data into a variable.
    print "Callback"
if __name__ == '__main__':
    main()
