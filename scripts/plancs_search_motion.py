#!/usr/bin/env python

import rospy, sys, plancs, os
from plancs import *
from std_msgs.msg import Bool

def getData():
    global ourBool
    ourBool = False
    stopper = True
    isAbsolute = True
    walking = False
    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    subName = "plancs_search_trigger"
    argv = rospy.myargv(argv=sys.argv)
    alproxyname = "ALMotion"

    if len(argv) >=2:
	subName = subName + "_" + argv[1]
	name = name + "_" + argv[1]
        alproxyname = alproxyname + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    sub = mynode.add_subscriber(subName, Bool, main)
    post = True
    motionProxy = PALProxy(alproxyname)
    jointName   = "Body"
    motionProxy.setStiffnesses(jointName, 1.0)
    
    while not rospy.is_shutdown():
	
	if ourBool == True and stopper == True:		# Runs once every time the node is started/excited.
	    motionProxy.killMove()
	    names      = ["HeadPitch", "HeadYaw"]
	    angleLists = [[1.0, 0.0], [0.0]]
	    timeLists  = [[1.0, 2.0], [0.5]]
	    motionProxy.post.angleInterpolation(names, angleLists, timeLists, isAbsolute)
	
	    #names      = ["HeadYaw", "HeadPitch"]
	    #angleLists = [[1.0, -1.0, 0.0, 1.0, -1.0, 0.0], 
		#		    [1.0, 0.0, 1.0, 0.0]]
	    #timeLists  = [[1.0, 3.0, 5.0, 7.0, 9.0, 11.0], [2.0, 4.0, 8.0, 11.0]]
	    #motionProxy.angleInterpolation(names, angleLists, timeLists, isAbsolute, post)
	    ourBool = False
	    mynode.sleep(0.1)
	    stopper = False
	if ourBool == True and stopper == False:	# Main looping if-statement.
	    x = 0.0
	    y = 0.0
	    theta = 1.0
	    motionProxy.move(x, y, theta)
	    #ourBool = False
	    #mynode.sleep(0.2)

	    names      = ["HeadPitch"]
	    angleLists = [1.0, 0.0]
	    timeLists  = [1.0, 2.0]
	    motionProxy.post.angleInterpolation(names, angleLists, timeLists, isAbsolute)
	    ourBool = False
	    mynode.sleep(0.1)

	if ourBool == False and stopper == False:	# Runs once when the node is inhibited.
	    motionProxy.killTasksUsingResources(["HeadYaw", "HeadPitch"])
	    motionProxy.killMove()
	    stopper = True
	    walking = False
        #rospy.sleep(0.1)


def main(ball):
    global ourBool
    ourBool = ball.data

if __name__ == '__main__':
    getData()
