#!/usr/bin/env python

from sensor_msgs.msg import JointState
from plancs.msg import ObjectDimensions
from std_msgs.msg import Bool, Empty
import rospy, plancs, time, sys, os

def get_data():
    global ball, goal, neck, init, timeStamp
    ball = None
    goal = None
    neck = None
    init = None

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["bottom_vision_ball_dimensions", "opponent_goal", "joint_states", "plancs_kick_left_trigger", ]
    inhibit_names = ["plancs_search_trigger", "plancs_approach_left_trigger", "plancs_approach_right_trigger", "plancs_approach_center_trigger", "plancs_align_trigger"]
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "_" + argv[1] + "/plancs_inhibit"
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]
    else:
        for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "/plancs_inhibit"

    timeStamp = time.time()
    mynode = plancs.Plancs_Node(name)
    mynode.set_decay_period(1.0)
    bottomBallSub = mynode.add_subscriber(topic_names[0], ObjectDimensions, ballData)
    goalSub = mynode.add_subscriber(topic_names[1], ObjectDimensions, opponentGoalData)
    neckSub = mynode.add_subscriber(topic_names[2], JointState, getNeckData)
    boolPub = rospy.Publisher(topic_names[3], Bool)

    searchInhibit = rospy.Publisher(inhibit_names[0], Empty)
    leftApproachInhibit = rospy.Publisher(inhibit_names[1], Empty)
    rightApproachInhibit = rospy.Publisher(inhibit_names[2], Empty)
    centerApproachInhibit = rospy.Publisher(inhibit_names[3], Empty)
    alignInhibit = rospy.Publisher(inhibit_names[4], Empty)
    while not rospy.is_shutdown():
        if time.time() > timeStamp + 3:
           goal = None
	if init != None:
	    if neck.position[1] >= 0.45:
	        if ball != None and goal != None:
 		    print (ball.x + (ball.width/2))
		    if (ball.x + (ball.width/2)) >= 80.0 and (ball.x + (ball.width/2)) <= 145.0 and ball.y >= 50:
                        if (ball.x + (ball.width/2)) >= goal.x and (ball.x + (ball.width/2)) <= (goal.x + goal.width):
			    print "Ball aligned for kick"
	                    boolPub.publish(Bool(True))
                            searchInhibit.publish()
                            leftApproachInhibit.publish()
                            rightApproachInhibit.publish()
                            centerApproachInhibit.publish()
                            alignInhibit.publish()

        rospy.sleep(0.1)
                        

def ballData(bl):
    global ball
    ball = bl

def opponentGoalData(gl):
    global goal, timeStamp
    timeStamp = time.time()
    goal = gl

def getNeckData(nk):
    global neck, init
    neck = nk
    init = True
if __name__ == '__main__':
    get_data()
