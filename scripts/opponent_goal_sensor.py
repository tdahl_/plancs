#!/usr/bin/env python

from plancs.msg import ObjectDimensions
import rospy, time, plancs, os, sys
from sensor_msgs.msg import JointState

def get_dimensions():
    global goal, waistband, timeStamp, joints
    waistband = None
    goal = None
    timeStamp = None
    previousTimeStamp = None
    joints = None

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["joint_states", "waistband_dimensions", "goal_dimensions", 'opponent_goal']
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(topic_names[0], JointState, jointStates)
    mynode.add_subscriber(topic_names[1], ObjectDimensions, waistbandData)
    mynode.add_subscriber(topic_names[2], ObjectDimensions, goalData)
    goalPub = rospy.Publisher(topic_names[3], ObjectDimensions)
    while not rospy.is_shutdown():
        if not waistband == None and not goal == None:
            if timeStamp != previousTimeStamp:
                goalWidth = goal.width
                goalHeight = goal.height
                goalxBR = goal.x + goalWidth
                goalyBR = goal.y + goalHeight
                previousTimeStamp = timeStamp
                if waistband.x > goal.x and waistband.x < goalxBR:
                    if waistband.y > goal.y and waistband.y < goalyBR:
                        if joints != None:
                            headYaw = joints.position[0]
                            if headYaw < 0.001 or headYaw > -0.001:
	                        print "Found opponent goal"
	                        goalPub.publish(goal)
        rospy.sleep(0.1)

def waistbandData(wb):
    global waistband, timeStamp
    timeStamp = time.time()
    waistband = wb

def goalData(gl):
    global goal, timeStamp
    timeStamp = time.time()
    goal = gl

def jointStates(jointValues):
    global joints
    joints = jointValues

if __name__ == '__main__':
    get_dimensions()
