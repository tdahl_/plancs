#!/usr/bin/env python

import sys, time, os
from collections import OrderedDict
from plancs import *
from os.path import expanduser

motionProxy = PALProxy("ALMotion")


def main(motionFile):
    PORT = 9559
    robotIP = "127.0.0.1"

    motionProxy.setStiffnesses("Body", 1.0)

    #userName = os.getlogin()
    #userName = pwd.getpwuid(os.getuid()).pw_name

    home = expanduser("~")

    #tmpfile = open("/home/" + userName + "/workspace/src/userplancs/scripts/motions/" + motionFile)
    tmpfile = open(home + "/workspace/src/userplancs/scripts/motions/" + motionFile)
    motions = OrderedDict()
    times = []
    timesNew = []
    for line in tmpfile:
        tmp = line.split(',')
	times.append((tmp.pop(0)).split(":"))
	tmp.pop(0)
	if line[0] == chr(35): #lines starting with hashes define joints used in motion.
	    times.pop(0)
	    for element in tmp:
		motions[element.strip()] = []
	else:
	    keys = motions.keys()
	    for i,element in enumerate(tmp):
	        motions[keys[i]].append(float(tmp[i]))

    for x in times:
	new_time = ((float("".join(x))) / 1000) + 1.0
	timesNew.append(new_time)
    timesNew.pop(0) # first row

    names = keys
    angleLists = []
    timeLists = []
    firstRow = []  # first row
    for name in range(len(names)):
	firstRow.append((motions[keys[name]]).pop(0)) # first row
        angleLists.append(motions[keys[name]])
	timeLists.append(timesNew)
    isAbsolute  = True
    maxSpeedFraction  = 0.9 #  first row
    targetAngles  = firstRow #  first row

    motionProxy.angleInterpolationWithSpeed(names, targetAngles, maxSpeedFraction)
    time.sleep(0.1)
    motionProxy.angleInterpolation(names, angleLists, timeLists, isAbsolute)

if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print "Usage python almotion_angleinterpolation.py (optional default: 127.0.0.1)"
    else:
        motionFile = sys.argv[1]

    main(motionFile)
