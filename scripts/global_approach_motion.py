#!/usr/bin/env python

import rospy, time, plancs, sys, math, os
from plancs.msg import ObjectDimensions
from sensor_msgs.msg import JointState
from std_msgs.msg import String
from std_srvs.srv import Empty
from plancs import *



def main():
    global position, ball, goal, joint, init1, init2, init3, done1, done2, done3, motionProxy
    position = None
    ball = None
    goal = None
    joint = None
    done1 = False
    done2 = False
    done3 = False
    init1 = True
    init2 = True
    init3 = True
    angle = 0.0
    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    subName1 = "position_sensor"
    subName2 = "global_ball_position"
    subName3 = "opponent_goal"
    motProx = "ALMotion"
    argv = rospy.myargv(argv=sys.argv)

    if len(argv) >= 2:
	subName1 = subName1 + "_" + argv[1]
#	subName2 = subName2 + "_" + argv[1]	# Shouldn't be necessary to add params, assuming only 1 robot runs the global ball pos node.
	subName3 = subName3 + "_" + argv[1]
	motProx = motProx + "_" + argv[1]
	name = name + "_" + argv[1]

    motionProxy = PALProxy(motProx)
    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(subName1, String, posCallback)
    mynode.add_subscriber(subName2, String, ballCallback)
    mynode.add_subscriber(subName3, ObjectDimensions, goalCallback)
    #mynode.add_subscriber("joint_states", JointState, jointCallback)
    motionProxy.setStiffnesses("Body", 1.0)

    while not rospy.is_shutdown():
	if done1 == False and done2 == False and done3 == False:
	    if ball != None and goal != None:

		if position == "A1":
		    if ball == "A2" or ball == "A3" or ball == "A4":
			angle = math.radians(15)
		    if ball == "B1" or ball == "C1":					# +ve angle == Left, -ve angle == Right
			angle = math.radians(-90)
		    elif ball == "B2" or ball == "C3":
			angle = math.radians(-45)
		    elif ball == "B3" or ball == "B4":
			angle = math.radians(-15)
		    elif ball == "C2":
			angle = math.radians(-65)
		    elif ball == "C4":
			angle = math.radians(-30)

		elif position == "A2":
		    if ball == "A1":
			angle = math.radians(-180)
		    elif ball == "A3" or ball == "A4":
			angle = math.radians(15)
		    elif ball == "B1":
			angle = math.radians(-120)
		    elif ball == "C1":
			angle = math.radians(-90)
		    elif ball == "B2" or ball == "C2":
			angle = math.radians(-75)
		    elif ball == "B3" or ball == "C4":
			angle = math.radians(-15)
		    
		elif position == "A3":
		    if ball == "A1" or ball == "A2":
			angle = math.radians(-120)
		    elif ball == "A4":
			angle = math.radians(45)
		    elif ball == "B1":
			angle = math.radians(-120)
		    elif ball == "B2" or ball == "C1":
			angle = math.radians(-90)
		    elif ball == "B3" or ball == "C3":
			angle = math.radians(-45)
		    elif ball == "C4":
			angle = math.radians(-30)

		elif position == "A4":
		    if ball == "A1" or ball == "A2" or ball == "A3":
			angle = math.radians(-135)
		    elif ball == "B1" or ball == "B2" or ball == "C1":
			angle = math.radians(-120)
		    elif ball == "B3" or ball == "C2":
			angle = math.radians(-90)
		    elif ball == "C3":
			angle = math.radians(-75)
		    elif ball == "B4" or ball == "C4":
			angle = math.radians(-45)
   
		elif position == "B1":
		    if ball == "A1":
			angle = math.radians(90)
		    elif ball == "C1":
			angle = math.radians(-90)
		    elif ball == "A2":
			angle = math.radians(45)
		    elif ball == "C2":
			angle = math.radians(-45)
		    elif ball == "A3":
			angle = math.radians(30)
		    elif ball == "C3":
			angle = math.radians(-30)
		    elif ball == "A4":
			angle = math.radians(15)
		    elif ball == "C4":
			angle = math.radians(-15)

		elif position == "B2":
		    if ball == "B1":
			angle = math.radians(-180)
		    elif ball == "A1":
			angle = math.radians(135)
		    elif ball == "C1":
			angle = math.radians(-135)
		    elif ball == "A2":
			angle = math.radians(90)
		    elif ball == "C2":
			angle = math.radians(-90)
		    elif ball == "A3":
			angle = math.radians(45)
		    elif ball == "C3":
			angle = math.radians(-45)
		    elif ball == "A4":
			angle = math.radians(15)
		    elif ball == "C4":
			angle = math.radians(-15)
		    
		elif position == "B3":
		    if ball == "B2" or ball == "B1":
			angle = math.radians(-180)
		    elif ball == "A1":
			angle = math.radians(150)
		    elif ball == "C1":
			angle = math.radians(-150)
		    elif ball == "A2":
			angle = math.radians(135)
		    elif ball == "C2":
			angle = math.radians(-135)
		    elif ball == "A3":
			angle = math.radians(90)
		    elif ball == "C3":
			angle = math.radians(-90)
		    elif ball == "A4":
			angle = math.radians(45)
		    elif ball == "C4":
			angle = math.radians(-45)

		#elif position == "B4":				# Robot is too close to identify the goal in B4

		if position == "C1":
		    if ball == "C2" or ball == "C3" or ball == "C4":
			angle = math.radians(-15)
		    if ball == "B1" or ball == "A1":
			angle = math.radians(90)
		    elif ball == "B2" or ball == "A3":
			angle = math.radians(45)
		    elif ball == "A3" or ball == "B4":
			angle = math.radians(15)
		    elif ball == "A2":
			angle = math.radians(65)
		    elif ball == "A4":
			angle = math.radians(30)

		elif position == "C2":
		    if ball == "C1":
			angle = math.radians(180)
		    elif ball == "C3" or ball == "C4":
			angle = math.radians(-15)
		    elif ball == "B1":
			angle = math.radians(120)
		    elif ball == "A1":
			angle = math.radians(90)
		    elif ball == "B2" or ball == "A2":
			angle = math.radians(75)
		    elif ball == "B3" or ball == "A4":
			angle = math.radians(15)
		    
		elif position == "C3":
		    if ball == "C1" or ball == "C2":
			angle = math.radians(120)
		    elif ball == "C4":
			angle = math.radians(-45)
		    elif ball == "B1":
			angle = math.radians(120)
		    elif ball == "B2" or ball == "A1":
			angle = math.radians(90)
		    elif ball == "B3" or ball == "A3":
			angle = math.radians(45)
		    elif ball == "A4":
			angle = math.radians(30)

		elif position == "C4":
		    if ball == "C1" or ball == "C2" or ball == "C3":
			angle = math.radians(135)
		    elif ball == "B1" or ball == "B2" or ball == "A1":
			angle = math.radians(120)
		    elif ball == "B3" or ball == "A2":
			angle = math.radians(90)
		    elif ball == "A3":
			angle = math.radians(75)
		    elif ball == "B4" or ball == "A4":
			angle = math.radians(45)

		angle = angle * 1.5	# Added 50% because of simulation problems
		turnBody(angle)
		done1 = True
		done2 = True
		done3 = True

def turnBody(ang):
    global motionProxy
    x  = 0.0
    y  = 0.0
    post = True
    motionProxy.moveTo(x, y, ang)

def ballCallback(bl):
    global ball, init1, done1
    if init1 == True:
	ball = bl.data
	init1 = False
    if done1 == True:    
	ball = bl.data
	done1 = False

def goalCallback(gl):
    global goal, init2, done2
    if init2 == True:
	goal = gl.x
	init2 = False
    if done2 == True:
	goal = gl.x
	done2 = False

def posCallback(pos):
    global position, init3, done3
    if init3 == True:
	position = pos.data
	init3 = False
    if done3 == True:
	position = pos.data
	done3 = False

def jointCallback(jt):
    global joint



if __name__ == "__main__":
    main()
