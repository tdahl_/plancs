#!/usr/bin/env python

import rospy, plancs, sys, os
from std_msgs.msg import Bool

def get_values():
    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    pubName = "plancs_search_trigger"
    argv = rospy.myargv(argv=sys.argv)

    if len(argv) >=2:
	pubName = pubName + "_" + argv[1]
	name = name + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)	
    ballPub = rospy.Publisher(pubName, Bool)
    mynode.set_decay_period(4.0)
    while not rospy.is_shutdown():
        ballPub.publish(Bool(1))
        mynode.sleep(0.05)

if __name__ == '__main__':
    get_values()
