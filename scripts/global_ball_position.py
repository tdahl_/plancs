#!/usr/bin/env python

"""This script estimates a range of distances and angles before performing trig calculations on them. The purpose is to estimate, using the goalkeeper's camera data, where the ball is globally. There is a problem that the robot won't react if the ball is in grid section A1, B1 or C1, since it is too close and out of view.
It must be made clear that the robot running this script MUST be in the friendly team's goal area, looking at the opponent's goal - centred on it if possible. Otherwise the estimated angles won't work. Also, the neck/head must be centered or again, it won't work accurately."""

import rospy, time, plancs, math, sys, os
from plancs.msg import ObjectDimensions
from std_srvs.srv import Empty
from plancs import *
from std_msgs.msg import String

def main():
    global position, ball, ballX, ballDistance, goalDistance, goalAngle, ballAngle, ballGoalDistance, init, done, goal, goalW
    ball = None
    ballX = None
    goal = None
    goalW = None
    position = None
    goalDistance = None
    ballPos = None
    goalAngle = None
    ballAngle = None
    init = True
    done = False
    ballDistance = None

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    subName = "position_sensor"
    subName2 = "opponent_goal"
    subName3 = "top_vision_ball_dimensions"
    pubName = "global_ball_position"

    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >=2:
	name = name + "_" + argv[1]
	subName = subName + "_" + argv[1]
	subName2 = subName2 + "_" + argv[1]
	subName3 = subName3 + "_" + argv[1]
#	pubName = pubName + "_" + argv[1]	# Not necessary to get params assuming only 1 robot runs global ball position node.

    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(subName, String, positionCallback)
    mynode.add_subscriber(subName2, ObjectDimensions, goalCallback)
    mynode.add_subscriber(subName3, ObjectDimensions, topCamCallback)
    ballPosPub = rospy.Publisher(pubName, String)

    while not rospy.is_shutdown():
        if position != None and ball != None and goal != None:
	    if done == False and position == "B1":	# The goalie MUST be in grid B1

		if ballX <= (goal / 3):			# Estimates for the angle between the ball, robot and left post.
		    goalAngle = 30.0			# Based on left/right position of ball relative to the goal.
		if ballX <= goal and ballX >= (goal / 3) :
		    goalAngle = 20.0
		if ballX == goal:
		    goalAngle = 5.0
		if ballX >= goal and ballX <= goal + (goalW /2):
		    goalAngle = -10.0
		if ballX >= goal + (goalW /2) and ballX <= goal + goalW:
		    goalAngle = -20.0
		if ballX >= (goal +  goalW):
		    goalAngle = -30.0


	    	if ball >= 220:					# Estimating distance to the ball from y-coordinates in camera.
                    ballDistance = 1.5				# TODO: Neck MUST be at a level angle - add a joint_state check
    	    	elif ball >= 200 and ball < 220:
            	    ballDistance = 2.0
            	elif ball >= 192 and ball < 200:
           	    ballDistance = 2.25
            	elif ball >= 182 and ball < 192:
            	    ballDistance = 2.5
    	    	elif ball >= 175 and ball < 182:
            	    ballDistance = 3.0
    	    	elif ball >= 168 and ball < 175:
            	    ballDistance = 3.5
    	    	elif ball >= 161 and ball < 168: 
            	    ballDistance = 4.0
    	    	elif ball >= 156 and ball < 161: 
            	    ballDistance = 4.5
    	    	elif ball >= 153 and ball < 156: 
            	    ballDistance = 5.0
    	    	elif ball >= 150 and ball < 153: 
            	    ballDistance = 5.5
    	    	elif ball >= 147 and ball < 150: 
            	    ballDistance = 6.0
    	    	elif ball >= 140 and ball < 147: 
           	    ballDistance = 6.5

		if ballDistance != None and goalAngle != None:
		    
		    hyp = float(goalDistance)
		    adj = float(ballDistance)

		    goalAngle = float(goalAngle)

		    goalAngleRad = math.radians(goalAngle)

		    hypS = math.pow(hyp, 2)  		
		    adjS = math.pow(adj, 2)
		    
		    opp = hypS + adjS - 2 * (hyp * adj) * math.cos(goalAngleRad)
		    opp = math.sqrt(opp)		# Calculating the length of the opposite side.
		    
		    sinCheck = (opp/math.sin(goalAngleRad))

		    ballAngle = math.asin(ballDistance/sinCheck)	# Calculating the angle of the opposite.
		    ballAngle = math.degrees(ballAngle)

		    # Checking for angles, distance to ball and distance between ball and goal.

		    if ballAngle > -55 and ballAngle < -30 and opp > 3.9 and opp < 4.5 and ballDistance >= 6.5:
			ballPos = "C4"
		    elif ballAngle > -40 and ballAngle < -23 and opp > 4 and opp < 5 and ballDistance <= 6.5 and ballDistance >= 4.5:
			ballPos = "C3"
		    elif ballAngle >= -23 and ballAngle < -10 and opp > 5 and opp < 6.2 and ballDistance <= 4.5:
			ballPos = "C2"

		    elif ballAngle > -30 and ballAngle < 30 and opp >= 2.8 and opp <= 3.2 and ballDistance >= 5.5:
			ballPos = "B4"
		    elif ballAngle > -28 and ballAngle < 29 and opp >= 2.0 and opp <= 4.0 and ballDistance >= 4 and ballDistance <= 5.5:
			ballPos = "B3"
		    elif ballAngle > -41 and ballAngle < 0 and opp > 1 and opp < 5 and ballDistance >= 1.5 and ballDistance <= 4:
			ballPos = "B2"



		    elif ballAngle > 48 and ballAngle < 54 and opp >= 2.8 and opp < 4 and ballDistance >= 6:
			ballPos = "A4"
		    elif ballAngle > 22 and ballAngle < 41 and opp >= 2.5 and opp <= 5 and ballDistance < 6 and ballDistance > 4:
			ballPos = "A3"
		    elif ballAngle > 11 and ballAngle < 24 and opp >= 2 and opp < 5 and ballDistance <= 4:
			ballPos = "A2"

		    ballPosPub.publish(ballPos)

		    #print "The ball is ", ballDistance, " metres away from me."
		    #print "I am ", goalDistance, " metres away from the left post."
		    #print "The distance between the ball and the goal is ", opp, " metres."
		    #print "The angle between the goal, myself and the ball is ", goalAngle, " degrees."
		    #print "The angle beteen the left post and the ball is ", ballAngle, " degrees."
		    #print
		    

		ball = None
		ballDistance = None
		
		done = True
		mynode.sleep(0.2)


def positionCallback(pos):
    global position, goalDistance
    position = pos.data
    if position == "A1":	# Estimating the distance to the left post from grid reference data.
	goalDistance = 8.0	# (In metres)
    if position == "A2":
	goalDistance = 5.75
    if position == "A3":
	goalDistance = 3.5
    if position == "A4":
	goalDistance = 1.75
    if position == "B1":
	goalDistance = 8.0
    if position == "B2":
	goalDistance = 5.5
    if position == "B3":
	goalDistance = 3.5
    if position == "C1":
	goalDistance = 8.5
    if position == "C2":
	goalDistance = 6.25
    if position == "C3":
	goalDistance = 4.5
    if position == "C4":
	goalDistance = 2.5


def topCamCallback(bl):
    global ball, ballX, init, done
    if init == True:
	ball = bl.y
	ballX = bl.x
	init = False
    if done == True:
	ball = bl.y
	ballX = bl.x
	done = False

def goalCallback(gl):
    global goal, goalW
    goal = gl.x
    goalW = gl.width


if __name__ == "__main__":
    main()
