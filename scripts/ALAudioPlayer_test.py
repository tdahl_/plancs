#!/usr/bin/env python

import sys
from plancs import *
import rospy
import plancs

def main():
    rospy.init_node("PlaySound")

#	This audio player works but it still always gets the following error.

#	File "/opt/ros/groovy/lib/python2.7/dist-packages/rospy/impl/tcpros_service.py", line 337, in _read_ok_byte
#	raise ServiceException("service [%s] responded with an error: %s"%(self.resolved_name, str))
#	rospy.service.ServiceException: service [/nao_ALAudioPlayer_playFile] responded with an error: service cannot process request: service #	handler returned None

    audioProxy = PALProxy("ALAudioPlayer")

    # Play file by first loading it and then play using taskID
    # --------------------------------------------------------

    fileID = audioProxy.loadFile('/usr/share/naoqi/wav/stop_jingle.wav')
    print fileID.taskID
    rospy.sleep(2)
    audioProxy.play(fileID.taskID)

    # Play file by automatically loading and playing
    # ----------------------------------------------

    #print audioProxy.getLoadedFilesNames()
    #audioProxy.playFile('/usr/share/naoqi/wav/stop_jingle.wav')

if __name__ == "__main__":
    main()
