#!/usr/bin/env python
import rospy, time, plancs, sys, os
from plancs.msg import ObjectDimensions
from sensor_msgs.msg import JointState
from std_srvs.srv import Empty
from plancs import *

motionProxy = PALProxy("ALMotion")

def main():
    global timeStamp, joints, ball, thetaValue

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["plancs_approach_left_trigger", "plancs_approach_center_trigger", "plancs_approach_right_trigger", "joint_states", "bottom_vision_ball_dimensions"]
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]

    mynode = plancs.Plancs_Node(name)
    thetaValue = 0.0
    moveActive = False
    ball = None
    joints = None
    timeStamp = 0.0
    previousTime = 0.0
    headHasTurned = False
    mynode.add_subscriber(topic_names[0], ObjectDimensions, walkLeft)
    mynode.add_subscriber(topic_names[1], ObjectDimensions, walkCenter)
    mynode.add_subscriber(topic_names[2], ObjectDimensions, walkRight)
    mynode.add_subscriber(topic_names[3], JointState, jointStates)
    mynode.add_subscriber(topic_names[4], ObjectDimensions, bottomCam)
    motionProxy.setStiffnesses("Body", 1.0)
    while not rospy.is_shutdown():
        timeNow = time.time()
        if timeNow < (timeStamp + 0.5):
            if joints != None:
                headYaw = joints.position[0]
                while headYaw > 0.001 or headYaw < -0.001:   # This section in the while loop will align the head to the body
                    headYaw = joints.position[0]             # while turning the body to face the ball. It is used if the head
                    if not headHasTurned:		     # is looking in a different direction to the body.
                        turnHead(headYaw)
                        turnBody(headYaw)
                        headHasTurned = True

                headHasTurned = False
                if previousTime < timeNow:			# Used to approach a ball.
                    motionProxy.move(1.0, 0.0, thetaValue)
                    mynode.sleep(0.05)
                    moveActive = True
                    previousTime = time.time()

                headPitch = joints.position[1]			# Used to tilt the head down as it gets closer to the ball.
                if ball != None:
                    if ball.y != 0.0:
                        if ball.y >= 100:
                            headPitch = joints.position[1]
		            if headPitch < 0.2: 
                                motionProxy.setAngles(["HeadPitch"], [0.25], 0.1)
		            elif headPitch >= 0.2 and headPitch < 0.5149:
                                motionProxy.setAngles(["HeadPitch"], [0.5149], 0.1) 

        timeNow = time.time()					# Stops all movement if the ball is not seen.
        if timeNow >= (timeStamp + 0.5) and moveActive == True:
            motionProxy.killMove()
            moveActive = False

        rospy.sleep(0.1)

def turnHead(headYawValue):
    time = headYawValue/1.6 * 10.0 # Set the time according to the angle size.
    names      = ["HeadYaw"]
    angleLists = [0.0]
    times      = [abs(time)]
    isAbsolute = True
    post = True
    motionProxy.post.angleInterpolation(names, angleLists, times, isAbsolute)

def turnBody(headYawValue):
    headYaw = headYawValue + headYawValue * 0.5  # Adding 50% extra to the rotation due to the simulation underestimating the motion.
    x  = 0.0
    y  = 0.0
    post = True
    motionProxy.post.moveTo(x, y, headYaw)

def jointStates(jointValues):
    global joints
    joints = jointValues

def walkLeft(bl):
    global timeStamp, thetaValue
    timeStamp = time.time()
    thetaValue = 0.25

def walkRight(bl):
    global timeStamp, thetaValue
    timeStamp = time.time()
    thetaValue = -0.25

def walkCenter(bl):
    global timeStamp, thetaValue
    timeStamp = time.time()
    thetaValue = 0.0

def bottomCam(bl):
    global ball
    ball = bl

if __name__ == "__main__":
    main()
