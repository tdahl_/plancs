#!/usr/bin/env python

import rospy
import motion, time, math
#from naoqi import ALProxy
from std_msgs.msg import Bool
from std_srvs.srv import Empty

from plancs.srv import *

if __name__ == "__main__":
	rospy.init_node('right_kick')
	setStiffnesses = rospy.ServiceProxy("nao_ALMotion_setStiffnesses", SALMotion_setStiffnesses)
	goToPosture = rospy.ServiceProxy("nao_ALRobotPosture_goToPosture", SALRobotPosture_goToPosture)
	wbEnable = rospy.ServiceProxy("nao_ALMotion_wbEnable", SALMotion_wbEnable)
	wbFootState = rospy.ServiceProxy("nao_ALMotion_wbFootState", SALMotion_wbFootState)
	wbEnableBalanceConstraint = rospy.ServiceProxy("nao_ALMotion_wbEnableBalanceConstraint", SALMotion_wbEnableBalanceConstraint)
	wbGoToBalance = rospy.ServiceProxy("nao_ALMotion_wbGoToBalance", SALMotion_wbGoToBalance)
	positionInterpolation = rospy.ServiceProxy("nao_ALMotion_positionInterpolation", SALMotion_positionInterpolation)
	wbEnableEffectorOptimization = rospy.ServiceProxy("nao_ALMotion_wbEnableEffectorOptimization", SALMotion_wbEnableEffectorOptimization)

	#motionProxy.setStiffnesses("Body", 1.0)
	setStiffnesses("Body", str(1.0), False)

	#postureProxy.goToPosture("StandInit", 0.5)
	goToPosture("StandInit", 1.0, False)

	# Activate Whole Body Balancer
	isEnabled  = True
	#motionProxy.wbEnable(isEnabled)
	wbEnable(isEnabled, False)

	# Legs are constrained fixed
	stateName  = "Fixed"
	supportLeg = "Legs"
	#motionProxy.wbFootState(stateName, supportLeg)
	wbFootState(stateName, supportLeg, False)

	# Constraint Balance Motion
	isEnable   = True
	supportLeg = "Legs"
	#motionProxy.wbEnableBalanceConstraint(isEnable, supportLeg)
	wbEnableBalanceConstraint(isEnable, supportLeg, False)

	# Com go to LLeg
	supportLeg = "LLeg"
	duration   = 0.7					# (DY edit. Original 2.0)
	#motionProxy.wbGoToBalance(supportLeg, duration)
	wbGoToBalance(supportLeg, duration, False)

	# RLeg is free
	stateName  = "Free"
	supportLeg = "RLeg"
	#motionProxy.wbFootState(stateName, supportLeg)
	wbFootState(stateName, supportLeg, False)

	# LLeg is optimized
	effectorName = "RLeg"
	axisMask	 = 63
	space = motion.SPACE_NAO

	# Motion of the RLeg
	dx	  = 0.05				 # translation axis X (meters) 	(DY edit. Original: 0.05)
	dz	  = 0.03				  # translation axis Z (meters) 	(DY edit. Original: 0.05)
	dwy	 = 5.0*math.pi/180.0	# rotation axis Y (radian)		(DY edit. Original: 5.0*math.pi/180.0)
	times   = [0.2, 0.4, 0.9]
	isAbsolute = False		

	targetList = [
	[-dx, 0.0, dz, 0.0, +dwy, 0.0],
	[+dx, 0.0, dz, 0.0, 0.0, 0.0],
	[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]
	mynewthing = str(times)
	path = str(targetList)
	#motionProxy.positionInterpolation(effectorName, space, targetList, axisMask, times, isAbsolute)
	positionInterpolation(effectorName, space, path, axisMask, mynewthing, isAbsolute, False)

	# Example showing how to Enable Effector Control as an Optimization
	isActive	 = False
	#motionProxy.wbEnableEffectorOptimization(effectorName, isActive)
	wbEnableEffectorOptimization(effectorName, isActive, False)
	time.sleep(0.2)			# (DY edit. Original: 1.0)

	# Deactivate Head tracking
	isEnabled	= False
	#motionProxy.wbEnable(isEnabled)
	wbEnable(isEnabled, False)

	# send robot to Pose Init
	#postureProxy.goToPosture("StandInit", 0.5)
	goToPosture("StandInit", 0.5, False)
