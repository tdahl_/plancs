#!/usr/bin/env python

import rospy
from std_srvs.srv import Empty
from plancs.srv import SALMotionProxy_wakeUp, SALMotionProxy_wakeUpResponse, \
						SALMotionProxy_rest, SALMotionProxy_restResponse, \
						SALMotionProxy_stiffnessInterpolation, SALMotionProxy_stiffnessInterpolationResponse, \
						SALMotionProxy_setStiffnesses, SALMotionProxy_setStiffnessesResponse, \
						SALMotionProxy_getStiffnesses, SALMotionProxy_getStiffnessesResponse, \
						SALMotionProxy_angleInterpolation, SALMotionProxy_angleInterpolationResponse, \
						SALMotionProxy_angleInterpolationWithSpeed, SALMotionProxy_angleInterpolationWithSpeedResponse, \
						SALMotionProxy_angleInterpolationBezier, SALMotionProxy_angleInterpolationBezierResponse, \
						SALMotionProxy_setAngles, SALMotionProxy_setAnglesResponse, \
						SALMotionProxy_changeAngles, SALMotionProxy_changeAnglesResponse, \
						SALMotionProxy_getAngles, SALMotionProxy_getAnglesResponse, \
						SALMotionProxy_closeHand, SALMotionProxy_closeHandResponse, \
						SALMotionProxy_openHand, SALMotionProxy_openHandResponse, \
						SALMotionProxy_setWalkTargetVelocity, SALMotionProxy_setWalkTargetVelocityResponse, \
						SALMotionProxy_walkTo, SALMotionProxy_walkToResponse, \
						SALMotionProxy_move, SALMotionProxy_moveResponse, \
						SALMotionProxy_moveTo, SALMotionProxy_moveToResponse, \
						SALMotionProxy_moveToward, SALMotionProxy_moveTowardResponse, \
						SALMotionProxy_setFootSteps, SALMotionProxy_setFootStepsResponse, \
						SALMotionProxy_setFootStepsWithSpeed, SALMotionProxy_setFootStepsWithSpeedResponse, \
						SALMotionProxy_getFootSteps, SALMotionProxy_getFootStepsResponse, \
						SALMotionProxy_walkInit, SALMotionProxy_walkInitResponse, \
						SALMotionProxy_moveInit, SALMotionProxy_moveInitResponse, \
						SALMotionProxy_waitUntilWalkIsFinished, SALMotionProxy_waitUntilWalkIsFinishedResponse, \
						SALMotionProxy_waitUntilMoveIsFinished, SALMotionProxy_waitUntilMoveIsFinishedResponse, \
						SALMotionProxy_walkIsActive, SALMotionProxy_walkIsActiveResponse, \
						SALMotionProxy_moveIsActive, SALMotionProxy_moveIsActiveResponse, \
						SALMotionProxy_stopWalk, SALMotionProxy_stopWalkResponse, \
						SALMotionProxy_stopMove, SALMotionProxy_stopMoveResponse, \
						SALMotionProxy_getFootGaitConfig, SALMotionProxy_getFootGaitConfigResponse, \
						SALMotionProxy_getMoveConfig, SALMotionProxy_getMoveConfigResponse, \
						SALMotionProxy_getRobotPosition, SALMotionProxy_getRobotPositionResponse, \
						SALMotionProxy_getNextRobotPosition, SALMotionProxy_getNextRobotPositionResponse, \
						SALMotionProxy_getRobotVelocity, SALMotionProxy_getRobotVelocityResponse, \
						SALMotionProxy_getWalkArmsEnabled, SALMotionProxy_getWalkArmsEnabledResponse, \
						SALMotionProxy_setWalkArmsEnabled, SALMotionProxy_setWalkArmsEnabledResponse, \
						SALMotionProxy_positionInterpolation, SALMotionProxy_positionInterpolationResponse, \
						SALMotionProxy_positionInterpolations, SALMotionProxy_positionInterpolationsResponse, \
						SALMotionProxy_setPosition, SALMotionProxy_setPositionResponse, \
						SALMotionProxy_changePosition, SALMotionProxy_changePositionResponse, \
						SALMotionProxy_getPosition, SALMotionProxy_getPositionResponse, \
						SALMotionProxy_transformInterpolation, SALMotionProxy_transformInterpolationResponse, \
						SALMotionProxy_transformInterpolations, SALMotionProxy_transformInterpolationsResponse, \
						SALMotionProxy_setTransform, SALMotionProxy_setTransformResponse, \
						SALMotionProxy_changeTransform, SALMotionProxy_changeTransformResponse, \
						SALMotionProxy_getTransform, SALMotionProxy_getTransformResponse, \
						SALMotionProxy_wbEnable, SALMotionProxy_wbEnableResponse, \
						SALMotionProxy_wbFootState, SALMotionProxy_wbFootStateResponse, \
						SALMotionProxy_wbEnableBalanceConstraint, SALMotionProxy_wbEnableBalanceConstraintResponse, \
						SALMotionProxy_wbGoToBalance, SALMotionProxy_wbGoToBalanceResponse, \
						SALMotionProxy_wbEnableEffectorControl, SALMotionProxy_wbEnableEffectorControlResponse, \
						SALMotionProxy_wbSetEffectorControl, SALMotionProxy_wbSetEffectorControlResponse, \
						SALMotionProxy_wbEnableEffectorOptimization, SALMotionProxy_wbEnableEffectorOptimizationResponse, \
						SALMotionProxy_setCollisionProtectionEnabled, SALMotionProxy_setCollisionProtectionEnabledResponse, \
						SALMotionProxy_getCollisionProtectionEnabled, SALMotionProxy_getCollisionProtectionEnabledResponse, \
						SALMotionProxy_isCollision, SALMotionProxy_isCollisionResponse, \
						SALMotionProxy_setFallManagerEnabled, SALMotionProxy_setFallManagerEnabledResponse, \
						SALMotionProxy_getFallManagerEnabled, SALMotionProxy_getFallManagerEnabledResponse, \
						SALMotionProxy_setSmartStiffnessEnabled, SALMotionProxy_setSmartStiffnessEnabledResponse, \
						SALMotionProxy_getSmartStiffnessEnabled, SALMotionProxy_getSmartStiffnessEnabledResponse, \
						SALMotionProxy_getBodyNames, SALMotionProxy_getBodyNamesResponse, \
						SALMotionProxy_getJointNames, SALMotionProxy_getJointNamesResponse, \
						SALMotionProxy_getSensorNames, SALMotionProxy_getSensorNamesResponse, \
						SALMotionProxy_getLimits, SALMotionProxy_getLimitsResponse, \
						SALMotionProxy_getMotionCycleTime, SALMotionProxy_getMotionCycleTimeResponse, \
						SALMotionProxy_getRobotConfig, SALMotionProxy_getRobotConfigResponse, \
						SALMotionProxy_getSummary, SALMotionProxy_getSummaryResponse, \
						SALMotionProxy_getMass, SALMotionProxy_getMassResponse, \
						SALMotionProxy_getCOM, SALMotionProxy_getCOMResponse, \
						SALMotionProxy_setMotionConfig, SALMotionProxy_setMotionConfigResponse, \
						SALMotionProxy_updateTrackerTarget, SALMotionProxy_updateTrackerTargetResponse, \
						SALMotionProxy_getTaskList, SALMotionProxy_getTaskListResponse, \
						SALMotionProxy_areResourcesAvailable, SALMotionProxy_areResourcesAvailableResponse, \
						SALMotionProxy_killTask, SALMotionProxy_killTaskResponse, \
						SALMotionProxy_killTasksUsingResources, SALMotionProxy_killTasksUsingResourcesResponse, \
						SALMotionProxy_killWalk, SALMotionProxy_killWalkResponse, \
						SALMotionProxy_killMove, SALMotionProxy_killMoveResponse, \
						SALMotionProxy_killAll, SALMotionProxy_killAllResponse

class ALProxy(object):
	def __init__(self, name, *rest):
		if name == "ALMotion": 
			self.ALMotionProxy_wakeUp = rospy.ServiceProxy("nao_ALMotionProxy_wakeUp", SALMotionProxy_wakeUp)
			self.ALMotionProxy_rest = rospy.ServiceProxy("nao_ALMotionProxy_rest", SALMotionProxy_rest)
			self.ALMotionProxy_stiffnessInterpolation = rospy.ServiceProxy("nao_ALMotionProxy_stiffnessInterpolation", SALMotionProxy_stiffnessInterpolation)
			self.ALMotionProxy_setStiffnesses = rospy.ServiceProxy("nao_ALMotionProxy_setStiffnesses", SALMotionProxy_setStiffnesses)
			self.ALMotionProxy_getStiffnesses = rospy.ServiceProxy("nao_ALMotionProxy_getStiffnesses", SALMotionProxy_getStiffnesses)
			self.ALMotionProxy_angleInterpolation = rospy.ServiceProxy("nao_ALMotionProxy_angleInterpolation", SALMotionProxy_angleInterpolation)
			self.ALMotionProxy_angleInterpolationWithSpeed = rospy.ServiceProxy("nao_ALMotionProxy_angleInterpolationWithSpeed", SALMotionProxy_angleInterpolationWithSpeed)
			self.ALMotionProxy_angleInterpolationBezier = rospy.ServiceProxy("nao_ALMotionProxy_angleInterpolationBezier", SALMotionProxy_angleInterpolationBezier)
			self.ALMotionProxy_setAngles = rospy.ServiceProxy("nao_ALMotionProxy_setAngles", SALMotionProxy_setAngles)
			self.ALMotionProxy_changeAngles = rospy.ServiceProxy("nao_ALMotionProxy_changeAngles", SALMotionProxy_changeAngles)
			self.ALMotionProxy_getAngles = rospy.ServiceProxy("nao_ALMotionProxy_getAngles", SALMotionProxy_getAngles)
			self.ALMotionProxy_closeHand = rospy.ServiceProxy("nao_ALMotionProxy_closeHand", SALMotionProxy_closeHand)
			self.ALMotionProxy_openHand = rospy.ServiceProxy("nao_ALMotionProxy_openHand", SALMotionProxy_openHand)
			self.ALMotionProxy_setWalkTargetVelocity = rospy.ServiceProxy("nao_ALMotionProxy_setWalkTargetVelocity", SALMotionProxy_setWalkTargetVelocity)
			self.ALMotionProxy_walkTo = rospy.ServiceProxy("nao_ALMotionProxy_walkTo", SALMotionProxy_walkTo)
			self.ALMotionProxy_move = rospy.ServiceProxy("nao_ALMotionProxy_move", SALMotionProxy_move)
			self.ALMotionProxy_moveTo = rospy.ServiceProxy("nao_ALMotionProxy_moveTo", SALMotionProxy_moveTo)
			self.ALMotionProxy_moveToward = rospy.ServiceProxy("nao_ALMotionProxy_moveToward", SALMotionProxy_moveToward)
			self.ALMotionProxy_setFootSteps = rospy.ServiceProxy("nao_ALMotionProxy_setFootSteps", SALMotionProxy_setFootSteps)
			self.ALMotionProxy_setFootStepsWithSpeed = rospy.ServiceProxy("nao_ALMotionProxy_setFootStepsWithSpeed", SALMotionProxy_setFootStepsWithSpeed)
			self.ALMotionProxy_getFootSteps = rospy.ServiceProxy("nao_ALMotionProxy_getFootSteps", SALMotionProxy_getFootSteps)
			self.ALMotionProxy_walkInit = rospy.ServiceProxy("nao_ALMotionProxy_walkInit", SALMotionProxy_walkInit)
			self.ALMotionProxy_moveInit = rospy.ServiceProxy("nao_ALMotionProxy_moveInit", SALMotionProxy_moveInit)
			self.ALMotionProxy_waitUntilWalkIsFinished = rospy.ServiceProxy("nao_ALMotionProxy_waitUntilWalkIsFinished", SALMotionProxy_waitUntilWalkIsFinished)
			self.ALMotionProxy_waitUntilMoveIsFinished = rospy.ServiceProxy("nao_ALMotionProxy_waitUntilMoveIsFinished", SALMotionProxy_waitUntilMoveIsFinished)
			self.ALMotionProxy_walkIsActive = rospy.ServiceProxy("nao_ALMotionProxy_walkIsActive", SALMotionProxy_walkIsActive)
			self.ALMotionProxy_moveIsActive = rospy.ServiceProxy("nao_ALMotionProxy_moveIsActive", SALMotionProxy_moveIsActive)
			self.ALMotionProxy_stopWalk = rospy.ServiceProxy("nao_ALMotionProxy_stopWalk", SALMotionProxy_stopWalk)
			self.ALMotionProxy_stopMove = rospy.ServiceProxy("nao_ALMotionProxy_stopMove", SALMotionProxy_stopMove)
			self.ALMotionProxy_getFootGaitConfig = rospy.ServiceProxy("nao_ALMotionProxy_getFootGaitConfig", SALMotionProxy_getFootGaitConfig)
			self.ALMotionProxy_getMoveConfig = rospy.ServiceProxy("nao_ALMotionProxy_getMoveConfig", SALMotionProxy_getMoveConfig)
			self.ALMotionProxy_getRobotPosition = rospy.ServiceProxy("nao_ALMotionProxy_getRobotPosition", SALMotionProxy_getRobotPosition)
			self.ALMotionProxy_getNextRobotPosition = rospy.ServiceProxy("nao_ALMotionProxy_getNextRobotPosition", SALMotionProxy_getNextRobotPosition)
			self.ALMotionProxy_getRobotVelocity = rospy.ServiceProxy("nao_ALMotionProxy_getRobotVelocity", SALMotionProxy_getRobotVelocity)
			self.ALMotionProxy_getWalkArmsEnabled = rospy.ServiceProxy("nao_ALMotionProxy_getWalkArmsEnabled", SALMotionProxy_getWalkArmsEnabled)
			self.ALMotionProxy_setWalkArmsEnabled = rospy.ServiceProxy("nao_ALMotionProxy_setWalkArmsEnabled", SALMotionProxy_setWalkArmsEnabled)
			self.ALMotionProxy_positionInterpolation = rospy.ServiceProxy("nao_ALMotionProxy_positionInterpolation", SALMotionProxy_positionInterpolation)
			self.ALMotionProxy_positionInterpolations = rospy.ServiceProxy("nao_ALMotionProxy_positionInterpolations", SALMotionProxy_positionInterpolations)
			self.ALMotionProxy_setPosition = rospy.ServiceProxy("nao_ALMotionProxy_setPosition", SALMotionProxy_setPosition)
			self.ALMotionProxy_changePosition = rospy.ServiceProxy("nao_ALMotionProxy_changePosition", SALMotionProxy_changePosition)
			self.ALMotionProxy_getPosition = rospy.ServiceProxy("nao_ALMotionProxy_getPosition", SALMotionProxy_getPosition)
			self.ALMotionProxy_transformInterpolation = rospy.ServiceProxy("nao_ALMotionProxy_transformInterpolation", SALMotionProxy_transformInterpolation)
			self.ALMotionProxy_transformInterpolations = rospy.ServiceProxy("nao_ALMotionProxy_transformInterpolations", SALMotionProxy_transformInterpolations)
			self.ALMotionProxy_setTransform = rospy.ServiceProxy("nao_ALMotionProxy_setTransform", SALMotionProxy_setTransform)
			self.ALMotionProxy_changeTransform = rospy.ServiceProxy("nao_ALMotionProxy_changeTransform", SALMotionProxy_changeTransform)
			self.ALMotionProxy_getTransform = rospy.ServiceProxy("nao_ALMotionProxy_getTransform", SALMotionProxy_getTransform)
			self.ALMotionProxy_wbEnable = rospy.ServiceProxy("nao_ALMotionProxy_wbEnable", SALMotionProxy_wbEnable)
			self.ALMotionProxy_wbFootState = rospy.ServiceProxy("nao_ALMotionProxy_wbFootState", SALMotionProxy_wbFootState)
			self.ALMotionProxy_wbEnableBalanceConstraint = rospy.ServiceProxy("nao_ALMotionProxy_wbEnableBalanceConstraint", SALMotionProxy_wbEnableBalanceConstraint)
			self.ALMotionProxy_wbGoToBalance = rospy.ServiceProxy("nao_ALMotionProxy_wbGoToBalance", SALMotionProxy_wbGoToBalance)
			self.ALMotionProxy_wbEnableEffectorControl = rospy.ServiceProxy("nao_ALMotionProxy_wbEnableEffectorControl", SALMotionProxy_wbEnableEffectorControl)
			self.ALMotionProxy_wbSetEffectorControl = rospy.ServiceProxy("nao_ALMotionProxy_wbSetEffectorControl", SALMotionProxy_wbSetEffectorControl)
			self.ALMotionProxy_wbEnableEffectorOptimization = rospy.ServiceProxy("nao_ALMotionProxy_wbEnableEffectorOptimization", SALMotionProxy_wbEnableEffectorOptimization)
			self.ALMotionProxy_setCollisionProtectionEnabled = rospy.ServiceProxy("nao_ALMotionProxy_setCollisionProtectionEnabled", SALMotionProxy_setCollisionProtectionEnabled)
			self.ALMotionProxy_getCollisionProtectionEnabled = rospy.ServiceProxy("nao_ALMotionProxy_getCollisionProtectionEnabled", SALMotionProxy_getCollisionProtectionEnabled)
			self.ALMotionProxy_isCollision = rospy.ServiceProxy("nao_ALMotionProxy_isCollision", SALMotionProxy_isCollision)
			self.ALMotionProxy_setFallManagerEnabled = rospy.ServiceProxy("nao_ALMotionProxy_setFallManagerEnabled", SALMotionProxy_setFallManagerEnabled)
			self.ALMotionProxy_getFallManagerEnabled = rospy.ServiceProxy("nao_ALMotionProxy_getFallManagerEnabled", SALMotionProxy_getFallManagerEnabled)
			self.ALMotionProxy_setSmartStiffnessEnabled = rospy.ServiceProxy("nao_ALMotionProxy_setSmartStiffnessEnabled", SALMotionProxy_setSmartStiffnessEnabled)
			self.ALMotionProxy_getSmartStiffnessEnabled = rospy.ServiceProxy("nao_ALMotionProxy_getSmartStiffnessEnabled", SALMotionProxy_getSmartStiffnessEnabled)
			self.ALMotionProxy_getBodyNames = rospy.ServiceProxy("nao_ALMotionProxy_getBodyNames", SALMotionProxy_getBodyNames)
			self.ALMotionProxy_getJointNames = rospy.ServiceProxy("nao_ALMotionProxy_getJointNames", SALMotionProxy_getJointNames)
			self.ALMotionProxy_getSensorNames = rospy.ServiceProxy("nao_ALMotionProxy_getSensorNames", SALMotionProxy_getSensorNames)
			self.ALMotionProxy_getLimits = rospy.ServiceProxy("nao_ALMotionProxy_getLimits", SALMotionProxy_getLimits)
			self.ALMotionProxy_getMotionCycleTime = rospy.ServiceProxy("nao_ALMotionProxy_getMotionCycleTime", SALMotionProxy_getMotionCycleTime)
			self.ALMotionProxy_getRobotConfig = rospy.ServiceProxy("nao_ALMotionProxy_getRobotConfig", SALMotionProxy_getRobotConfig)
			self.ALMotionProxy_getSummary = rospy.ServiceProxy("nao_ALMotionProxy_getSummary", SALMotionProxy_getSummary)
			self.ALMotionProxy_getMass = rospy.ServiceProxy("nao_ALMotionProxy_getMass", SALMotionProxy_getMass)
			self.ALMotionProxy_getCOM = rospy.ServiceProxy("nao_ALMotionProxy_getCOM", SALMotionProxy_getCOM)
			self.ALMotionProxy_setMotionConfig = rospy.ServiceProxy("nao_ALMotionProxy_setMotionConfig", SALMotionProxy_setMotionConfig)
			self.ALMotionProxy_updateTrackerTarget = rospy.ServiceProxy("nao_ALMotionProxy_updateTrackerTarget", SALMotionProxy_updateTrackerTarget)
			self.ALMotionProxy_getTaskList = rospy.ServiceProxy("nao_ALMotionProxy_getTaskList", SALMotionProxy_getTaskList)
			self.ALMotionProxy_areResourcesAvailable = rospy.ServiceProxy("nao_ALMotionProxy_areResourcesAvailable", SALMotionProxy_areResourcesAvailable)
			self.ALMotionProxy_killTask = rospy.ServiceProxy("nao_ALMotionProxy_killTask", SALMotionProxy_killTask)
			self.ALMotionProxy_killTasksUsingResources = rospy.ServiceProxy("nao_ALMotionProxy_killTasksUsingResources", SALMotionProxy_killTasksUsingResources)
			self.ALMotionProxy_killWalk = rospy.ServiceProxy("nao_ALMotionProxy_killWalk", SALMotionProxy_killWalk)
			self.ALMotionProxy_killMove = rospy.ServiceProxy("nao_ALMotionProxy_killMove", SALMotionProxy_killMove)
			self.ALMotionProxy_killAll = rospy.ServiceProxy("nao_ALMotionProxy_killAll", SALMotionProxy_killAll)
 

	def wakeUp(self, *msg):
		return self.ALMotionProxy_wakeUp(msg).reply


	def rest(self, *msg):
		return self.ALMotionProxy_rest(msg).reply


	def stiffnessInterpolation(self, *msg):
		return self.ALMotionProxy_stiffnessInterpolation(msg).reply


	def setStiffnesses(self, *msg):
		return self.ALMotionProxy_setStiffnesses(msg).reply


	def getStiffnesses(self, *msg):
		return self.ALMotionProxy_getStiffnesses(msg).reply


	def angleInterpolation(self, *msg):
		return self.ALMotionProxy_angleInterpolation(msg).reply


	def angleInterpolationWithSpeed(self, *msg):
		return self.ALMotionProxy_angleInterpolationWithSpeed(msg).reply


	def angleInterpolationBezier(self, *msg):
		return self.ALMotionProxy_angleInterpolationBezier(msg).reply


	def setAngles(self, *msg):
		return self.ALMotionProxy_setAngles(msg).reply


	def changeAngles(self, *msg):
		return self.ALMotionProxy_changeAngles(msg).reply


	def getAngles(self, *msg):
		return self.ALMotionProxy_getAngles(msg).reply


	def closeHand(self, *msg):
		return self.ALMotionProxy_closeHand(msg).reply


	def openHand(self, *msg):
		return self.ALMotionProxy_openHand(msg).reply


	def setWalkTargetVelocity(self, *msg):
		return self.ALMotionProxy_setWalkTargetVelocity(msg).reply


	def walkTo(self, *msg):
		return self.ALMotionProxy_walkTo(msg).reply


	def move(self, *msg):
		return self.ALMotionProxy_move(msg).reply


	def moveTo(self, *msg):
		return self.ALMotionProxy_moveTo(msg).reply


	def moveToward(self, *msg):
		return self.ALMotionProxy_moveToward(msg).reply


	def setFootSteps(self, *msg):
		return self.ALMotionProxy_setFootSteps(msg).reply


	def setFootStepsWithSpeed(self, *msg):
		return self.ALMotionProxy_setFootStepsWithSpeed(msg).reply


	def getFootSteps(self, *msg):
		return self.ALMotionProxy_getFootSteps(msg).reply


	def walkInit(self, *msg):
		return self.ALMotionProxy_walkInit(msg).reply


	def moveInit(self, *msg):
		return self.ALMotionProxy_moveInit(msg).reply


	def waitUntilWalkIsFinished(self, *msg):
		return self.ALMotionProxy_waitUntilWalkIsFinished(msg).reply


	def waitUntilMoveIsFinished(self, *msg):
		return self.ALMotionProxy_waitUntilMoveIsFinished(msg).reply


	def walkIsActive(self, *msg):
		return self.ALMotionProxy_walkIsActive(msg).reply


	def moveIsActive(self, *msg):
		return self.ALMotionProxy_moveIsActive(msg).reply


	def stopWalk(self, *msg):
		return self.ALMotionProxy_stopWalk(msg).reply


	def stopMove(self, *msg):
		return self.ALMotionProxy_stopMove(msg).reply


	def getFootGaitConfig(self, *msg):
		return self.ALMotionProxy_getFootGaitConfig(msg).reply


	def getMoveConfig(self, *msg):
		return self.ALMotionProxy_getMoveConfig(msg).reply


	def getRobotPosition(self, *msg):
		return self.ALMotionProxy_getRobotPosition(msg).reply


	def getNextRobotPosition(self, *msg):
		return self.ALMotionProxy_getNextRobotPosition(msg).reply


	def getRobotVelocity(self, *msg):
		return self.ALMotionProxy_getRobotVelocity(msg).reply


	def getWalkArmsEnabled(self, *msg):
		return self.ALMotionProxy_getWalkArmsEnabled(msg).reply


	def setWalkArmsEnabled(self, *msg):
		return self.ALMotionProxy_setWalkArmsEnabled(msg).reply


	def positionInterpolation(self, *msg):
		return self.ALMotionProxy_positionInterpolation(msg).reply


	def positionInterpolations(self, *msg):
		return self.ALMotionProxy_positionInterpolations(msg).reply


	def setPosition(self, *msg):
		return self.ALMotionProxy_setPosition(msg).reply


	def changePosition(self, *msg):
		return self.ALMotionProxy_changePosition(msg).reply


	def getPosition(self, *msg):
		return self.ALMotionProxy_getPosition(msg).reply


	def transformInterpolation(self, *msg):
		return self.ALMotionProxy_transformInterpolation(msg).reply


	def transformInterpolations(self, *msg):
		return self.ALMotionProxy_transformInterpolations(msg).reply


	def setTransform(self, *msg):
		return self.ALMotionProxy_setTransform(msg).reply


	def changeTransform(self, *msg):
		return self.ALMotionProxy_changeTransform(msg).reply


	def getTransform(self, *msg):
		return self.ALMotionProxy_getTransform(msg).reply


	def wbEnable(self, *msg):
		return self.ALMotionProxy_wbEnable(msg).reply


	def wbFootState(self, *msg):
		return self.ALMotionProxy_wbFootState(msg).reply


	def wbEnableBalanceConstraint(self, *msg):
		return self.ALMotionProxy_wbEnableBalanceConstraint(msg).reply


	def wbGoToBalance(self, *msg):
		return self.ALMotionProxy_wbGoToBalance(msg).reply


	def wbEnableEffectorControl(self, *msg):
		return self.ALMotionProxy_wbEnableEffectorControl(msg).reply


	def wbSetEffectorControl(self, *msg):
		return self.ALMotionProxy_wbSetEffectorControl(msg).reply


	def wbEnableEffectorOptimization(self, *msg):
		return self.ALMotionProxy_wbEnableEffectorOptimization(msg).reply


	def setCollisionProtectionEnabled(self, *msg):
		return self.ALMotionProxy_setCollisionProtectionEnabled(msg).reply


	def getCollisionProtectionEnabled(self, *msg):
		return self.ALMotionProxy_getCollisionProtectionEnabled(msg).reply


	def isCollision(self, *msg):
		return self.ALMotionProxy_isCollision(msg).reply


	def setFallManagerEnabled(self, *msg):
		return self.ALMotionProxy_setFallManagerEnabled(msg).reply


	def getFallManagerEnabled(self, *msg):
		return self.ALMotionProxy_getFallManagerEnabled(msg).reply


	def setSmartStiffnessEnabled(self, *msg):
		return self.ALMotionProxy_setSmartStiffnessEnabled(msg).reply


	def getSmartStiffnessEnabled(self, *msg):
		return self.ALMotionProxy_getSmartStiffnessEnabled(msg).reply


	def getBodyNames(self, *msg):
		return self.ALMotionProxy_getBodyNames(msg).reply


	def getJointNames(self, *msg):
		return self.ALMotionProxy_getJointNames(msg).reply


	def getSensorNames(self, *msg):
		return self.ALMotionProxy_getSensorNames(msg).reply


	def getLimits(self, *msg):
		return self.ALMotionProxy_getLimits(msg).reply


	def getMotionCycleTime(self, *msg):
		return self.ALMotionProxy_getMotionCycleTime(msg).reply


	def getRobotConfig(self, *msg):
		return self.ALMotionProxy_getRobotConfig(msg).reply


	def getSummary(self, *msg):
		return self.ALMotionProxy_getSummary(msg).reply


	def getMass(self, *msg):
		return self.ALMotionProxy_getMass(msg).reply


	def getCOM(self, *msg):
		return self.ALMotionProxy_getCOM(msg).reply


	def setMotionConfig(self, *msg):
		return self.ALMotionProxy_setMotionConfig(msg).reply


	def updateTrackerTarget(self, *msg):
		return self.ALMotionProxy_updateTrackerTarget(msg).reply


	def getTaskList(self, *msg):
		return self.ALMotionProxy_getTaskList(msg).reply


	def areResourcesAvailable(self, *msg):
		return self.ALMotionProxy_areResourcesAvailable(msg).reply


	def killTask(self, *msg):
		return self.ALMotionProxy_killTask(msg).reply


	def killTasksUsingResources(self, *msg):
		return self.ALMotionProxy_killTasksUsingResources(msg).reply


	def killWalk(self, *msg):
		return self.ALMotionProxy_killWalk(msg).reply


	def killMove(self, *msg):
		return self.ALMotionProxy_killMove(msg).reply


	def killAll(self, *msg):
		return self.ALMotionProxy_killAll(msg).reply



if __name__ == "__main__":
	import almath
	rospy.init_node('naoqi_wrapper_test')
	motionProxy = ALProxy("ALMotion", "127.0.0.1", 9559)
	names  = ["HeadYaw","HeadPitch"]
	angleLists  = [[50.0*almath.TO_RAD, 0.0],
				[-30.0*almath.TO_RAD, 30.0*almath.TO_RAD, 0.0]]
	timeLists   = [[1.0, 2.0], [ 1.0, 2.0, 3.0]]
	isAbsolute  = True
	reply = motionProxy.angleInterpolation(names, angleLists, timeLists, isAbsolute)
	print reply
























