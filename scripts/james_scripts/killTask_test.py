#!/usr/bin/env python

import sys
import time
import rospy
from plancs import *

def main(robotIP):
	PORT = 9559

	try:
		motionProxy = PALProxy("ALMotion", robotIP, PORT)
	except Exception,e:
		print "Could not create proxy to ALMotion"
		print "Error was: ",e
		sys.exit(1)

	#motionProxy.setStiffnesses("Head", 1.0)

	# Example showing how to killTasksUsingResources
	# We will create a task first, so that we see a result
	name = "HeadYaw"
	#motionProxy.post.angleInterpolation(name, 1.0, 1.0, True)
	time.sleep(0.5)
	motionProxy.killTasksUsingResources([name])

	#motionProxy.setStiffnesses("Head", 0.0)


if __name__ == "__main__":
	rospy.init_node('naoqi_wrapper_test')
	robotIp = "127.0.0.1"

	if len(sys.argv) <= 1:
		print "Usage python almotion_killtasksusingresources.py robotIP (optional default: 127.0.0.1)"
	else:
		robotIp = sys.argv[1]

	main(robotIp)
