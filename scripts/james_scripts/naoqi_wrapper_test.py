#!/usr/bin/env python

import sys
from plancs import *
import rospy


def main():
    rospy.init_node("test")

    motionProxy = PALProxy("ALMotion")

    # Example showing how to get the Body stiffnesses
    jointName   = "Body"
    motionProxy.setStiffnesses(jointName, 1.0)
    motionProxy.move(1.0, 0.0, 0.0)
    #stiffnesses = motionProxy.getStiffnesses(jointName)
    
    print "Body Stiffnesses:"
    #print stiffnesses

if __name__ == "__main__":
    main()
