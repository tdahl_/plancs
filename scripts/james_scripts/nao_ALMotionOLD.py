#!/usr/bin/env python

import os
import sys

import roslib
roslib.load_manifest('plancs')
import rospy

from naoqi import ALProxy

from std_srvs.srv import Empty, EmptyResponse

from plancs.srv import *


class Nao():
	def __init__(self, name):
	
		# ROS initialization:
		rospy.init_node(name)
		rospy.loginfo("%s starting...", name)
		argv = []
		argv = rospy.myargv(argv=sys.argv)
		if len(argv)>=2:
			nao_ip=argv[1].split('--pip=')[1]
			nao_port=int(argv[2].split('--pport=')[1])
		else:
			nao_ip="127.0.0.1"
			nao_port=9559
		rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
		try:
			self.motionProxy = ALProxy("ALMotion", nao_ip, nao_port)
		except RuntimeError, e:
			rospy.logerr("Could not create Proxy to ALMotion, exiting. \nException message:\n%s", e)
			exit(1)
		
		# start services:
		rospy.loginfo("ALMotion services going up...")
################
		rospy.Service("nao_ALMotion_wakeUp", SALMotionProxy_wakeUp, self.handleALMotionProxy_wakeUp)
		rospy.Service("nao_ALMotion_rest", SALMotionProxy_rest, self.handleALMotionProxy_rest)
		rospy.Service("nao_ALMotion_stiffnessInterpolation", SALMotionProxy_stiffnessInterpolation, self.handleALMotionProxy_stiffnessInterpolation)
		rospy.Service("nao_ALMotion_setStiffnesses", SALMotionProxy_setStiffnesses, self.handleALMotionProxy_setStiffnesses)
		rospy.Service("nao_ALMotion_getStiffnesses", SALMotionProxy_getStiffnesses, self.handleALMotionProxy_getStiffnesses)
		rospy.Service("nao_ALMotion_angleInterpolation", SALMotionProxy_angleInterpolation, self.handleALMotionProxy_angleInterpolation)
		rospy.Service("nao_ALMotion_angleInterpolationWithSpeed", SALMotionProxy_angleInterpolationWithSpeed, self.handleALMotionProxy_angleInterpolationWithSpeed)
		rospy.Service("nao_ALMotion_angleInterpolationBezier", SALMotionProxy_angleInterpolationBezier, self.handleALMotionProxy_angleInterpolationBezier)
		rospy.Service("nao_ALMotion_setAngles", SALMotionProxy_setAngles, self.handleALMotionProxy_setAngles)
		rospy.Service("nao_ALMotion_changeAngles", SALMotionProxy_changeAngles, self.handleALMotionProxy_changeAngles)
		rospy.Service("nao_ALMotion_getAngles", SALMotionProxy_getAngles, self.handleALMotionProxy_getAngles)
		rospy.Service("nao_ALMotion_closeHand", SALMotionProxy_closeHand, self.handleALMotionProxy_closeHand)
		rospy.Service("nao_ALMotion_openHand", SALMotionProxy_openHand, self.handleALMotionProxy_openHand)
		rospy.Service("nao_ALMotion_setWalkTargetVelocity", SALMotionProxy_setWalkTargetVelocity, self.handleALMotionProxy_setWalkTargetVelocity)
		rospy.Service("nao_ALMotion_walkTo", SALMotionProxy_walkTo, self.handleALMotionProxy_walkTo)
		rospy.Service("nao_ALMotion_move", SALMotionProxy_move, self.handleALMotionProxy_move)
		rospy.Service("nao_ALMotion_moveTo", SALMotionProxy_moveTo, self.handleALMotionProxy_moveTo)
		rospy.Service("nao_ALMotion_moveToward", SALMotionProxy_moveToward, self.handleALMotionProxy_moveToward)
		rospy.Service("nao_ALMotion_setFootSteps", SALMotionProxy_setFootSteps, self.handleALMotionProxy_setFootSteps)
		rospy.Service("nao_ALMotion_setFootStepsWithSpeed", SALMotionProxy_setFootStepsWithSpeed, self.handleALMotionProxy_setFootStepsWithSpeed)
		rospy.Service("nao_ALMotion_getFootSteps", SALMotionProxy_getFootSteps, self.handleALMotionProxy_getFootSteps)
		rospy.Service("nao_ALMotion_walkInit", SALMotionProxy_walkInit, self.handleALMotionProxy_walkInit)
		rospy.Service("nao_ALMotion_moveInit", SALMotionProxy_moveInit, self.handleALMotionProxy_moveInit)
		rospy.Service("nao_ALMotion_waitUntilWalkIsFinished", SALMotionProxy_waitUntilWalkIsFinished, self.handleALMotionProxy_waitUntilWalkIsFinished)
		rospy.Service("nao_ALMotion_waitUntilMoveIsFinished", SALMotionProxy_waitUntilMoveIsFinished, self.handleALMotionProxy_waitUntilMoveIsFinished)
		rospy.Service("nao_ALMotion_walkIsActive", SALMotionProxy_walkIsActive, self.handleALMotionProxy_walkIsActive)
		rospy.Service("nao_ALMotion_moveIsActive", SALMotionProxy_moveIsActive, self.handleALMotionProxy_moveIsActive)
		rospy.Service("nao_ALMotion_stopWalk", SALMotionProxy_stopWalk, self.handleALMotionProxy_stopWalk)
		rospy.Service("nao_ALMotion_stopMove", SALMotionProxy_stopMove, self.handleALMotionProxy_stopMove)
		rospy.Service("nao_ALMotion_getFootGaitConfig", SALMotionProxy_getFootGaitConfig, self.handleALMotionProxy_getFootGaitConfig)
		rospy.Service("nao_ALMotion_getMoveConfig", SALMotionProxy_getMoveConfig, self.handleALMotionProxy_getMoveConfig)
		rospy.Service("nao_ALMotion_getRobotPosition", SALMotionProxy_getRobotPosition, self.handleALMotionProxy_getRobotPosition)
		rospy.Service("nao_ALMotion_getNextRobotPosition", SALMotionProxy_getNextRobotPosition, self.handleALMotionProxy_getNextRobotPosition)
		rospy.Service("nao_ALMotion_getRobotVelocity", SALMotionProxy_getRobotVelocity, self.handleALMotionProxy_getRobotVelocity)
		rospy.Service("nao_ALMotion_getWalkArmsEnabled", SALMotionProxy_getWalkArmsEnabled, self.handleALMotionProxy_getWalkArmsEnabled)
		rospy.Service("nao_ALMotion_setWalkArmsEnabled", SALMotionProxy_setWalkArmsEnabled, self.handleALMotionProxy_setWalkArmsEnabled)
		rospy.Service("nao_ALMotion_positionInterpolation", SALMotionProxy_positionInterpolation, self.handleALMotionProxy_positionInterpolation)
		rospy.Service("nao_ALMotion_positionInterpolations", SALMotionProxy_positionInterpolations, self.handleALMotionProxy_positionInterpolations)
		rospy.Service("nao_ALMotion_setPosition", SALMotionProxy_setPosition, self.handleALMotionProxy_setPosition)
		rospy.Service("nao_ALMotion_changePosition", SALMotionProxy_changePosition, self.handleALMotionProxy_changePosition)
		rospy.Service("nao_ALMotion_getPosition", SALMotionProxy_getPosition, self.handleALMotionProxy_getPosition)
		rospy.Service("nao_ALMotion_transformInterpolation", SALMotionProxy_transformInterpolation, self.handleALMotionProxy_transformInterpolation)
		rospy.Service("nao_ALMotion_transformInterpolations", SALMotionProxy_transformInterpolations, self.handleALMotionProxy_transformInterpolations)
		rospy.Service("nao_ALMotion_setTransform", SALMotionProxy_setTransform, self.handleALMotionProxy_setTransform)
		rospy.Service("nao_ALMotion_changeTransform", SALMotionProxy_changeTransform, self.handleALMotionProxy_changeTransform)
		rospy.Service("nao_ALMotion_getTransform", SALMotionProxy_getTransform, self.handleALMotionProxy_getTransform)
		rospy.Service("nao_ALMotion_wbEnable", SALMotionProxy_wbEnable, self.handleALMotionProxy_wbEnable)
		rospy.Service("nao_ALMotion_wbFootState", SALMotionProxy_wbFootState, self.handleALMotionProxy_wbFootState)
		rospy.Service("nao_ALMotion_wbEnableBalanceConstraint", SALMotionProxy_wbEnableBalanceConstraint, self.handleALMotionProxy_wbEnableBalanceConstraint)
		rospy.Service("nao_ALMotion_wbGoToBalance", SALMotionProxy_wbGoToBalance, self.handleALMotionProxy_wbGoToBalance)
		rospy.Service("nao_ALMotion_wbEnableEffectorControl", SALMotionProxy_wbEnableEffectorControl, self.handleALMotionProxy_wbEnableEffectorControl)
		rospy.Service("nao_ALMotion_wbSetEffectorControl", SALMotionProxy_wbSetEffectorControl, self.handleALMotionProxy_wbSetEffectorControl)
		rospy.Service("nao_ALMotion_wbEnableEffectorOptimization", SALMotionProxy_wbEnableEffectorOptimization, self.handleALMotionProxy_wbEnableEffectorOptimization)
		rospy.Service("nao_ALMotion_setCollisionProtectionEnabled", SALMotionProxy_setCollisionProtectionEnabled, self.handleALMotionProxy_setCollisionProtectionEnabled)
		rospy.Service("nao_ALMotion_getCollisionProtectionEnabled", SALMotionProxy_getCollisionProtectionEnabled, self.handleALMotionProxy_getCollisionProtectionEnabled)
		rospy.Service("nao_ALMotion_isCollision", SALMotionProxy_isCollision, self.handleALMotionProxy_isCollision)
		rospy.Service("nao_ALMotion_setFallManagerEnabled", SALMotionProxy_setFallManagerEnabled, self.handleALMotionProxy_setFallManagerEnabled)
		rospy.Service("nao_ALMotion_getFallManagerEnabled", SALMotionProxy_getFallManagerEnabled, self.handleALMotionProxy_getFallManagerEnabled)
		rospy.Service("nao_ALMotion_setSmartStiffnessEnabled", SALMotionProxy_setSmartStiffnessEnabled, self.handleALMotionProxy_setSmartStiffnessEnabled)
		rospy.Service("nao_ALMotion_getSmartStiffnessEnabled", SALMotionProxy_getSmartStiffnessEnabled, self.handleALMotionProxy_getSmartStiffnessEnabled)
		rospy.Service("nao_ALMotion_getBodyNames", SALMotionProxy_getBodyNames, self.handleALMotionProxy_getBodyNames)
		rospy.Service("nao_ALMotion_getJointNames", SALMotionProxy_getJointNames, self.handleALMotionProxy_getJointNames)
		rospy.Service("nao_ALMotion_getSensorNames", SALMotionProxy_getSensorNames, self.handleALMotionProxy_getSensorNames)
		rospy.Service("nao_ALMotion_getLimits", SALMotionProxy_getLimits, self.handleALMotionProxy_getLimits)
		rospy.Service("nao_ALMotion_getMotionCycleTime", SALMotionProxy_getMotionCycleTime, self.handleALMotionProxy_getMotionCycleTime)
		rospy.Service("nao_ALMotion_getRobotConfig", SALMotionProxy_getRobotConfig, self.handleALMotionProxy_getRobotConfig)
		rospy.Service("nao_ALMotion_getSummary", SALMotionProxy_getSummary, self.handleALMotionProxy_getSummary)
		rospy.Service("nao_ALMotion_getMass", SALMotionProxy_getMass, self.handleALMotionProxy_getMass)
		rospy.Service("nao_ALMotion_getCOM", SALMotionProxy_getCOM, self.handleALMotionProxy_getCOM)
		rospy.Service("nao_ALMotion_setMotionConfig", SALMotionProxy_setMotionConfig, self.handleALMotionProxy_setMotionConfig)
		rospy.Service("nao_ALMotion_updateTrackerTarget", SALMotionProxy_updateTrackerTarget, self.handleALMotionProxy_updateTrackerTarget)
		rospy.Service("nao_ALMotion_getTaskList", SALMotionProxy_getTaskList, self.handleALMotionProxy_getTaskList)
		rospy.Service("nao_ALMotion_areResourcesAvailable", SALMotionProxy_areResourcesAvailable, self.handleALMotionProxy_areResourcesAvailable)
		rospy.Service("nao_ALMotion_killTask", SALMotionProxy_killTask, self.handleALMotionProxy_killTask)
		rospy.Service("nao_ALMotion_killTasksUsingResources", SALMotionProxy_killTasksUsingResources, self.handleALMotionProxy_killTasksUsingResources)
		rospy.Service("nao_ALMotion_killWalk", SALMotionProxy_killWalk, self.handleALMotionProxy_killWalk)
		rospy.Service("nao_ALMotion_killMove", SALMotionProxy_killMove, self.handleALMotionProxy_killMove)
		rospy.Service("nao_ALMotion_killAll", SALMotionProxy_killAll, self.handleALMotionProxy_killAll)
################
		rospy.loginfo("Initiated.")

		rospy.loginfo("%s initialized", name)

################ MUST TAKE MSG IN!
	def handleALMotionProxy_wakeUp(self, msg):
		try:
			self.motionProxy.wakeUp()
			who = msg._connection_header['callerid']
			rospy.loginfo("wakeUp() from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wakeUpResponse()

	def handleALMotionProxy_rest(self, msg):
		try:
			self.motionProxy.rest()
			who = msg._connection_header['callerid']
			rospy.loginfo("rest() from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_restResponse()

	def handleALMotionProxy_stiffnessInterpolation(self, msg):
		names = make_string_array(msg.names)
		stiffnessLists = make_float_array(msg.stiffnessLists)
		timeLists = make_float_array(msg.timeLists)
		try:
			self.motionProxy.stiffnessInterpolation(names, stiffnessLists, timeLists)
			who = msg._connection_header['callerid']
			rospy.loginfo("stiffnessInterpolation(%s, %s, %s)", str(msg.names), str(stiffList), str(durations))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_stiffnessInterpolationResponse()

	def handleALMotionProxy_setStiffnesses(self, msg):
		names = make_string_array(msg.names)
		stiffnesses = make_float_array(msg.stiffnesses)
		try:
			self.motionProxy.setStiffnesses(names, stiffnesses)
			who = msg._connection_header['callerid']
			rospy.loginfo("setStiffnesses(%s, %s) from: "+who, str(msg.names), str(msg.stiffnesses))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setStiffnessesResponse()

	def handleALMotionProxy_getStiffnesses(self, msg):
		jointName = make_string_array(msg.jointName)
		try:
			reply = self.motionProxy.getStiffnesses(jointName)
			who = msg._connection_header['callerid']
			rospy.loginfo("getStiffnesses(%s) from: "+who,str(msg.jointName))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getStiffnessesResponse(str(reply))

	def handleALMotionProxy_angleInterpolation(self, msg):

		names = make_string_array(msg.names)
		angleLists = make_float_array(msg.angleLists)
		timeLists = make_float_array(msg.timeLists)

		try:
			if msg.post:
				self.motionProxy.post.angleInterpolation(names, angleLists, timeLists, msg.isAbsolute)
				who = msg._connection_header['callerid']
				rospy.loginfo("angleInterpolation(%s, %s,%s, %s, non-blocking) from: "+who, str(msg.names), str(msg.angleLists), str(msg.timeLists), str(msg.isAbsolute))
			else:
				self.motionProxy.angleInterpolation(names, angleLists, timeLists, msg.isAbsolute)
				who = msg._connection_header['callerid']
				rospy.loginfo("angleInterpolation(%s, %s,%s, %s) from: "+who, str(msg.names), str(msg.angleLists), str(msg.timeLists), str(msg.isAbsolute))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_angleInterpolationResponse()

	def handleALMotionProxy_angleInterpolationWithSpeed(self, msg):
		try:
			self.motionProxy.angleInterpolationWithSpeed()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_angleInterpolationWithSpeedResponse()

	def handleALMotionProxy_angleInterpolationBezier(self, msg):
		try:
			self.motionProxy.angleInterpolationBezier()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_angleInterpolationBezierResponse()

	def handleALMotionProxy_setAngles(self, msg):
		names = make_string_array(msg.names)
		angles = make_float_array(msg.angles)
		try:
			self.motionProxy.setAngles(names, angles, msg.fractionMaxSpeed)
			who = msg._connection_header['callerid']
			rospy.loginfo("setAngles(%s, %s, %s) from: "+who, str(msg.names), str(msg.angles), str(msg.fractionMaxSpeed))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setAnglesResponse()

	def handleALMotionProxy_changeAngles(self, msg):
		try:
			self.motionProxy.changeAngles()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_changeAnglesResponse()

	def handleALMotionProxy_getAngles(self, msg):
		try:
			self.motionProxy.getAngles()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getAnglesResponse()

	def handleALMotionProxy_closeHand(self, msg):
		try:
			self.motionProxy.closeHand()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_closeHandResponse()

	def handleALMotionProxy_openHand(self, msg):
		try:
			self.motionProxy.openHand()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_openHandResponse()

	def handleALMotionProxy_setWalkTargetVelocity(self, msg):
		try:
			self.motionProxy.setWalkTargetVelocity()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setWalkTargetVelocityResponse()

	def handleALMotionProxy_walkTo(self, msg):
		try:
			self.motionProxy.walkTo()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_walkToResponse()

	def handleALMotionProxy_move(self, msg):
		if msg.moveConfig == "NOMOVECONFIG":
			pass
			# do something with moveConfig
		try:
			if msg.post:
				self.motionProxy.post.move(msg.x, msg.y, msg.theta)
				who = msg._connection_header['callerid']
				rospy.loginfo("move(%s, %s, %s, non-blocking) from: "+who, str(msg.x), str(msg.y), str(msg.theta))
			else:
				self.motionProxy.move(msg.x, msg.y, msg.theta)
				who = msg._connection_header['callerid']
				rospy.loginfo("move(%s, %s, %s) from: "+who, str(msg.x), str(msg.y), str(msg.theta))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_moveResponse()

	def handleALMotionProxy_moveTo(self, msg):
		try:
			if msg.post:
				self.motionProxy.post.moveTo(msg.x, msg.y, msg.theta)
				who = msg._connection_header['callerid']
				rospy.loginfo("moveTo(%s, %s, %s, non-blocking) from: "+who, str(msg.x), str(msg.y), str(msg.theta))
			else:
				self.motionProxy.moveTo(msg.x, msg.y, msg.theta)
				who = msg._connection_header['callerid']
				rospy.loginfo("moveTo(%s, %s, %s) from: "+who, str(msg.x), str(msg.y), str(msg.theta))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_moveToResponse()

	def handleALMotionProxy_moveToward(self, msg):
		try:
			self.motionProxy.moveToward()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_moveTowardResponse()

	def handleALMotionProxy_setFootSteps(self, msg):
		try:
			self.motionProxy.setFootSteps()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setFootStepsResponse()

	def handleALMotionProxy_setFootStepsWithSpeed(self, msg):
		try:
			self.motionProxy.setFootStepsWithSpeed()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setFootStepsWithSpeedResponse()

	def handleALMotionProxy_getFootSteps(self, msg):
		try:
			self.motionProxy.getFootSteps()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getFootStepsResponse()

	def handleALMotionProxy_walkInit(self, msg):
		try:
			self.motionProxy.walkInit()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_walkInitResponse()

	def handleALMotionProxy_moveInit(self, msg):
		try:
			self.motionProxy.moveInit()
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_moveInitResponse()

	def handleALMotionProxy_waitUntilWalkIsFinished(self, msg):
		try:
			self.motionProxy.waitUntilWalkIsFinished()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_waitUntilWalkIsFinishedResponse()

	def handleALMotionProxy_waitUntilMoveIsFinished(self, msg):
		try:
			self.motionProxy.waitUntilMoveIsFinished()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_waitUntilMoveIsFinishedResponse()

	def handleALMotionProxy_walkIsActive(self, msg):
		try:
			self.motionProxy.walkIsActive()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_walkIsActiveResponse()

	def handleALMotionProxy_moveIsActive(self, msg):
		reply = False
		try:
			reply = self.motionProxy.moveIsActive()
			who = msg._connection_header['callerid']
			rospy.loginfo("moveIsActive() from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_moveIsActiveResponse(reply)

	def handleALMotionProxy_stopWalk(self, msg):
		try:
			self.motionProxy.stopWalk()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_stopWalkResponse()

	def handleALMotionProxy_stopMove(self, msg):
		try:
			self.motionProxy.stopMove()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_stopMoveResponse()

	def handleALMotionProxy_getFootGaitConfig(self, msg):
		try:
			self.motionProxy.getFootGaitConfig()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getFootGaitConfigResponse()

	def handleALMotionProxy_getMoveConfig(self, msg):
		try:
			self.motionProxy.getMoveConfig()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getMoveConfigResponse()

	def handleALMotionProxy_getRobotPosition(self, msg):
		try:
			self.motionProxy.getRobotPosition()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getRobotPositionResponse()

	def handleALMotionProxy_getNextRobotPosition(self, msg):
		try:
			self.motionProxy.getNextRobotPosition()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getNextRobotPositionResponse()

	def handleALMotionProxy_getRobotVelocity(self, msg):
		try:
			self.motionProxy.getRobotVelocity()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getRobotVelocityResponse()

	def handleALMotionProxy_getWalkArmsEnabled(self, msg):
		try:
			self.motionProxy.getWalkArmsEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getWalkArmsEnabledResponse()

	def handleALMotionProxy_setWalkArmsEnabled(self, msg):
		try:
			self.motionProxy.setWalkArmsEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setWalkArmsEnabledResponse()

	def handleALMotionProxy_positionInterpolation(self, msg):
		try:
			self.motionProxy.positionInterpolation()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_positionInterpolationResponse()

	def handleALMotionProxy_positionInterpolations(self, msg):
		try:
			self.motionProxy.positionInterpolations()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_positionInterpolationsResponse()

	def handleALMotionProxy_setPosition(self, msg):
		try:
			self.motionProxy.setPosition()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setPositionResponse()

	def handleALMotionProxy_changePosition(self, msg):
		try:
			self.motionProxy.changePosition()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_changePositionResponse()

	def handleALMotionProxy_getPosition(self, msg):
		try:
			self.motionProxy.getPosition()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getPositionResponse()

	def handleALMotionProxy_transformInterpolation(self, msg):
		try:
			self.motionProxy.transformInterpolation()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_transformInterpolationResponse()

	def handleALMotionProxy_transformInterpolations(self, msg):
		try:
			self.motionProxy.transformInterpolations()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_transformInterpolationsResponse()

	def handleALMotionProxy_setTransform(self, msg):
		try:
			self.motionProxy.setTransform()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setTransformResponse()

	def handleALMotionProxy_changeTransform(self, msg):
		try:
			self.motionProxy.changeTransform()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_changeTransformResponse()

	def handleALMotionProxy_getTransform(self, msg):
		try:
			self.motionProxy.getTransform()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getTransformResponse()

	def handleALMotionProxy_wbEnable(self, msg):
		try:
			self.motionProxy.wbEnable()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbEnableResponse()

	def handleALMotionProxy_wbFootState(self, msg):
		try:
			self.motionProxy.wbFootState()
			who = msg._connection_header['callerid']
			rospy.loginfo("angleInterpolationWithSpeed from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbFootStateResponse()

	def handleALMotionProxy_wbEnableBalanceConstraint(self, msg):
		try:
			self.motionProxy.wbEnableBalanceConstraint()
			who = msg._connection_header['callerid']
			rospy.loginfo("wbEnableBalanceConstraint from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbEnableBalanceConstraintResponse()

	def handleALMotionProxy_wbGoToBalance(self, msg):
		try:
			self.motionProxy.wbGoToBalance()
			who = msg._connection_header['callerid']
			rospy.loginfo("wbGoToBalance from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbGoToBalanceResponse()

	def handleALMotionProxy_wbEnableEffectorControl(self, msg):
		try:
			self.motionProxy.wbEnableEffectorControl()
			who = msg._connection_header['callerid']
			rospy.loginfo("wbEnableEffectorControl from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbEnableEffectorControlResponse()

	def handleALMotionProxy_wbSetEffectorControl(self, msg):
		try:
			self.motionProxy.wbSetEffectorControl()
			who = msg._connection_header['callerid']
			rospy.loginfo("wbSetEffectorControl from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbSetEffectorControlResponse()

	def handleALMotionProxy_wbEnableEffectorOptimization(self, msg):
		try:
			self.motionProxy.wbEnableEffectorOptimization()
			who = msg._connection_header['callerid']
			rospy.loginfo("wbEnableEffectorOptimization from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_wbEnableEffectorOptimizationResponse()

	def handleALMotionProxy_setCollisionProtectionEnabled(self, msg):
		try:
			self.motionProxy.setCollisionProtectionEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("setCollisionProtectionEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setCollisionProtectionEnabledResponse()

	def handleALMotionProxy_getCollisionProtectionEnabled(self, msg):
		try:
			self.motionProxy.getCollisionProtectionEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("getCollisionProtectionEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getCollisionProtectionEnabledResponse()

	def handleALMotionProxy_isCollision(self, msg):
		try:
			self.motionProxy.isCollision()
			who = msg._connection_header['callerid']
			rospy.loginfo("isCollision from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_isCollisionResponse()

	def handleALMotionProxy_setFallManagerEnabled(self, msg):
		try:
			self.motionProxy.setFallManagerEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("setFallManagerEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setFallManagerEnabledResponse()

	def handleALMotionProxy_getFallManagerEnabled(self, msg):
		try:
			self.motionProxy.getFallManagerEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("getFallManagerEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getFallManagerEnabledResponse()

	def handleALMotionProxy_setSmartStiffnessEnabled(self, msg):
		try:
			self.motionProxy.setSmartStiffnessEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("setSmartStiffnessEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setSmartStiffnessEnabledResponse()

	def handleALMotionProxy_getSmartStiffnessEnabled(self, msg):
		try:
			self.motionProxy.getSmartStiffnessEnabled()
			who = msg._connection_header['callerid']
			rospy.loginfo("getSmartStiffnessEnabled from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getSmartStiffnessEnabledResponse()

	def handleALMotionProxy_getBodyNames(self, msg):
		try:
			self.motionProxy.getBodyNames()
			who = msg._connection_header['callerid']
			rospy.loginfo("getBodyNames from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getBodyNamesResponse()

	def handleALMotionProxy_getJointNames(self, msg):
		try:
			self.motionProxy.getJointNames()
			who = msg._connection_header['callerid']
			rospy.loginfo("getJointNames from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getJointNamesResponse()

	def handleALMotionProxy_getSensorNames(self, msg):
		try:
			self.motionProxy.getSensorNames()
			who = msg._connection_header['callerid']
			rospy.loginfo("getSensorNames from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getSensorNamesResponse()

	def handleALMotionProxy_getLimits(self, msg):
		try:
			self.motionProxy.getLimits()
			who = msg._connection_header['callerid']
			rospy.loginfo("getLimits from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getLimitsResponse()

	def handleALMotionProxy_getMotionCycleTime(self, msg):
		try:
			self.motionProxy.getMotionCycleTime()
			who = msg._connection_header['callerid']
			rospy.loginfo("getMotionCycleTime from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getMotionCycleTimeResponse()

	def handleALMotionProxy_getRobotConfig(self, msg):
		try:
			self.motionProxy.getRobotConfig()
			who = msg._connection_header['callerid']
			rospy.loginfo("getRobotConfig from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getRobotConfigResponse()

	def handleALMotionProxy_getSummary(self, msg):
		try:
			self.motionProxy.getSummary()
			who = msg._connection_header['callerid']
			rospy.loginfo("getSummary from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getSummaryResponse()

	def handleALMotionProxy_getMass(self, msg):
		try:
			self.motionProxy.getMass()
			who = msg._connection_header['callerid']
			rospy.loginfo("getMass from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getMassResponse()

	def handleALMotionProxy_getCOM(self, msg):
		try:
			self.motionProxy.getCOM()
			who = msg._connection_header['callerid']
			rospy.loginfo("getCOM from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getCOMResponse()

	def handleALMotionProxy_setMotionConfig(self, msg):
		try:
			self.motionProxy.setMotionConfig()
			who = msg._connection_header['callerid']
			rospy.loginfo("setMotionConfig from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_setMotionConfigResponse()

	def handleALMotionProxy_updateTrackerTarget(self, msg):
		try:
			self.motionProxy.updateTrackerTarget()
			who = msg._connection_header['callerid']
			rospy.loginfo("updateTrackerTarget from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_updateTrackerTargetResponse()

	def handleALMotionProxy_getTaskList(self, msg):
		try:
			self.motionProxy.getTaskList()
			who = msg._connection_header['callerid']
			rospy.loginfo("getTaskList from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_getTaskListResponse()

	def handleALMotionProxy_areResourcesAvailable(self, msg):
		try:
			self.motionProxy.areResourcesAvailable()
			who = msg._connection_header['callerid']
			rospy.loginfo("areResourcesAvailable from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_areResourcesAvailableResponse()

	def handleALMotionProxy_killTask(self, msg):
		try:
			self.motionProxy.killTask()
			who = msg._connection_header['callerid']
			rospy.loginfo("killTask from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_killTaskResponse()

	def handleALMotionProxy_killTasksUsingResources(self, msg):
		resourceNames = make_string_array(msg.resourceNames)
		try:
			self.motionProxy.post.killTasksUsingResources(resourceNames)
			who = msg._connection_header['callerid']
			rospy.loginfo("killTasksUsingResources(%s) from: "+who, str(msg.resourceNames))
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_killTasksUsingResourcesResponse()

	def handleALMotionProxy_killWalk(self, msg):
		try:
			self.motionProxy.killWalk()
			who = msg._connection_header['callerid']
			rospy.loginfo("killWalk from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_killWalkResponse()

	def handleALMotionProxy_killMove(self, msg):
		try:
			self.motionProxy.post.killMove()
			who = msg._connection_header['callerid']
			rospy.loginfo("killMove() from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_killMoveResponse()

	def handleALMotionProxy_killAll(self, msg):
		try:
			self.motionProxy.killAll()
			who = msg._connection_header['callerid']
			rospy.loginfo("killAll from: "+who)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotionProxy_killAllResponse()


def make_float_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[')
	if len(message) > 1: #nested
		message = message[1].split(']]')
		message = message[0].split('], [')
		for line in message:
			line = line.split(', ')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(float(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(float(line[0]))
		

	else:
		message = message[0].split('[')
		if len(message) > 1: #array
			message = message[1].split(']')
			message = message[0].split(', ')
			for val in message:
				ret_msg.append(float(val))
		else:
			ret_msg = (float(message[0]))
	return ret_msg

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1: #nested
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
		

	else:
		message = message[0].split('[\'')
		if len(message) > 1: #array
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg

################

if __name__ == '__main__':
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	print name
	node = Nao(name)
	rospy.loginfo("%s running...", name)
	rospy.spin()
	rospy.loginfo("%s stopped.", name)
	exit(0)
