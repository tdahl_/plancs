#!/usr/bin/env python

import rospy
import motion, time, math
#from naoqi import ALProxy
from std_msgs.msg import Bool
from std_srvs.srv import Empty

from plancs import PALProxy
from plancs.srv import *

if __name__ == "__main__":
	rospy.init_node('right_kick')
	motionProxy = PALProxy("ALMotion")
	postureProxy = PALProxy("ALRobotPosture")

	motionProxy.setStiffnesses("Body", 1.0)
	
	postureProxy.goToPosture("StandInit", 0.5)
	
	# Activate Whole Body Balancer
	isEnabled  = True
	motionProxy.wbEnable(isEnabled)
	
	# Legs are constrained fixed
	stateName  = "Fixed"
	supportLeg = "Legs"
	motionProxy.wbFootState(stateName, supportLeg)
	
	# Constraint Balance Motion
	isEnable   = True
	supportLeg = "Legs"
	motionProxy.wbEnableBalanceConstraint(isEnable, supportLeg)
	
	# Com go to LLeg
	supportLeg = "LLeg"
	duration   = 0.7					# (DY edit. Original 2.0)
	motionProxy.wbGoToBalance(supportLeg, duration)
	
	# RLeg is free
	stateName  = "Free"
	supportLeg = "RLeg"
	motionProxy.wbFootState(stateName, supportLeg)
	
	# LLeg is optimized
	effectorName = "RLeg"
	axisMask	 = 63
	space = motion.SPACE_NAO

	# Motion of the RLeg
	dx	  = 0.05				 # translation axis X (meters) 	(DY edit. Original: 0.05)
	dz	  = 0.03				  # translation axis Z (meters) 	(DY edit. Original: 0.05)
	dwy	 = 5.0*math.pi/180.0	# rotation axis Y (radian)		(DY edit. Original: 5.0*math.pi/180.0)
	times   = [0.2, 0.4, 0.9]
	isAbsolute = False		

	targetList = [
	[-dx, 0.0, dz, 0.0, +dwy, 0.0],
	[+dx, 0.0, dz, 0.0, 0.0, 0.0],
	[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]
	
	motionProxy.positionInterpolation(effectorName, space, targetList, axisMask, times, isAbsolute)


	# Example showing how to Enable Effector Control as an Optimization
	isActive	 = False
	motionProxy.wbEnableEffectorOptimization(effectorName, isActive)
	time.sleep(0.2)			# (DY edit. Original: 1.0)

	# Deactivate Head tracking
	isEnabled	= False
	motionProxy.wbEnable(isEnabled)
	
	# send robot to Pose Init
	postureProxy.goToPosture("StandInit", 0.5)
