#!/usr/bin/env python

from plancs.srv import *
from std_msgs.msg import String, Empty
import plancs
import rospy

def handle_add_two_ints(req):
	print "Returning [%s + %s = %s]"%(req.a, req.b, (req.a + req.b))
	return AddTwoIntsResponse(req.a + req.b)

def test_callback(data):
	print data.data	

def add_two_ints_server():
	mynode = plancs.Plancs_Node('plancs_add_two_ints_server')
	srv = mynode.create_service('add_two_ints', AddTwoInts, handle_add_two_ints)
	sub = mynode.add_subscriber('test', String, test_callback)
	pub = rospy.Publisher('test', String)
	inhib_pub = rospy.Publisher(mynode.plancs_subscriber_name, Empty)
	print "Ready to add two ints."
	#while not rospy.is_shutdown():
	#	pub.publish("subscriber test")
	#	print "sleep test"
	#	mynode.sleep(1)
	for x in range(0, 4):		
		inhib_pub.publish()
		rospy.sleep(1)
	rospy.spin()

if __name__ == "__main__":
	add_two_ints_server()
