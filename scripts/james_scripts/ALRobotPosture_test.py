#!/usr/bin/env python

import sys
import time
import rospy
from plancs import *
from plancs.srv import *


if __name__ == "__main__":
	rospy.init_node('naoqi_wrapper_test')
	s = rospy.ServiceProxy("nao_ALRobotPosture_goToPosture", plancs.srv.SALRobotPosture_goToPosture)
	print s('Stand', 1.0, True)
