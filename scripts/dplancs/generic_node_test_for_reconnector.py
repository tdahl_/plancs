#!/usr/bin/env python
import rospy, plancs
from std_msgs.msg import String

mynode = plancs.Plancs_Node('connectorTest')

def get_data():
    sub1 = mynode.add_subscriber("dplancs_data", String, firstcallback) 
    #sub2 = mynode.add_subscriber("dplancs_data2", String, secondcallback)
    #sub3 = mynode.add_subscriber("dplancs_data3", String, thirdcallback) 
    #sub4 = mynode.add_subscriber("dplancs_data4", String, fourthcallback)

    rospy.spin()

def firstcallback(data):
    rospy.loginfo(data.data)

def secondcallback(data):
    rospy.loginfo(data.data)

def thirdcallback(data):
    rospy.loginfo(data.data)

def fourthcallback(data):
    rospy.loginfo(data.data)

if __name__ == '__main__':
    get_data()
