#!/usr/bin/env python

import rospy
from std_msgs.msg import *
from subprocess import *
import os, sys

def createRequest():
    global kill_node
    rospy.init_node('dplancs_server')
    rospy.Subscriber("/dplancs_node_kill", String, killNode)
    rospy.Subscriber("/dplancs_node_creator", String, createNode)
    rospy.Subscriber("/dplancs_run_cmd", String, send_list)
    rospy.Subscriber("/dplancs_node_start", String, runFile)
    rospy.Subscriber("/dplancs_get_files", String, displayFiles)
    rospy.spin()

def killNode(node):
    global kill_node
    call(["rosnode", "kill", node.data])

def createNode(data):
    #lis = []
    #lis = data.data#.split(' ')
    #name = lis.pop(0)
    #new_data = ' '.join(lis)
    #-------Following lines are used to save the code into a file--------------
    #-------The dplancs_ui.html will need to be edited to accept a filename.---
    #--------------------------------------------------------------------------
    #path = '/home/brian/plancs/src/plancs_tutorials/scripts/'
    #fileName = name
    #rospy.loginfo("Creating a new file called " + fileName)
    #with open(os.path.join(path, fileName), "wb") as temp_file:
	#temp_file.write(new_data)
    #rospy.loginfo("File created. Now running...")
    #call(["python", new_data])
    #--------------------------------------------------------------------
    #exec new_data   #---If the above call method is used then this line should not be.

    proc = Popen(["python"], stdin=PIPE)
    proc.communicate(input=data.data) 

def send_list(data):
    pub = rospy.Publisher('dplancs_lists', String)
    while not rospy.is_shutdown():
	lis = []
	lis = data.data.split(' ')
	if len(lis) == 3:
	    str = check_output([lis[1].strip(), lis[0].strip(), lis[2]])
	    rospy.loginfo("Sending output from '" + lis[1] +" list -v'")
        else:
            str = check_output([lis[1].strip(), lis[0].strip()])
	    rospy.loginfo("Sending output from '" + lis[1] +" list'")
        pub.publish(String(str))
        rospy.sleep(1.0)
	rospy.loginfo("Output sent")
	sys.exit(0)

def runFile(filename):
    rospy.loginfo("Running file " + filename.data)
    call(["python", filename.data])

def displayFiles(message):
    pub = rospy.Publisher('dplancs_send_files', String)
    path = message.data
    str = check_output(["ls", path])
    rospy.loginfo("Sending output from 'ls " + path + "'")
    pub.publish(String(str))
    rospy.sleep(1.0)
    rospy.loginfo("Output sent")
    sys.exit(0)
	
if __name__ == '__main__':
    createRequest()
