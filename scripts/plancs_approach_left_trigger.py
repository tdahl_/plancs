#!/usr/bin/env python

import rospy, plancs, sys, os
from plancs.msg import ObjectDimensions
from std_msgs.msg import Empty

def get_values():
    global ball
    ball = None

    name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    topic_names = ["top_vision_ball_dimensions", "bottom_vision_ball_dimensions", "plancs_approach_left_trigger"]
    inhibit_names = ['plancs_search_trigger']
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) >= 2:
        name = name + "_" + argv[1]
	for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "_" + argv[1] + "/plancs_inhibit"
	for x in range(len(topic_names)):
	    topic_names[x] = topic_names[x] + "_" + argv[1]
    else:
        for x in range(len(inhibit_names)):
            inhibit_names[x] = inhibit_names[x] + "/plancs_inhibit"

    mynode = plancs.Plancs_Node(name)
    mynode.add_subscriber(topic_names[0], ObjectDimensions, ballData)
    mynode.add_subscriber(topic_names[1], ObjectDimensions, ballData)
    mynode.set_decay_period(2.0)
    ballPub = rospy.Publisher(topic_names[2], ObjectDimensions)
    searchInhibit = rospy.Publisher(inhibit_names[0], Empty)
    imageWidth = 320
    while not rospy.is_shutdown():
        if ball != None:
            if ball.x <= imageWidth / 3 and ball.x != 0.0:
                #print "Ball is in the left third of the camera."
                ballPub.publish(ball)
                searchInhibit.publish()
        rospy.sleep(0.1)

def ballData(bl):
    global ball
    ball = bl

if __name__ == '__main__':
    get_values()
