#!/usr/bin/env python

import os
import sys

import roslib
roslib.load_manifest('plancs')
import rospy

from naoqi import ALProxy

from std_srvs.srv import Empty, EmptyResponse

from plancs.srv import *

class Nao():
	def __init__(self, name):
		# ROS initialization:
		rospy.init_node(name)
		rospy.loginfo("%s starting...", name)
		argv = []
		argv = rospy.myargv(argv=sys.argv)
		if len(argv)>=2:
			nao_ip=argv[1].split('--pip=')[1]
			nao_port=int(argv[2].split('--pport=')[1])
		else:
			nao_ip="127.0.0.1"
			nao_port=9559
		rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
		try:
			self.ALAudioPlayer = ALProxy("ALAudioPlayer", nao_ip, nao_port)
		except RuntimeError, e:
			rospy.logerr("Could not create Proxy to ALAudioPlayer, exiting. \nException message:\n%s", e)
			exit(1)
		# start services:
		rospy.loginfo("ALAudio services going up...")

		rospy.Service("nao_ALAudioPlayer_getLoadedFilesNames", SALAudioPlayer_getLoadedFilesNames, self.handle_getLoadedFilesNames)
		rospy.Service("nao_ALAudioPlayer_playFile", SALAudioPlayer_playFile, self.handle_playFile)
		rospy.Service("nao_ALAudioPlayer_play", SALAudioPlayer_play, self.handle_play)
		rospy.Service("nao_ALAudioPlayer_loadFile", SALAudioPlayer_loadFile, self.handle_loadFile)

		rospy.loginfo("Initiated.")

		rospy.loginfo("%s initialized", name)


	def handle_getLoadedFilesNames(self, msg):
		try:
			self.ALAudioPlayer.getLoadedFilesNames()
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALAudioPlayer_getLoadedFilesNamesResponse()

	def handle_playFile(self, msg):
		try:
			self.ALAudioPlayer.playFile(msg.filename)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)

	def handle_play(self, msg):
		try:
			self.ALAudioPlayer.play(msg.fileID)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		#return SALAudioPlayer_play()

	def handle_loadFile(self, msg):
		try:
			taskID = self.ALAudioPlayer.loadFile(msg.fileName)
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALAudioPlayer_loadFileResponse(taskID)

if __name__ == '__main__':
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	print name
	node = Nao(name)
	rospy.loginfo("%s running...", name)
	rospy.spin()
	rospy.loginfo("%s stopped.", name)
	exit(0)
