#! /usr/bin/env python

import rospy
import os
import sys
import motion
import vision_definitions
from naoqi import ALProxy
from sensor_msgs.msg import Image


def run_image():
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

	argv = []
	argv = rospy.myargv(argv=sys.argv)
	if len(argv)>=4:
		nao_ip=argv[1].split('--pip=')[1]
		nao_port=int(argv[2].split('--pport=')[1])
		robot=argv[3].split('--rrobot=')[1]
		name = name + "_" + robot
	else:
		nao_ip="127.0.0.1"
		nao_port=9559
	#print name
	#rospy.loginfo("%s starting...", name)
	rospy.init_node(name)
	rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
	camProxy = ALProxy("ALVideoDevice", nao_ip, nao_port)
	if camProxy is None:
		exit(1)
	image_top_pub = rospy.Publisher(name, Image, queue_size=10)
	index = 0		# top camera, see:	http://www.aldebaran-robotics.com/documentation/naoqi/vision/alvideodevice-api.html#internal-types
	#resolution = 1		# kVGA, see:		http://www.aldebaran-robotics.com/documentation/naoqi/vision/alvideodevice-api.html#resolution
	resolution = vision_definitions.kQVGA
	colorSpace = 13		# kBGRColorSpace see:	http://www.aldebaran-robotics.com/documentation/naoqi/vision/alvideodevice-api.html#colorspace
	#colorSpace = vision_definitions.kYUVColorSpace
	fps = 10			# DY Edit from 30. We think this will save processing - can the robot really respond to 30 images per second?
	whiteBalance = 0
	autoExposure = 0
	nameId = ''
	nameId = camProxy.subscribeCamera(name, index, resolution, colorSpace, fps)
	#nameId = camProxy.subscribe("_client", resolution, colorSpace, 5)
	camProxy.setParam(vision_definitions.kCameraSelectID, 0)
	#camProxy.setParam(vision_definitions.kCameraAutoWhiteBalanceID, whiteBalance)
	#camProxy.setParam(vision_definitions.kCameraAutoExpositionID, autoExposure)
	

	rospy.sleep(1)
	print "Ready..."
	img = Image()
	if nameId:
		while not rospy.is_shutdown():
			image = camProxy.getImageRemote(nameId)
			img.header.stamp = rospy.Time.now()
			img.header.frame_id = "/CameraTop_frame"
			img.height = image[1]
			img.width = image[0]
			nbLayers = image[2]
			img.encoding = "bgr8"
			img.step = img.width * nbLayers
			img.data = image[6]
			image_top_pub.publish(img)
			#camProxy.releaseImage(nameId)
			#rospy.sleep(0.2)
	camProxy.unsubscribe(nameId)

if __name__ == "__main__":
	run_image()
