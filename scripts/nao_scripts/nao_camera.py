#! /usr/bin/env python

import rospy
import os
import sys
import motion
import vision_definitions
from naoqi import ALProxy
from sensor_msgs.msg import Image
from std_msgs.msg import *
from std_srvs.srv import *
from plancs.srv import *

def notify_camera_id(req):
	global pub
	global index
	id = int(req.id)
	if(id == 1 or id == 0):
		index = id
		pub.publish(index)
	return index

def notification_asked(req):
	pub.publish(index)
	return TriggerResponse(True, '')

def run_image():
	## Get the name of the node/program and store it in argv
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	argv = []
	argv = rospy.myargv(argv = sys.argv)
	
	## If the robot specifications are set the variables to default
	if len(argv)>=4:
		nao_ip=argv[1].split('--pip=')[1]
		nao_port=int(argv[2].split('--pport=')[1])
		robot=argv[3].split('--rrobot=')[1]
		name = name + "_" + robot
	else:
		nao_ip="127.0.0.1"
		nao_port=9559
		
	## Init the ros node
	rospy.init_node(name)
	rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
	
	## Init the camera proxy
	camProxy = ALProxy("ALVideoDevice", nao_ip, nao_port)
	if camProxy is None:
		exit(1)

	## Advertise image publisher
	image_pub = rospy.Publisher(name, Image, queue_size=10)
	
	## Set camera parameters
	global index
	index = 0
	resolution = vision_definitions.kQVGA
	colorSpace = vision_definitions.kBGRColorSpace#12
	fps = 10			# DY Edit from 30. We think this will save processing - can the robot really respond to 30 images per second?
	whiteBalance = 0	# deactivate white balance
	autoExposure = 0	# and auto exposure
	autoGain = 0		# Auto gain
	aecAlgo = 0
	greenGain = 63
	
	## Subscribe to camera stream
	nameId = ''
	nameId = camProxy.subscribeCamera(name, index, resolution, colorSpace, fps)
	
	
	
	## Set parameters and check if it is the real robot or the simulator
	# (simulator throws a runtime exception at the second seParam)
	isSimulator = False
	try:
		camProxy.setParam(vision_definitions.kCameraSelectID, index)
		camProxy.setParam(vision_definitions.kCameraAutoWhiteBalanceID, whiteBalance)
		camProxy.setParam(vision_definitions.kCameraAutoExpositionID, autoExposure)
		camProxy.setParam(vision_definitions.kCameraAutoGainID, autoGain)
		camProxy.setParam(vision_definitions.kCameraAecAlgorithmID, aecAlgo)
		camProxy.setParam(vision_definitions.kCameraAwbGreenGainID, greenGain)
	except:
		isSimulator = True
		
	previous_cam = index
	
	
	## Open the message topic to send the camera number to all subscribers
	global pub
	pub = rospy.Publisher(name+'/camera_id', Int32, queue_size = 1)
	## send it
	pub.publish(index)
	
	## Open the service to change the camera id
	srv = rospy.Service(name+'/camera_id', NaoCameraID, notify_camera_id)
	srv = rospy.Service(name+'/request_camera_id', Trigger, notification_asked)
	
	## Wait a moment to let the system operate changes (1 sec is more than sufficient)
	rospy.sleep(1)
	rospy.loginfo("nao_camera is ready...")
	
	## Create the image and loop
	img = Image()
	if nameId:
		while not rospy.is_shutdown():
			cam_id = index
			if cam_id != previous_cam:
				try:
					camProxy.setParam(vision_definitions.kCameraSelectID, cam_id)
					camProxy.setParam(vision_definitions.kCameraAutoWhiteBalanceID, whiteBalance)
					camProxy.setParam(vision_definitions.kCameraAutoExpositionID, autoExposure)
				except:
					pass
			previous_cam = cam_id
			
			## Get the image from the robot
			image = camProxy.getImageRemote(nameId)
			img.header.stamp = rospy.Time.now()
			img.header.frame_id = "/Camera_frame"
			img.height = image[1]
			img.width = image[0]
			nbLayers = image[2]
			img.encoding = "bgr8"
			img.step = img.width * nbLayers
			img.data = image[6]
			image_pub.publish(img)
			camProxy.releaseImage(nameId)
			## rospy.sleep(0.2)
	## Unsubscribe camera when finished
	camProxy.unsubscribe(nameId)

if __name__ == "__main__":
	run_image()
