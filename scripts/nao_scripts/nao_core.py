#!/usr/bin/env python

import os
import sys

import roslib
roslib.load_manifest('plancs')
import rospy

from naoqi import ALProxy

from std_srvs.srv import Empty, EmptyResponse

from plancs.srv import SALConnectionManagerProxyState, SALConnectionManagerProxyStateResponse, \
												SALConnectionManagerProxyScan, SALConnectionManagerProxyScanResponse, \
												SALMemoryProxy_declareEvent, SALMemoryProxy_declareEventResponse, \
												SALMemoryProxy_getData, SALMemoryProxy_getDataResponse, \
												SALMemoryProxy_getDataList, SALMemoryProxy_getDataListResponse, \
												SALMemoryProxy_getDataListName, SALMemoryProxy_getDataListNameResponse
 
from plancs import *

class Nao():
	def __init__(self, name):
	
		# ROS initialization:
		rospy.init_node(name)
		rospy.loginfo("%s starting...", name)
		# do some stuff about gettin ip and port for param server
		rospy.sleep(1)
		
		argv = []
		argv = rospy.myargv(argv=sys.argv)
		if len(argv)>=4:
			nao_ip=argv[1].split('--pip=')[1]
			nao_port=int(argv[2].split('--pport=')[1])
			robot=argv[3].split('--rrobot=')[1]
		else:
			nao_ip="127.0.0.1"
			nao_port=9559

		rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
		try:
			self.ConManProxy = ALProxy("ALConnectionManager", nao_ip, nao_port)
		except:
			self.ConManProxy = None
		try:
			self.MemManProxy = ALProxy("ALMemory", nao_ip, nao_port)
		except:
			self.MemManProxy = None
		try:
			self.ModuleProxy = ALProxy("ALModule", nao_ip, nao_port)
		except:
			self.ModuleProxy = None
		
		# start services:
		if self.ConManProxy:
			rospy.loginfo("ALConnectionManger services goin up...")
			rospy.Service("nao_ALConnectionManagerProxyState", SALConnectionManagerProxyState, self.handleALConnectionManagerProxyState)
			rospy.Service("nao_ALConnectionManagerProxyScan", SALConnectionManagerProxyScan, self.handleALConnectionManagerProxyScan)
			rospy.loginfo("Initiated.")
		else:
			rospy.loginfo("No ALConnectionManger proxy.")


		if self.MemManProxy:
			rospy.loginfo("ALMemory services goin up...")
			rospy.Service("nao_ALMemoryProxy_declareEvent", SALMemoryProxy_declareEvent, self.handleALMemoryProxy_declareEvent)
			rospy.Service("nao_ALMemoryProxy_getData", SALMemoryProxy_getData, self.handleALMemoryProxy_getData)
			rospy.Service("nao_ALMemoryProxy_getDataList", SALMemoryProxy_getDataList, self.handleALMemoryProxy_getDataList)
			rospy.Service("nao_ALMemoryProxy_getDataListName", SALMemoryProxy_getDataListName, self.handleALMemoryProxy_getDataListName)
			rospy.loginfo("Initiated.")
		else:
			rospy.loginfo("No ALMemory proxy.")

		if self.ModuleProxy:
			rospy.loginfo("ALModule services goin up...")

			rospy.loginfo("Initiated.")
		else:
			rospy.loginfo("No ALModule proxy.")


		rospy.loginfo("%s initialized", name)

#######################

	def handleALConnectionManagerProxyState(self, msg):
		try:
			msg = self.ConManProxy.state()
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
		return SALConnectionManagerProxyStateResponse(msg.respone)

	def handleALConnectionManagerProxyScan(self, msg):
		try:
			self.ConManProxy.scan()
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
		return SALConnectionManagerProxyScanResponse()

#######################

	def handleALMemoryProxy_declareEvent(self, msg):
		try:
			if msg.extractorName:
				self.MemManProxy.declareEvent(msg.eventName, msg.extractorName)
			else:
				self.MemManProxy.declareEvent(msg.eventName)
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
		return SALMemoryProxy_declareEventResponse()


	def handleALMemoryProxy_getData(self, msg):
		newmsg = None
		try:
				replymsg = self.MemManProxy.getData(msg.key)
				newmsg = str(replymsg)
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
				newmsg = str(e)
		return SALMemoryProxy_getDataResponse(newmsg)


	def handleALMemoryProxy_getDataList(self, msg):
		newmsg = []
		try:
				newmsg = self.MemManProxy.getDataList(msg.filter)
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
				newmsg = str(e)
		return SALMemoryProxy_getDataListResponse(newmsg)


	def handleALMemoryProxy_getDataListName(self, msg):
		newmsg = []
		try:
				newmsg = self.MemManProxy.getDataListName()
		except RuntimeError,e:
				rospy.logerr("Exception caught:\n%s", e)
				newmsg = str(e)
		return SALMemoryProxy_getDataListNameResponse(newmsg)

######################

if __name__ == '__main__':
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	node = Nao(name)
	rospy.loginfo("%s running...", name)
	rospy.spin()
	rospy.loginfo("%s stopped.", name)
	exit(0)
