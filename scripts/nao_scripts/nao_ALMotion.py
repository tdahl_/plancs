#!/usr/bin/env python

import os
import sys

import roslib
roslib.load_manifest('plancs')
import rospy

from naoqi import ALProxy

from std_srvs.srv import Empty, EmptyResponse

from plancs.srv import *


class Nao():
	def __init__(self, name):
		# ROS initialization:
		rospy.init_node(name)
		#rospy.loginfo("%s starting...", name)
		argv = []
		argv = rospy.myargv(argv=sys.argv)
		topic_names = ["nao_ALMotion_setStiffnesses",
			"nao_ALMotion_wbEnable",
			"nao_ALMotion_wbFootState",
		 	"nao_ALMotion_wbEnableBalanceConstraint",
		 	"nao_ALMotion_wbGoToBalance",
		 	"nao_ALMotion_positionInterpolation", 
			"nao_ALMotion_wbEnableEffectorOptimization",
			"nao_ALMotion_wakeUp",
			"nao_ALMotion_rest", 
			"nao_ALMotion_angleInterpolation",
			"nao_ALMotion_angleInterpolationWithSpeed",
			"nao_ALMotion_setAngles", 
			"nao_ALMotion_changeAngles",
			"nao_ALMotion_getAngles",
			"nao_ALMotion_getPosition",
			"nao_ALMotion_getTaskStatusFromID",
			"nao_ALMotion_move",
			"nao_ALMotion_moveTo", 
		 	"nao_ALMotion_moveIsActive",
		 	"nao_ALMotion_killTasksUsingResources",
		 	"nao_ALMotion_killMove",
			"nao_ALMotion_setWalkArmsEnabled",
			"nao_ALMotion_setWalkTargetVelocity",
			"nao_ALMotion_setFallManagerEnabled",
			]
			
		if len(argv)>=4:
			nao_ip=argv[1].split('--pip=')[1]
			nao_port=int(argv[2].split('--pport=')[1])
			robot=argv[3].split('--rrobot=')[1]
			for x in range(len(topic_names)):
	    			topic_names[x] = topic_names[x] + "_" + robot
		else:
			nao_ip="127.0.0.1"
			nao_port=9559
		#rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
		try:
			self.ALMotion = ALProxy("ALMotion", nao_ip, nao_port)
		except RuntimeError, e:
			rospy.logerr("Could not create Proxy to ALMotion, exiting. \nException message:\n%s", e)
			exit(1)
		# start services:
		#rospy.loginfo("ALMotion services going up...")

		rospy.Service(topic_names[0], SALMotion_setStiffnesses, self.handle_setStiffnesses)
		rospy.Service(topic_names[1], SALMotion_wbEnable, self.handle_wbEnable)
		rospy.Service(topic_names[2], SALMotion_wbFootState, self.handle_wbFootState)
		rospy.Service(topic_names[3], SALMotion_wbEnableBalanceConstraint, self.handle_wbEnableBalanceConstraint)
		rospy.Service(topic_names[4], SALMotion_wbGoToBalance, self.handle_wbGoToBalance)
		rospy.Service(topic_names[5], SALMotion_positionInterpolation, self.handle_positionInterpolation)
		rospy.Service(topic_names[6], SALMotion_wbEnableEffectorOptimization, self.handle_wbEnableEffectorOptimization)
		rospy.Service(topic_names[7], SALMotion_wakeUp, self.handle_wakeUp)
		rospy.Service(topic_names[8], SALMotion_rest, self.handle_rest)
		rospy.Service(topic_names[9], SALMotion_angleInterpolation, self.handle_angleInterpolation)
		rospy.Service(topic_names[10], SALMotion_angleInterpolationWithSpeed, self.handle_angleInterpolationWithSpeed)
		rospy.Service(topic_names[11], SALMotion_setAngles, self.handle_setAngles)
		rospy.Service(topic_names[12], SALMotion_changeAngles, self.handle_changeAngles)
		rospy.Service(topic_names[13], SALMotion_getAngles, self.handle_getAngles)
		rospy.Service(topic_names[14], SALMotion_getPosition, self.handle_getPosition)
		rospy.Service(topic_names[15], SALMotion_getTaskStatusFromID, self.handle_getTaskStatusFromID)
		rospy.Service(topic_names[16], SALMotion_move, self.handle_move)
		rospy.Service(topic_names[17], SALMotion_moveTo, self.handle_moveTo)
		rospy.Service(topic_names[18], SALMotion_moveIsActive, self.handle_moveIsActive)
		rospy.Service(topic_names[19], SALMotion_killTasksUsingResources, self.handle_killTasksUsingResources)
		rospy.Service(topic_names[20], SALMotion_killMove, self.handle_killMove)
		rospy.Service(topic_names[21], SALMotion_setWalkArmsEnabled, self.handle_setWalkArmsEnabled)
		rospy.Service(topic_names[22], SALMotion_setWalkTargetVelocity, self.handle_setWalkTargetVelocity)
		rospy.Service(topic_names[23], SALMotion_setFallManagerEnabled, self.handle_setFallManagerEnabled)

		#rospy.loginfo("Initiated.")

		#rospy.loginfo("%s initialized", name)

	def handle_setFallManagerEnabled(self, msg):
		isEnabled = msg.isTrue
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbEnable(isTrue)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnable("+str(isTrue)+") non-blocking")
			else:
				self.ALMotion.wbEnable(isTrue)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnable("+str(isTrue)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_setFallManagerEnabledResponse(proxyrep, int(proxyid))

	def handle_setWalkTargetVelocity(self, msg):
		x = msg.x
		y = msg.y
		theta = msg.theta
		frequency = msg.frequency
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.setWalkTargetVelocity(x, y, theta, frequency)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setWalkTargetVelocity("+str(x)+", "+str(y)+", "+str(theta)+", "+str(frequency)+") non-blocking")
			else:
				self.ALMotion.setWalkTargetVelocity(x, y, theta, frequency)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setWalkTargetVelocity("+str(x)+", "+str(y)+", "+str(theta)+", "+str(frequency)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_setWalkTargetVelocityResponse(proxyrep, int(proxyid))

	def handle_setWalkArmsEnabled(self, msg):
		leftArmEnable = msg.leftArmEnable
		rightArmEnable = msg.rightArmEnable
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.setWalkArmsEnabled(leftArmEnable, rightArmEnable)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setWalkArmsEnabled("+str(leftArmEnable)+", "+str(rightArmEnable)+") non-blocking")
			else:
				self.ALMotion.setWalkArmsEnabled(leftArmEnable, rightArmEnable)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setWalkArmsEnabled("+str(leftArmEnable)+", "+str(rightArmEnable)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_setWalkArmsEnabledResponse(proxyrep, int(proxyid))


	def handle_setStiffnesses(self, msg):
		names = make_string_array(msg.names)
		stiffnesses = make_float_array(msg.stiffnesses)
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.setStiffnesses(names, stiffnesses)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setStiffnesses("+str(names)+", "+str(stiffnesses)+") non-blocking")
			else:
				self.ALMotion.setStiffnesses(names, stiffnesses)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setStiffnesses("+str(names)+", "+str(stiffnesses)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_setStiffnessesResponse(proxyrep, int(proxyid))


	def handle_wbEnable(self, msg):
		isEnabled = msg.isEnabled
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbEnable(isEnabled)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnable("+str(isEnabled)+") non-blocking")
			else:
				self.ALMotion.wbEnable(isEnabled)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnable("+str(isEnabled)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wbEnableResponse(proxyrep, int(proxyid))


	def handle_wbFootState(self, msg):
		stateName = msg.stateName
		supportLeg = msg.supportLeg
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbFootState(stateName, supportLeg)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbFootState("+str(stateName)+", "+str(supportLeg)+") non-blocking")
			else:
				self.ALMotion.wbFootState(stateName, supportLeg)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbFootState("+str(stateName)+", "+str(supportLeg)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wbFootStateResponse(proxyrep, int(proxyid))


	def handle_wbEnableBalanceConstraint(self, msg):
		isEnable = msg.isEnable
		supportLeg = msg.supportLeg
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbEnableBalanceConstraint(isEnable, supportLeg)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnableBalanceConstraint("+str(isEnable)+", "+str(supportLeg)+") non-blocking")
			else:
				self.ALMotion.wbEnableBalanceConstraint(isEnable, supportLeg)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnableBalanceConstraint("+str(isEnable)+", "+str(supportLeg)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wbEnableBalanceConstraintResponse(proxyrep, int(proxyid))


	def handle_wbGoToBalance(self, msg):
		supportLeg = msg.supportLeg
		duration = msg.duration
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbGoToBalance(supportLeg, duration)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbGoToBalance("+str(supportLeg)+", "+str(duration)+") non-blocking")
			else:
				self.ALMotion.wbGoToBalance(supportLeg, duration)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbGoToBalance("+str(supportLeg)+", "+str(duration)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wbGoToBalanceResponse(proxyrep, int(proxyid))


	def handle_positionInterpolation(self, msg):
		chainName = msg.chainName
		space = msg.space
		path = make_float_array(msg.path)
		axisMask = msg.axisMask
		durations = make_float_array(msg.durations)
		isAbsolute = msg.isAbsolute
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.positionInterpolation(chainName, space, path, axisMask, durations, isAbsolute)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t positionInterpolation("+str(chainName)+", "+str(space)+", "+str(path)+", "+str(axisMask)+", "+str(durations)+", "+str(isAbsolute)+") non-blocking")
			else:
				self.ALMotion.positionInterpolation(chainName, space, path, axisMask, durations, isAbsolute)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t positionInterpolation("+str(chainName)+", "+str(space)+", "+str(path)+", "+str(axisMask)+", "+str(durations)+", "+str(isAbsolute)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_positionInterpolationResponse(proxyrep, int(proxyid))


	def handle_wbEnableEffectorOptimization(self, msg):
		effectorName = msg.effectorName
		isEnabled = msg.isEnabled
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wbEnableEffectorOptimization(effectorName, isEnabled)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnableEffectorOptimization("+str(effectorName)+", "+str(isEnabled)+") non-blocking")
			else:
				self.ALMotion.wbEnableEffectorOptimization(effectorName, isEnabled)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wbEnableEffectorOptimization("+str(effectorName)+", "+str(isEnabled)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wbEnableEffectorOptimizationResponse(proxyrep, int(proxyid))


	def handle_wakeUp(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.wakeUp()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wakeUp() non-blocking")
			else:
				self.ALMotion.wakeUp()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t wakeUp()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_wakeUpResponse(proxyrep, int(proxyid))


	def handle_rest(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.rest()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t rest() non-blocking")
			else:
				self.ALMotion.rest()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t rest()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_restResponse(proxyrep, int(proxyid))


	def handle_angleInterpolation(self, msg):
		names = make_string_array(msg.names)
		angleLists = make_float_array(msg.angleLists)
		timeLists = make_float_array(msg.timeLists)
		isAbsolute = msg.isAbsolute
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.angleInterpolation(names, angleLists, timeLists, isAbsolute)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t angleInterpolation("+str(names)+", "+str(angleLists)+", "+str(timeLists)+", "+str(isAbsolute)+") non-blocking")
			else:
				self.ALMotion.angleInterpolation(names, angleLists, timeLists, isAbsolute)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t angleInterpolation("+str(names)+", "+str(angleLists)+", "+str(timeLists)+", "+str(isAbsolute)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_angleInterpolationResponse(proxyrep, int(proxyid))


	def handle_angleInterpolationWithSpeed(self, msg):
		names = make_string_array(msg.names)
		targetAngles = make_float_array(msg.targetAngles)
		maxSpeedFraction = msg.maxSpeedFraction
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.angleInterpolationWithSpeed(names, targetAngles, maxSpeedFraction)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t angleInterpolationWithSpeed("+str(names)+", "+str(targetAngles)+", "+str(maxSpeedFraction)+") non-blocking")
			else:
				self.ALMotion.angleInterpolationWithSpeed(names, targetAngles, maxSpeedFraction)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t angleInterpolationWithSpeed("+str(names)+", "+str(targetAngles)+", "+str(maxSpeedFraction)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_angleInterpolationWithSpeedResponse(proxyrep, int(proxyid))


	def handle_setAngles(self, msg):
		names = make_string_array(msg.names)
		angles = make_float_array(msg.angles)
		fractionMaxSpeed = msg.fractionMaxSpeed
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.setAngles(names, angles, fractionMaxSpeed)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setAngles("+str(names)+", "+str(angles)+", "+str(fractionMaxSpeed)+") non-blocking")
			else:
				self.ALMotion.setAngles(names, angles, fractionMaxSpeed)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t setAngles("+str(names)+", "+str(angles)+", "+str(fractionMaxSpeed)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_setAnglesResponse(proxyrep, int(proxyid))


	def handle_changeAngles(self, msg):
		names = make_string_array(msg.names)
		changes = make_float_array(msg.changes)
		fractionMaxSpeed = msg.fractionMaxSpeed
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.changeAngles(names, changes, fractionMaxSpeed)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t changeAngles("+str(names)+", "+str(changes)+", "+str(fractionMaxSpeed)+") non-blocking")
			else:
				self.ALMotion.changeAngles(names, changes, fractionMaxSpeed)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t changeAngles("+str(names)+", "+str(changes)+", "+str(fractionMaxSpeed)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_changeAnglesResponse(proxyrep, int(proxyid))


	def handle_getAngles(self, msg):
		names = make_string_array(msg.names)
		useSensors = msg.useSensors
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.getAngles(names, useSensors)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t getAngles("+str(names)+", "+str(useSensors)+") non-blocking")
			else:
				proxyrep = self.ALMotion.getAngles(names, useSensors)
				who = msg._connection_header['callerid']
				##rospy.loginfo("from: "+who+"\t getAngles("+str(names)+", "+str(useSensors)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_getAnglesResponse(str(proxyrep), int(proxyid))
	
	
	def handle_getPosition(self, msg):
		name = msg.name
		space = msg.space		# FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2
		useSensors = msg.useSensors
		proxyrep = []
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.getPosition(name, space, useSensors)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t getAngles("+str(name)+", "+str(space)+", "+str(useSensors)+") non-blocking")
			else:
				proxyrep = self.ALMotion.getPosition(name, space, useSensors)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t getAngles("+str(name)+", "+str(space)+", "+str(useSensors)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_getPositionResponse(proxyrep, int(proxyid))
	
	def handle_getTaskStatusFromID(self, msg):
		id = msg.task_id
		status = self.ALMotion.isRunning(id)
		return status

	def handle_move(self, msg):
		x = msg.x
		y = msg.y
		theta = msg.theta
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.move(x, y, theta)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t move("+str(x)+", "+str(y)+", "+str(theta)+") non-blocking")
			else:
				self.ALMotion.move(x, y, theta)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t move("+str(x)+", "+str(y)+", "+str(theta)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_moveResponse(proxyrep, int(proxyid))


	def handle_moveTo(self, msg):
		x = msg.x
		y = msg.y
		theta = msg.theta
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.moveTo(x, y, theta)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t moveTo("+str(x)+", "+str(y)+", "+str(theta)+") non-blocking")
			else:
				self.ALMotion.moveTo(x, y, theta)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t moveTo("+str(x)+", "+str(y)+", "+str(theta)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_moveToResponse(proxyrep, int(proxyid))


	def handle_moveIsActive(self, msg):
		proxyrep = True
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.moveIsActive()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t moveIsActive() non-blocking")
			else:
				proxyrep = self.ALMotion.moveIsActive()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t moveIsActive()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_moveIsActiveResponse(proxyrep, int(proxyid))


	def handle_killTasksUsingResources(self, msg):
		resourceNames = msg.resourceNames
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.killTasksUsingResources(resourceNames)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t killTasksUsingResources("+str(resourceNames)+") non-blocking")
			else:
				self.ALMotion.killTasksUsingResources(resourceNames)
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t killTasksUsingResources("+str(resourceNames)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_killTasksUsingResourcesResponse(proxyrep, int(proxyid))


	def handle_killMove(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALMotion.post.killMove()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t killMove() non-blocking")
			else:
				self.ALMotion.killMove()
				who = msg._connection_header['callerid']
				#rospy.loginfo("from: "+who+"\t killMove()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALMotion_killMoveResponse(proxyrep, int(proxyid))


def make_float_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[')
	if len(message) > 1: #nested
		message = message[1].split(']]')
		message = message[0].split('], [')
		for line in message:
			line = line.split(', ')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(float(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(float(line[0]))
	else:
		message = message[0].split('[')
		if len(message) > 1: #array
			message = message[1].split(']')
			message = message[0].split(', ')
			for val in message:
				ret_msg.append(float(val))
		else:
			ret_msg = (float(message[0]))
	return ret_msg

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1:
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1:
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
	else:
		message = message[0].split('[\'')
		if len(message) > 1:
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg



if __name__ == '__main__':
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	print name
	node = Nao(name)
	#rospy.loginfo("%s running...", name)
	rospy.spin()
	#rospy.loginfo("%s stopped.", name)
	exit(0)
