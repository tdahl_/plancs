#!/usr/bin/env python

import os
import sys

import roslib
roslib.load_manifest('plancs')
import rospy

from naoqi import ALProxy

from std_srvs.srv import Empty, EmptyResponse

from plancs.srv import *


class Nao():
	def __init__(self, name):
		# ROS initialization:
		rospy.init_node(name)
		rospy.loginfo("%s starting...", name)
		argv = []
		argv = rospy.myargv(argv=sys.argv)
		if len(argv)>=2:
			nao_ip=argv[1].split('--pip=')[1]
			nao_port=int(argv[2].split('--pport=')[1])
		else:
			nao_ip="127.0.0.1"
			nao_port=9559
		rospy.loginfo('Connecting to: %s:%s', nao_ip, nao_port)
		try:
			self.ALRobotPosture = ALProxy("ALRobotPosture", nao_ip, nao_port)
		except RuntimeError, e:
			rospy.logerr("Could not create Proxy to ALRobotPosture, exiting. \nException message:\n%s", e)
			exit(1)
		# start services:
		rospy.loginfo("ALRobotPosture services going up...")

		rospy.Service("nao_ALRobotPosture_getPostureList", SALRobotPosture_getPostureList, self.handle_getPostureList)
		rospy.Service("nao_ALRobotPosture_goToPosture", SALRobotPosture_goToPosture, self.handle_goToPosture)
		rospy.Service("nao_ALRobotPosture_applyPosture", SALRobotPosture_applyPosture, self.handle_applyPosture)
		rospy.Service("nao_ALRobotPosture_stopMove", SALRobotPosture_stopMove, self.handle_stopMove)
		rospy.Service("nao_ALRobotPosture_getPostureFamily", SALRobotPosture_getPostureFamily, self.handle_getPostureFamily)
		rospy.Service("nao_ALRobotPosture_getPostureFamilyList", SALRobotPosture_getPostureFamilyList, self.handle_getPostureFamilyList)
		rospy.Service("nao_ALRobotPosture_setMaxTryNumber", SALRobotPosture_setMaxTryNumber, self.handle_setMaxTryNumber)

		rospy.loginfo("Initiated.")

		rospy.loginfo("%s initialized", name)




	def handle_getPostureList(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.getPostureList()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureList() non-blocking")
			else:
				proxyrep = self.ALRobotPosture.getPostureList()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureList()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_getPostureListResponse(str(proxyrep), int(proxyid))


	def handle_goToPosture(self, msg):
		postureName = msg.postureName
		speed = msg.speed
		proxyrep = True
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.goToPosture(postureName, speed)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t goToPosture("+str(postureName)+", "+str(speed)+") non-blocking")
			else:
				proxyrep = self.ALRobotPosture.goToPosture(postureName, speed)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t goToPosture("+str(postureName)+", "+str(speed)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_goToPostureResponse(proxyrep, int(proxyid))


	def handle_applyPosture(self, msg):
		postureName = msg.postureName
		speed = msg.speed
		proxyrep = True
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.applyPosture(postureName, speed)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t applyPosture("+str(postureName)+", "+str(speed)+") non-blocking")
			else:
				proxyrep = self.ALRobotPosture.applyPosture(postureName, speed)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t applyPosture("+str(postureName)+", "+str(speed)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_applyPostureResponse(proxyrep, int(proxyid))


	def handle_stopMove(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.stopMove()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t stopMove() non-blocking")
			else:
				self.ALRobotPosture.stopMove()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t stopMove()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_stopMoveResponse(proxyrep, int(proxyid))


	def handle_getPostureFamily(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.getPostureFamily()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureFamily() non-blocking")
			else:
				proxyrep = self.ALRobotPosture.getPostureFamily()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureFamily()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_getPostureFamilyResponse(proxyrep, int(proxyid))


	def handle_getPostureFamilyList(self, msg):
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.getPostureFamilyList()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureFamilyList() non-blocking")
			else:
				proxyrep = self.ALRobotPosture.getPostureFamilyList()
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t getPostureFamilyList()")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_getPostureFamilyListResponse(str(proxyrep), int(proxyid))


	def handle_setMaxTryNumber(self, msg):
		maxTryNumber = msg.maxTryNumber
		proxyrep = ''
		proxyid = 0
		try:
			if msg.post:
				proxyid = self.ALRobotPosture.post.setMaxTryNumber(maxTryNumber)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t setMaxTryNumber("+str(maxTryNumber)+") non-blocking")
			else:
				self.ALRobotPosture.setMaxTryNumber(maxTryNumber)
				who = msg._connection_header['callerid']
				rospy.loginfo("from: "+who+"\t setMaxTryNumber("+str(maxTryNumber)+")")
		except RuntimeError,e:
			rospy.logerr("Exception caught:\n%s", e)
		return SALRobotPosture_setMaxTryNumberResponse(proxyrep, int(proxyid))


def make_float_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[')
	if len(message) > 1: #nested
		message = message[1].split(']]')
		message = message[0].split('], [')
		for line in message:
			line = line.split(', ')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(float(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(float(line[0]))
	else:
		message = message[0].split('[')
		if len(message) > 1: #array
			message = message[1].split(']')
			message = message[0].split(', ')
			for val in message:
				ret_msg.append(float(val))
		else:
			ret_msg = (float(message[0]))
	return ret_msg

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1:
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1:
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
	else:
		message = message[0].split('[\'')
		if len(message) > 1:
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg



if __name__ == '__main__':
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
	print name
	node = Nao(name)
	rospy.loginfo("%s running...", name)
	rospy.spin()
	rospy.loginfo("%s stopped.", name)
	exit(0)