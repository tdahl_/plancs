## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

set_args = generate_distutils_setup(
    packages=['plancs'],
    package_dir={'': 'src'},
    requires=['std_msgs', 'rospy']
)

setup(**set_args)
