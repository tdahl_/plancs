#!/usr/bin/env python
import os, sys, stat, re

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1: #nested
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
		

	else:
		message = message[0].split('[\'')
		if len(message) > 1: #array
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg


if __name__ == '__main__':
	proxyName = str(raw_input('Proxy name: '))
	filename = 'nao_'+proxyName	
	if not os.path.isfile('./APISaveFiles/'+filename):
		commands = []
		while True:
			inpstr = str(raw_input('Command name [leave blank and return for end]: '))
			if not inpstr:
				break
			command = []
			command.append(inpstr)
			variables = []
			types = []
			while True:
				variable = str(raw_input('Variable name [leave blank and return for end]: '))
				if not variable:
					break
				vartype = str(raw_input('Variable type: '))
				variables.append(variable)
				types.append(vartype)
			if len(variables) == 0:
				variables.append('Request')
				types.append('std_msgs/Empty')
			inpstr = str(raw_input('Reply type [leave blank for none]: '))
			reply = []
			if not inpstr:
				reply.append('std_msgs/Empty')
			else:
				reply.append(inpstr)
			line = [command, variables, types, reply]
			commands.append(line)
		sf = open('./APISaveFiles/'+filename, 'w')
		for line in commands:
			sf.write(str(line)+'\n')
		sf.close()
		for line in commands:
			print str(line)+'\n'
	else:
		print "Read from file "+filename+"\n"
		readline = []
		commands = []
		with open('./APISaveFiles/'+filename) as sf:
			for line in sf:
				readline.append(make_string_array(str(line)))
		for line in readline:
			[command, variables, types, reply] = line
			new1 = []
			new2 = []
			if type(variables) is str:
				new1.append(str(variables))
				new2.append(str(types))
				line = [command, new1, new2, reply]
			else:
				line = [command, variables, types, reply]
			commands.append(line)
			print line
	print "Processing..."
	path = './APImadeFiles/'
	if not os.path.exists(path):
		os.makedirs(path)
	pf = open('./APImadeFiles/'+filename+'.py', 'w')
	pf.write('#!/usr/bin/env python\n\
\n\
import os\n\
import sys\n\
\n\
import roslib\n\
roslib.load_manifest(\'plancs\')\n\
import rospy\n\
\n\
from naoqi import ALProxy\n\
\n\
from std_srvs.srv import Empty, EmptyResponse\n\
\n\
from plancs.srv import *')
#	pf.write('\\\n')
#	for x in range(0, len(commands)-1):
#		pf.write('\t\t\tS'+proxyName+'_'+commands[x][0]+', S'+proxyName+'_'+commands[x][0]+'Response, \\\n')
#	pf.write('\t\t\tS'+proxyName+'_'+commands[len(commands)-1][0]+', S'+proxyName+'_'+commands[len(commands)-1][0]+'Response\n')
	pf.write('\n\
\n\
\n\
class Nao():\n\
	def __init__(self, name):\n\
		# ROS initialization:\n\
		rospy.init_node(name)\n\
		rospy.loginfo("%s starting...", name)\n\
		argv = []\n\
		argv = rospy.myargv(argv=sys.argv)\n\
		if len(argv)>=2:\n\
			nao_ip=argv[1].split(\'--pip=\')[1]\n\
			nao_port=int(argv[2].split(\'--pport=\')[1])\n\
		else:\n\
			nao_ip="127.0.0.1"\n\
			nao_port=9559\n\
		rospy.loginfo(\'Connecting to: %s:%s\', nao_ip, nao_port)\n\
		try:\n\
			self.'+proxyName+' = ALProxy("'+proxyName+'", nao_ip, nao_port)\n\
		except RuntimeError, e:\n\
			rospy.logerr("Could not create Proxy to '+proxyName+', exiting. \\nException message:\\n%s", e)\n\
			exit(1)\n\
		# start services:\n\
		rospy.loginfo("'+proxyName+' services going up...")\n\n')
	for line in commands:
		pf.write('\t\trospy.Service("nao_'+proxyName+'_'+line[0]+'", S'+proxyName+'_'+line[0]+', self.handle_'+line[0]+')\n')
	pf.write('\n\
		rospy.loginfo("Initiated.")\n\
\n\
		rospy.loginfo("%s initialized", name)\n\n')
	for line in commands:
		[command, variables, types, reply] = line
		pf.write('\n\n\n\
	def handle_'+command+'(self, msg):\n')
		for variable in variables:
			vartype = types.pop(0)
			if vartype == 'vector string':
				pf.write('\t\t'+variable+' = make_string_array(msg.'+variable+')\n')
			elif vartype == 'vector float':
				pf.write('\t\t'+variable+' = make_float_array(msg.'+variable+')\n')
			elif vartype == 'std_msgs/Empty':
				pass
			else:
				pf.write('\t\t'+variable+' = msg.'+variable+'\n')
			types.append(vartype)
		if reply == 'bool':
			pf.write('\t\tproxyrep = True\n')
		elif re.search(r'int', reply, re.M|re.I):
			pf.write('\t\tproxyrep = 0\n')
		else:
			pf.write('\t\tproxyrep = \'\'\n')
		pf.write('\t\tproxyid = 0\n')
		pf.write('\
		try:\n\
			if msg.post:\n\
				proxyid = self.'+proxyName+'.post.'+command+'(')
		for x in range(0, (len(variables)-1)):
			pf.write(variables[x]+', ')
		if types[len(types)-1] != "std_msgs/Empty":
			pf.write(variables[len(variables)-1])
		pf.write(')\n\
				who = msg._connection_header[\'callerid\']\n\
				rospy.loginfo("from: "+who+"\\t '+command+'(')
		for x in range(0, (len(variables)-1)):
			pf.write('"+str('+variables[x]+')+", ')
		if types[len(types)-1] != 'std_msgs/Empty':
			pf.write('"+str('+variables[len(variables)-1]+')+"')
		pf.write(') non-blocking")\n\
			else:\n\t\t\t\t')
		if reply != 'std_msgs/Empty':
			pf.write('proxyrep = ')
		pf.write('self.'+proxyName+'.'+command+'(')
		for x in range(0, (len(variables)-1)):
			pf.write(variables[x]+', ')
		if types[len(types)-1] != "std_msgs/Empty":
			pf.write(variables[len(variables)-1])
		pf.write(')\n\
				who = msg._connection_header[\'callerid\']\n\
				rospy.loginfo("from: "+who+"\\t '+command+'(')
		for x in range(0, (len(variables)-1)):
			pf.write('"+str('+variables[x]+')+", ')
		if types[len(types)-1] != 'std_msgs/Empty':
			pf.write('"+str('+variables[len(variables)-1]+')+"')
		pf.write(')")\n\
		except RuntimeError,e:\n\
			rospy.logerr("Exception caught:\\n%s", e)\n\
		return S'+proxyName+'_'+command+'Response(')
		if reply == 'vector string':
			pf.write('str(proxyrep), int(proxyid))')
		elif reply == 'vector float':
			pf.write('str(proxyrep), int(proxyid))')
		elif reply == 'std_msgs/Empty':
			pf.write('proxyrep, int(proxyid))')
		else:
			pf.write('proxyrep, int(proxyid))')
	pf.write('\n\
\n\
\n\
def make_float_array(msg):\n\
	ret_msg = []\n\
	message = []\n\
	message = msg.split(\'[[\')\n\
	if len(message) > 1: #nested\n\
		message = message[1].split(\']]\')\n\
		message = message[0].split(\'], [\')\n\
		for line in message:\n\
			line = line.split(\', \')\n\
			if len(line) > 1: #more than one val\n\
				vals = []\n\
				for val in line:\n\
					vals.append(float(val))\n\
				ret_msg.append(vals)\n\
			else:\n\
				ret_msg.append(float(line[0]))\n\
	else:\n\
		message = message[0].split(\'[\')\n\
		if len(message) > 1: #array\n\
			message = message[1].split(\']\')\n\
			message = message[0].split(\', \')\n\
			for val in message:\n\
				ret_msg.append(float(val))\n\
		else:\n\
			ret_msg = (float(message[0]))\n\
	return ret_msg\n\
\n\
def make_string_array(msg):\n\
	ret_msg = []\n\
	message = []\n\
	message = msg.split(\'[[\\\'\')\n\
	if len(message) > 1:\n\
		message = message[1].split(\'\\\']]\')\n\
		message = message[0].split(\'\\\'], [\\\'\')\n\
		for line in message:\n\
			line = line.split(\'\\\', \\\'\')\n\
			if len(line) > 1:\n\
				vals = []\n\
				for val in line:\n\
					vals.append(str(val))\n\
				ret_msg.append(vals)\n\
			else:\n\
				ret_msg.append(str(line[0]))\n\
	else:\n\
		message = message[0].split(\'[\\\'\')\n\
		if len(message) > 1:\n\
			message = message[1].split(\'\\\']\')\n\
			message = message[0].split(\'\\\', \\\'\')\n\
			for val in message:\n\
				ret_msg.append(str(val))\n\
		else:\n\
			ret_msg = (str(message[0]))\n\
	return ret_msg\n\
\n\
\n\
\n\
if __name__ == \'__main__\':\n\
	name = os.path.splitext(os.path.basename(sys.argv[0]))[0]\n\
	print name\n\
	node = Nao(name)\n\
	rospy.loginfo("%s running...", name)\n\
	rospy.spin()\n\
	rospy.loginfo("%s stopped.", name)\n\
	exit(0)')
	pf.close()
	print "Python ros node (back-end) created"


	print "Creating service files..."
#'+proxyName+'
#'+command+'
	folder = proxyName+'Srv'
	path = './APImadeFiles/'
	if not os.path.exists(path+folder):
		os.makedirs(path+folder)
	for line in commands:
		[command, variables, types, reply] = line
		name =  folder+'/'+'S'+proxyName+'_'+command+'.srv'
		print "  "+name
		name = path+name
		s = open(name, 'w')
		s.write("#")
		s.write(str(line))
		s.write("\n\n")
		for x in range(0, len(types)):
			if types[x] == 'vector string' or types[x] == 'vector float':
				s.write('string '+variables[x]+'\n')
			else:
				s.write(types[x]+' '+variables[x]+'\n')
		s.write('bool post')
		s.write("\n---\n")
		if reply == 'vector string' or reply == 'vector float':
			s.write('string reply')
		else:
			s.write(reply+' reply')
		s.write('\nuint32 taskid')
		s.close()
		os.chmod('./APImadeFiles/'+filename+'.py', stat.S_IRWXU)
	print "service files created, add the files above to CMakeLists.txt, then add the FOLDER with the service files in, to the srv folder and catkin_make to get them created."


	print "Creating front end for the api..."
	fe = open('./APImadeFiles/plancs_nao_'+proxyName+'.py', 'w')
	fe.write('import rospy\n\
\n\
import roslib; roslib.load_manifest(\'plancs\')\n\
from .srv import *\n\
\n\
\n\
class '+proxyName+'(object):\n\
	def __init__(self):\n')
	for line in commands:
		[command, variables, types, reply] = line
		fe.write('\t\tself.'+proxyName+'_'+command+' = rospy.ServiceProxy("nao_'+proxyName+'_'+command+'", S'+proxyName+'_'+command+')\n')
	fe.write('\t\tself.post = '+proxyName+'Post(self)\n')
		
	for line in commands:
		[command, variables, types, reply] = line
		fe.write('\tdef '+command+'(self')
		for variable in variables:
			vartype = types.pop(0)
			if vartype == 'std_msgs/Empty':
				fe.write(', *msg')
			else:
				fe.write(', '+variable)
			types.append(vartype)
		fe.write('):\n')
		for variable in variables:
			vartype = types.pop(0)
			if vartype == 'vector string' or vartype == 'vector float':
				fe.write('\t\t'+variable+' = str('+variable+')\n')
			types.append(vartype)
		fe.write('\t\treturn ')
		if reply == 'vector string':
			fe.write('make_string_array(')
		elif reply == 'vector float':
			fe.write('make_float_array(')
		fe.write('self.'+proxyName+'_'+command+'(')
		for variable in variables:
			vartype = types.pop(0)
			if vartype != 'std_msgs/Empty':
				fe.write(variable+', ')
			else:
				fe.write('\'\', ')
			types.append(vartype)
		fe.write('False).reply')
		if reply == 'vector string':
			fe.write(')')
		elif reply == 'vector float':
			fe.write(')')
		fe.write('\n')

	fe.write('\n\nclass '+proxyName+'Post(object):\n\
	def __init__(self, proxy):\n\
		self.proxy = proxy\n')
	for line in commands:
		[command, variables, types, reply] = line
		fe.write('\tdef '+command+'(self')
		for variable in variables:
			vartype = types.pop(0)
			if vartype == 'std_msgs/Empty':
				fe.write(', msg')
			else:
				fe.write(', '+variable)
			types.append(vartype)
		fe.write('):\n')
		for variable in variables:
			vartype = types.pop(0)
			if vartype == 'vector string' or vartype == 'vector float':
				fe.write('\t\t'+variable+' = str('+variable+')\n')
			types.append(vartype)
		fe.write('\t\treturn self.proxy.'+proxyName+'_'+command+'(')
		for variable in variables:
			vartype = types.pop(0)
			if vartype != 'std_msgs/Empty':
				fe.write(variable+', ')
			else:
				fe.write('\'\', ')
			types.append(vartype)
		fe.write('True).taskid\n')
	fe.write('\n\ndef make_float_array(msg):\n\
	ret_msg = []\n\
	message = []\n\
	message = msg.split(\'[[\')\n\
	if len(message) > 1: #nested\n\
		message = message[1].split(\']]\')\n\
		message = message[0].split(\'], [\')\n\
		for line in message:\n\
			line = line.split(\', \')\n\
			if len(line) > 1: #more than one val\n\
				vals = []\n\
				for val in line:\n\
					vals.append(float(val))\n\
				ret_msg.append(vals)\n\
			else:\n\
				ret_msg.append(float(line[0]))\n\
	else:\n\
		message = message[0].split(\'[\')\n\
		if len(message) > 1: #array\n\
			message = message[1].split(\']\')\n\
			message = message[0].split(\', \')\n\
			for val in message:\n\
				ret_msg.append(float(val))\n\
		else:\n\
			ret_msg = (float(message[0]))\n\
	return ret_msg\n\
\n\
def make_string_array(msg):\n\
	ret_msg = []\n\
	message = []\n\
	message = msg.split(\'[[\\\'\')\n\
	if len(message) > 1: #nested\n\
		message = message[1].split(\'\\\']]\')\n\
		message = message[0].split(\'\\\'], [\\\'\')\n\
		for line in message:\n\
			line = line.split(\'\\\', \\\'\')\n\
			if len(line) > 1: #more than one val\n\
				vals = []\n\
				for val in line:\n\
					vals.append(str(val))\n\
				ret_msg.append(vals)\n\
			else:\n\
				ret_msg.append(str(line[0]))\n\
	else:\n\
		message = message[0].split(\'[\\\'\')\n\
		if len(message) > 1: #array\n\
			message = message[1].split(\'\\\']\')\n\
			message = message[0].split(\'\\\', \\\'\')\n\
			for val in message:\n\
				ret_msg.append(str(val))\n\
		else:\n\
			ret_msg = (str(message[0]))\n\
	return ret_msg\n\
\n')
	fe.close()
	print "All created."
