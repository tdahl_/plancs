# Welcome to the plancs core repository
Here is the code for the main plancs core plus the nao drivers. Credit for plancs core goes to Plymouth University United Kingdom, credit for some of the nao drivers goes to Frieburg University Germany.

Please click [here](https://bitbucket.org/tdahl_/plancs/wiki/Home) for the Wiki.

Once you have installed all the technologies and want access to our teleoperation experiment, please go to [the UserPLANCs Wiki.](https://bitbucket.org/tdahl_/userplancs/overview)

##Maintainer emails:##

torbjorn.dahl@plymouth.ac.uk

dominic.youel@students.plymouth.ac.uk

brian.viviers@students.plymouth.ac.uk