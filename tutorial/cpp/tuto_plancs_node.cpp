#include <ros/ros.h>				// To use ROS_INFO
#include "../plancs/PlancsNode.h"	// To use PlancsNode

using namespace plancs;				// To use PlancsNode functions

class MyNode: public PlancsNode
{
public:
	// Constructor
	MyNode(int argc, char* argv[]): PlancsNode(argc, argv, "my_super_node")
	{
		// Set my variables
	}

private:
	// Override the init function
	void init()
	{
		ROS_INFO("I do my init");
		set_sleep_time(1000000);	// 1s
		set_verbose(true);			// Activate the verbose mode
		set_decay_period(1.0);		// 1s
		set_excitation_offset(1);	// Always running unless getting inhibition
		set_inhibition_offset(0);	// No offset for inhibition

		// Declare nodes which can be inhibited or excited
		add_inhibiter("a_node_to_inhibit");
		add_inhibiter("another_node_to_inhibit");
		add_exciter("a_node_to_excite");
	}

	// Override the plancs_do function (called in the main loop)
	void plancs_do()
	{
		ROS_INFO("Lets Run and do my Job");
		activate_node();
	}

	// Called when receive a deactivation event
	void on_pause()
	{
		// Called when the node is deactivated (falling edge)
		ROS_INFO("Node paused");
	}

	// Called when receive an activation event
	void on_resume()
	{
		// Called when the node is activated (rising edge)
		ROS_INFO("Node resumed");
	}
};


// Program entry
int main(int argc, char *argv[])
{
	// Create your node
	MyNode node(argc, argv);

	//Start your node (blocking)
	node.start();
	return 0;
}
