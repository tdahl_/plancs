#!/usr/bin/env python
import sys
import time
import rospy
from plancs import *

class MyNode(PlancsNode):
    def __init__(self, argv, name):
        super(MyNode, self).__init__(argv, name)
        
    def init(self):
        rospy.loginfo('I do my init')
        self.set_sleep_time(1.0)
        self.set_verbose(True)
        self.set_decay_period(1.0)
        self.set_excitation_offset(1)
        self.set_inhibition_offset(0)
        
        self.add_inhibiter('a_node_to_inhibit')
        self.add_inhibiter(['a_node_to_inhibit2', 'a_third_node'])
        self.add_exciter('or_to_excite')
        
    def plancs_do(self):
        rospy.loginfo('Lets Run and do my Job')
        self.activate_node()
    
    def on_resume(self):
        rospy.loginfo('Node resumed')
        
    def on_pause(self):
        rospy.loginfo('Node paused')

if __name__ == '__main__':
    node = MyNode(None, 'my_super_node')
    node.start()