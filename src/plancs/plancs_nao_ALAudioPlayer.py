import rospy

import roslib; roslib.load_manifest('plancs')
from .srv import *


class ALAudioPlayer(object):
	def __init__(self):
		self.ALAudioPlayer_playFile = rospy.ServiceProxy("nao_ALAudioPlayer_playFile", SALAudioPlayer_playFile)
		self.ALAudioPlayer_getLoadedFilesNames = rospy.ServiceProxy("nao_ALAudioPlayer_getLoadedFilesNames", SALAudioPlayer_getLoadedFilesNames)
		self.ALAudioPlayer_loadFile = rospy.ServiceProxy("nao_ALAudioPlayer_loadFile", SALAudioPlayer_loadFile)
		self.ALAudioPlayer_play = rospy.ServiceProxy("nao_ALAudioPlayer_play", SALAudioPlayer_play)

	def playFile(self, filename):
		return self.ALAudioPlayer_playFile(filename)
	def getLoadedFilesNames(self):
		return self.ALAudioPlayer_getLoadedFilesNames()
	def loadFile(self, fileName):
		return self.ALAudioPlayer_loadFile(fileName)
	def play(self, fileID):
		return self.ALAudioPlayer_play(fileID)









