/**
 * @file PlancsNode.h
 * @brief Implementation file of the PlancsNode class
 * @author mthibaud
 * @date 11/06/2015
 */

#include "PlancsNode.h"

#include <cstdlib>

namespace plancs
{
	using namespace std;
	using namespace ros;

	PlancsNode::PlancsNode (int argc, char *argv[], std::string name)
	{
		// Set minimal parameters to default values
		initialised = false;
		asynchrone_mode = false;
		this->name = name;
  		verbose = false;
  		nh = NULL;
  		decay_period = Standard_decay_period;
  		inhibition_offset = 0;
  		excitation_offset = 1;
  		inhibition_list.empty();
  		excitation_list.empty();
  		inhibition_count = excitation_offset - inhibition_offset;
  		worker = NULL;
  		sleep_time = 0;
  		was_inhibited = false;

  		// Verify the required informations
  		if(name.empty())
  		{
  			ROS_ERROR("YOU MUST SPECIFY A NODE NAME!");
  			exit(EXIT_FAILURE);	// Exit the program to avoid runtime errors
  		}
  		// Open ros connection
  		ros::init(argc, argv, name.c_str());
  		nh = new NodeHandle();
	}

	PlancsNode::~PlancsNode ()
	{
		// Stops the thread and delete it
		if(worker)
		{
			worker->join();
			delete worker;
			worker = NULL;
		}
		delete nh;
	}

	void PlancsNode::start()
	{
		// Call custom init method from the inherited class
		 init();

		 // Then tell the node that init is finished
		 initialised = true;

		 // Create its own inhibition and excitation receptor
		 inhibit_sub = nh->subscribe(string(name+"/inhibit").c_str(), 5, &PlancsNode::inhibition_callback, this);
		 excite_sub = nh->subscribe(string(name+"/excite").c_str(), 5, &PlancsNode::excitation_callback, this);

		 // Open all publishers from the list specified in the init
		 open_publishers();

		 // The following line has not been tested: If true, the callbacks are called asynchronously: requires a high performance system
		 if(asynchrone_mode)
		 {
			 ros::AsyncSpinner spinner(1);	// Allows callbacks to be called in a different thread
			 spinner.start();
		 }
		 // Create the thread for inhibition/excitation/callbacks(if asynchronous mode deactiveted)
		 worker = new boost::thread(&PlancsNode::activation_watcher,this);

		 if(verbose)
			 ROS_INFO("Starting %s", name.c_str());

		 // Run the custom loop function
		 while(ros::ok())
		 {
			active = false;
			// Check if activated then run plancs_do
		  	if(inhibition_count > 0)
		  	{
		  		plancs_do();
		  		if(active)	// Activated by the user
		  		{
		  			notify_subscribers();
		  		}
		  	}else	// Otherwise, notify it has been inhibited and could not be run
		  	{
		  		was_inhibited = true;
		  	}
		  	// Apply specified delay
		  	if(sleep_time > 0)
		  		usleep(sleep_time);
		 }
		 // The ros::ok() returned false: The program is asked to stop

		 // Notify all the sub classes that the node is not usable anymore
		 initialised = false;
		 // Stop the worker thread
		 worker->join();
	}

	void PlancsNode::add_inhibiter(std::string name)
	{
		if(!initialised)
		{
			// Add the inhibiter to the list
			publisher_t pub;
			pub.dest = name;
			inhibiter_list.push_back(pub);
		}else
		{
			ROS_WARN("You must specify the subscribers in the init Function");
		}
	}

	void PlancsNode::add_exciter(std::string name)
	{
		if(!initialised)
		{
			// Add the exciter to the list
			publisher_t pub;
			pub.dest = name;
			exciter_list.push_back(pub);
		}else
		{
			ROS_WARN("You must specify the subscribers in the init Function");
		}
	}

	void PlancsNode::notify_subscribers()
	{
		plancs::Plancs_Inhibition msg;
		msg.emitter = this->name;
		for(int i = 0; i < inhibiter_list.size(); i++)
		{
			inhibiter_list.at(i).pub.publish(msg);
		}
		for(int i = 0; i < exciter_list.size(); i++)
		{
			exciter_list.at(i).pub.publish(msg);
		}
	}

	void PlancsNode::open_publishers()
	{
		// For each publisher declared during the init phase, open the topic
		for(int i = 0; i < inhibiter_list.size(); i++)
		{
			string topic = inhibiter_list.at(i).dest + "/inhibit";
			inhibiter_list.at(i).pub = nh->advertise<plancs::Plancs_Inhibition>(topic.c_str(),1);
		}
		for(int i = 0; i < exciter_list.size(); i++)
		{
			string topic = exciter_list.at(i).dest + "/excite";
			exciter_list.at(i).pub = nh->advertise<plancs::Plancs_Excitation>(topic.c_str(),1);
		}
	}

	void PlancsNode::inhibition_callback(const plancs::Plancs_Inhibition::ConstPtr& msg)
	{
		// Only write in the console if the verbose mode is activated
		if(verbose)
		{
			ROS_INFO("%s received inhibition from: %s",name.c_str(), msg->emitter.c_str());
		}
		// Get the event (name and time its been received: the time can vary with the sleep time of the watcher thread)
		event_t new_event;
		new_event.emitter = msg->emitter;
		new_event.last_event = ros::Time::now();

		// Check if the event as already been handled
		// (if received multiple event from a the same node, the time is reset but the event is not added->one event on the activation count)
		bool new_emitter = true;
		for(int i = 0; i < inhibition_list.size(); i++)
		{
			// Check if already handled
			if(!inhibition_list.at(i).emitter.compare(new_event.emitter))
			{
				// Already handled: just update the time and block creation of a new event
				new_emitter = false;
				inhibition_list.at(i).last_event = new_event.last_event;
			}
		}
		// If new event: Add it the list of current event
		if(new_emitter)
		{
			inhibition_list.push_back(new_event);
		}

		// If no thread is launch to handle the event update, then create it:
		// This can only happen if asynchronous mode is activated because otherwise the callback is called from the watcher thread=> already running)
		if(!worker)
		{
			worker = new boost::thread(&PlancsNode::activation_watcher,this);
		}
	}

	void PlancsNode::excitation_callback(const plancs::Plancs_Excitation::ConstPtr& msg)
	{
		// This method has exactly the same structure as the excitation_callback method: Please refer to it for comments
		if(verbose)
		{
			ROS_INFO("Received excitation from: %s", msg->emitter.c_str());
		}

		event_t new_event;
		new_event.emitter = msg->emitter;
		new_event.last_event = ros::Time::now();

		// Check if the event as already been handled
		bool new_emitter = true;
		for(int i = 0; i < excitation_list.size(); i++)
		{
			if(!excitation_list.at(i).emitter.compare(new_event.emitter))
			{
				new_emitter = false;
				excitation_list.at(i).last_event = new_event.last_event;
			}
		}
		if(new_emitter)
		{
			excitation_list.push_back(new_event);
		}

		if(!worker)
		{
			worker = new boost::thread(&PlancsNode::activation_watcher,this);
		}
	}

	void PlancsNode::activation_watcher()
	{
		// Save the previous state to compare if changed at the end
		bool previous_state = inhibition_count > 0;
		// Run as long as the node is initialised
		while(initialised)
		{
			// IF no asynchronous mode, call all callbacks in the stack
			if(!asynchrone_mode)
				ros::spinOnce();	// Call callback functions

			// Get the current time to compare all events
			ros::Time current_time = ros::Time::now();

			// Remove all out-dated excitations
			for(int it = 0 ; it < excitation_list.size(); it++)
			{
				if(current_time >= excitation_list.at(it).last_event + ros::Duration(decay_period))
				{
					excitation_list.erase(excitation_list.begin() + it);
				}
			}

			// Remove all out-dated inhibitions
			for(int it = 0 ; it < inhibition_list.size(); it++)
			{
				if(current_time >= inhibition_list.at(it).last_event + ros::Duration(decay_period))
				{
					inhibition_list.erase(inhibition_list.begin() + it);
				}
			}
			// Count the new activation count
			inhibition_count =	excitation_offset + excitation_list.size()
								- inhibition_offset - inhibition_list.size();

			// Check if any change in activation and call the event methods
			if(inhibition_count > 0 && !previous_state)
			{
				on_resume();
			}else if(inhibition_count <= 0 && previous_state)
			{
				on_pause();
			}

			// Save the actual activation for the next loop occurence
			previous_state = inhibition_count > 0;

			// Sleep 50 ms
			ros::Duration(0, 50000000).sleep();	// 50ms
		}
	}


	// Accessors
	string PlancsNode::get_name()							{ return name; }
	NodeHandle *PlancsNode::get_node_handler()				{ return nh; }
	bool PlancsNode::get_verbose()							{ return verbose; }
	double PlancsNode::get_decay_period()					{ return decay_period; }
	double PlancsNode::get_Standard_decay_period()			{ return Standard_decay_period; }
	int PlancsNode::get_inhibition_offset()					{ return inhibition_offset; }
	int PlancsNode::get_excitation_offset()					{ return excitation_offset; }
	int PlancsNode::get_activation_count()					{ return inhibition_count; }
	int PlancsNode::get_inhibition_count()					{ return (int)inhibition_list.size(); }
	int PlancsNode::get_excitation_count()					{ return (int)excitation_list.size(); }
	int PlancsNode::get_sleep_time()						{ return sleep_time; }
	bool PlancsNode::get_asynchrone_mode()					{ return asynchrone_mode; }
	bool PlancsNode::was_interrupted()
	{
		if(was_inhibited)
		{
			was_inhibited = false;	// Reset
			return true;
		}
		return false;
	}


	void PlancsNode::set_verbose(bool verbose)				{ this->verbose = verbose; }
	void PlancsNode::set_decay_period(double decay_period)	{ this->decay_period = decay_period; }
	void PlancsNode::reset_decay_period()					{ decay_period = Standard_decay_period; }
	void PlancsNode::set_inhibition_offset(int offset)		{ inhibition_offset = offset; }
	void PlancsNode::set_excitation_offset(int offset)		{ excitation_offset = offset; }
	void PlancsNode::set_sleep_time(int us)					{ sleep_time = us; }
	void PlancsNode::set_asynchrone_mode(bool mode)
	{
		if(!initialised)
			asynchrone_mode = mode;
		else
			ROS_WARN("You cannot modify the spinner mode after starting ROS: Operation ignored");
	}

	void PlancsNode::activate_node()
	{
		active = true;
	}
	bool PlancsNode::is_activated()
	{
		return active;
	}
	bool PlancsNode::is_inhibited()
	{
		return inhibition_count <= 0;
	}

} /* namespace plancs */
