import sys
import os
import rospy
import threading
from time import time

from std_msgs.msg import String, Empty, Header
from rospy.impl.tcpros import DEFAULT_BUFF_SIZE
import std_srvs.srv

import roslib; roslib.load_manifest('plancs')
from .srv import *
#from plancs.msg import Plancs_Inhibit

class Plancs_Node(object):
	def __init__(self, name, argv=None, anonymous=False, log_level=None, disable_rostime=False, disable_rosout=False, disable_signals=False, inhibited=False, debug=False, quiet=True, verbose=False):

		self.dplancs = rospy.Subscriber("/dplancs", String, self.connectState)
		self.quiet = quiet
		self.verbose = verbose
		self.inhibited = inhibited
		self.debug = debug
		self.subscribers = []
		self.services = {} # {name : Service}
		self.last_inhibit = 0
		self.worker = threading.Thread (target=self.release_inhibit)
		#self.worker2 = threading.Thread (target=self.release_inhibit2)
		self.standard_decay = 0.1
		self.inhibit_decay = 0.1
		self.topic_names = []
		self.callback_names = []

		if anonymous:
			print "Plancs node '"+name+"' tried to be initialised anonymous, this is not allowed."
			sys.exit()

		rospy.init_node(name, argv, anonymous, log_level, disable_rostime, disable_rosout, disable_signals)
		if not self.quiet:
			print "Plancs node '"+name+"' initiated"




		self.plancs_inhibit_static_subscriber_name = rospy.get_name()+'/plancs_inhibit_static'
		rospy.Subscriber(self.plancs_inhibit_static_subscriber_name, String, self.inhibit_static)
		if verbose:
			print("plancs_inhibit_static Subscriber created")

		self.plancs_subscriber_name = rospy.get_name()+'/plancs_inhibit'
		rospy.Subscriber(self.plancs_subscriber_name, Empty, self.inhibit, queue_size=1)
		if verbose:
			print("plancs_inhibit Subscriber created")

		#self.plancs_subscriber2_name = rospy.get_name()+'/plancs_inhibit2'
		#rospy.Subscriber(self.plancs_subscriber2_name, Plancs_Inhibit, self.inhibit2)
		#if verbose:
		#	print("plancs_inhibit2 Subscriber created")







		rospy.Subscriber("debug", String, self.debug_callback)
		if verbose:
			print("debug Subscriber created")

		self.plancs_service_name = rospy.get_name()+'/plancs'
		self.plancs_service = rospy.Service(self.plancs_service_name, PlancsService, self.plancs_handle)
		if verbose:
			print self.plancs_service_name+" Service created"





	def sleep(self, duration):
		# this has to be part of the plancs node class so that it recieves node.inhibited!!
		rospy.sleep(duration)
		if self.inhibited:
			while self.inhibited and not rospy.is_shutdown():
				rospy.sleep(0.1)

	def set_decay_period(self, duration):
		self.inhibit_decay = duration
		
	def get_decay_period(self):
		return self.inhibit_decay
	
	def reset_decay_period(self):
		self.inhibit_decay = self.standard_decay
		

	def inhibit(self, data):
		self.last_inhibit = time()
		if self.inhibited == False:
			self.inhibited = True
			self.worker = threading.Thread (target=self.release_inhibit)
			self.worker.start()
			rospy.loginfo("Stopped")
			for sub in self.subscribers:
				if sub.impl:	# it is possible to have a topic/subscriber without a callback
					if sub.callback is not None: 
						sub.impl.remove_callback(sub.callback, sub.callback_args)
		else:
			rospy.loginfo("Re-Stopped")

	def release_inhibit(self):
		while (time() <= self.last_inhibit + self.inhibit_decay):
			rospy.sleep(0.001)
		self.inhibited = False
		rospy.loginfo("Started")
		for sub in self.subscribers:
			if sub.impl:	# it is possible to have a topic/subscriber without a callback
				if sub.callback is not None: 
					sub.impl.add_callback(sub.callback, sub.callback_args)
		

	#def inhibit2(self, data):
	#	print data.stamp.nsecs
	#	self.last_inhibit2 = time()
	#	if self.inhibited == False:
	#		self.inhibited = True
	#		self.worker2.start()
	#		rospy.loginfo("Stopped")
	#		for sub in self.subscribers:
	#			if sub.impl:	# it is possible to have a topic/subscriber without a callback
	#				if sub.callback is not None: 
	#					sub.impl.remove_callback(sub.callback, sub.callback_args)
	#	else:
	#		rospy.loginfo("Re-Stopped")

	#def release_inhibit2(self):
	#	while (time() <= self.last_inhibit2 + self.inhibit_decay):
	#		rospy.sleep(0.0001)
	#	self.inhibited = False
	#	rospy.loginfo("Started")
	#	for sub in self.subscribers:
	#		if sub.impl:	# it is possible to have a topic/subscriber without a callback
	#			if sub.callback is not None: 
	#				sub.impl.add_callback(sub.callback, sub.callback_args)
		




	def inhibit_static(self, data):
		if data.data == "stop" and self.inhibited == False:
			self.inhibited = True
			rospy.loginfo("Stopped")
			for sub in self.subscribers:
				if sub.impl:	# it is possible to have a topic/subscriber without a callback
					if sub.callback is not None: 
						sub.impl.remove_callback(sub.callback, sub.callback_args)
		elif data.data == "start" and self.inhibited == True:
			self.inhibited = False
			rospy.loginfo("Started")
			for sub in self.subscribers:
				if sub.impl:	# it is possible to have a topic/subscriber without a callback
					if sub.callback is not None: 
						sub.impl.add_callback(sub.callback, sub.callback_args)
		

	def debug_callback(self, data):
		if data.data=="debug on" and self.debug==False:
			self.debug=True
			rospy.loginfo("Debug is changed to %s",debug)
		elif data.data=="debug off" and self.debug==True:
			self.debug=False
			rospy.loginfo("Debug is changed to %s",debug)

	def add_subscriber(self, name, data_class, callback=None, callback_args=None, queue_size=None, buff_size=DEFAULT_BUFF_SIZE, tcp_nodelay=False):
		# append create new sub to stack, then return it

		if name[0] != "/":
			name = "/" + name
		self.topic_names.append(name)        # dplancs
		self.callback_names.append(callback) # dplancs
		sub = rospy.Subscriber(name, data_class, callback, callback_args, queue_size, buff_size, tcp_nodelay)
		if self.inhibited:
			if sub.impl:	# it is possible to have a topic/subscriber without a callback
				if sub.callback is not None: 
					sub.impl.remove_callback(sub.callback, sub.callback_args)
		self.subscribers.append(sub)
		if self.verbose:
			rospy.loginfo("Subscriber '%s' created and inhibited is %s", name, self.inhibited)
		return sub

	def create_service(self, name, service_class, handler, buff_size=DEFAULT_BUFF_SIZE):
		srv = rospy.Service(name, service_class, handler, buff_size=DEFAULT_BUFF_SIZE)
		self.services[name] = srv
		if self.verbose:
			rospy.loginfo("Service '%s' created and inhibited is %s", name, self.inhibited)
		return srv

	def plancs_handle(self, data):
		if data.request == "state":
			return PlancsServiceResponse("state", self.inhibited)

	def do(self, loop, arg):
		while not rospy.is_shutdown():
			if arg:
				print "stuff"
				# so some kind of fork here


	def connectState(self, message):
		msg = []
		msg = message.data.split(' ')
		for x in range(len(msg)):
			if msg[x][0] != "/":
				msg[x] = "/" + msg[x]
		if rospy.get_name() == msg[1]:
			if msg[0] == "/disconnect":
				for i in range(len(self.topic_names)):
					if msg[2] == self.topic_names[i]:
						self.subscribers[i].unregister()
						rospy.loginfo("Topic '" + self.topic_names[i] + "' is disconnected from node: " + rospy.get_name())
			else:
				for i in range(len(self.topic_names)):
					if msg[2] == self.topic_names[i]:
						self.subscribers[i].unregister()
						self.topic_names[i] = msg[3]
						self.subscribers[i] = rospy.Subscriber(self.topic_names[i], String, self.callback_names[i])
						rospy.loginfo("Topic '" + self.topic_names[i] + "' is connected to node: " + rospy.get_name())

