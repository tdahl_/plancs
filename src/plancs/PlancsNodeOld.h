/**
 * @file PlancsNodeOld.h
 * @date 01/06/2015
 * @author mthibaud
 * Deprecated
 */

#ifndef PLANCS_SRC_PLANCS_PLANCSNODEOLD_H_
#define PLANCS_SRC_PLANCS_PLANCSNODEOLD_H_

#include <string>
#include <boost/thread.hpp>
#include <ros/ros.h>
#include "std_msgs/String.h"

using namespace std;
using namespace ros;
using namespace boost;

namespace plancsOld
{

class PlancsNode {
protected:
	bool running;
	string name;
	NodeHandle *nh;
	bool verbose;
	bool inhibited;
	double decay_period; // s
	const double Standard_decay_period = 0.1;
	ros::Subscriber inhibit_sub;
	boost::thread *worker;
	ros::Time last_inhibition;


public:
	PlancsNode(int argc, char *argv[], string name, bool verbose);
	~PlancsNode();

public:
	string get_name();
	NodeHandle *get_node_handler();
	bool get_verbose();
	bool is_inhibited();
	double get_decay_period(); // s
	double get_Standard_decay_period();

	void set_verbose(bool verbose);
	void set_decay_period(double decay_period);
	void reset_decay_period();

protected:
	void inhibit(const std_msgs::String::ConstPtr& msg);
	void release_inhibit();

public:
	void wait_inhibition();

};

}

#endif /* PLANCS_SRC_PLANCS_PLANCSNODEOLD_H_ */
