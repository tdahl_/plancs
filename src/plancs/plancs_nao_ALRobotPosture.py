import rospy

import roslib; roslib.load_manifest('plancs')
from .srv import *


class ALRobotPosture(object):
	def __init__(self):
		self.ALRobotPosture_getPostureList = rospy.ServiceProxy("nao_ALRobotPosture_getPostureList", SALRobotPosture_getPostureList)
		self.ALRobotPosture_goToPosture = rospy.ServiceProxy("nao_ALRobotPosture_goToPosture", SALRobotPosture_goToPosture)
		self.ALRobotPosture_applyPosture = rospy.ServiceProxy("nao_ALRobotPosture_applyPosture", SALRobotPosture_applyPosture)
		self.ALRobotPosture_stopMove = rospy.ServiceProxy("nao_ALRobotPosture_stopMove", SALRobotPosture_stopMove)
		self.ALRobotPosture_getPostureFamily = rospy.ServiceProxy("nao_ALRobotPosture_getPostureFamily", SALRobotPosture_getPostureFamily)
		self.ALRobotPosture_getPostureFamilyList = rospy.ServiceProxy("nao_ALRobotPosture_getPostureFamilyList", SALRobotPosture_getPostureFamilyList)
		self.ALRobotPosture_setMaxTryNumber = rospy.ServiceProxy("nao_ALRobotPosture_setMaxTryNumber", SALRobotPosture_setMaxTryNumber)
		self.post = ALRobotPosturePost(self)
	def getPostureList(self, *msg):
		return make_string_array(self.ALRobotPosture_getPostureList('', False).reply)
	def goToPosture(self, postureName, speed):
		return self.ALRobotPosture_goToPosture(postureName, speed, False).reply
	def applyPosture(self, postureName, speed):
		return self.ALRobotPosture_applyPosture(postureName, speed, False).reply
	def stopMove(self, *msg):
		return self.ALRobotPosture_stopMove('', False).reply
	def getPostureFamily(self, *msg):
		return self.ALRobotPosture_getPostureFamily('', False).reply
	def getPostureFamilyList(self, *msg):
		return make_string_array(self.ALRobotPosture_getPostureFamilyList('', False).reply)
	def setMaxTryNumber(self, maxTryNumber):
		return self.ALRobotPosture_setMaxTryNumber(maxTryNumber, False).reply


class ALRobotPosturePost(object):
	def __init__(self, proxy):
		self.proxy = proxy
	def getPostureList(self, msg):
		return self.proxy.ALRobotPosture_getPostureList('', True).taskid
	def goToPosture(self, postureName, speed):
		return self.proxy.ALRobotPosture_goToPosture(postureName, speed, True).taskid
	def applyPosture(self, postureName, speed):
		return self.proxy.ALRobotPosture_applyPosture(postureName, speed, True).taskid
	def stopMove(self, msg):
		return self.proxy.ALRobotPosture_stopMove('', True).taskid
	def getPostureFamily(self, msg):
		return self.proxy.ALRobotPosture_getPostureFamily('', True).taskid
	def getPostureFamilyList(self, msg):
		return self.proxy.ALRobotPosture_getPostureFamilyList('', True).taskid
	def setMaxTryNumber(self, maxTryNumber):
		return self.proxy.ALRobotPosture_setMaxTryNumber(maxTryNumber, True).taskid


def make_float_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[')
	if len(message) > 1: #nested
		message = message[1].split(']]')
		message = message[0].split('], [')
		for line in message:
			line = line.split(', ')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(float(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(float(line[0]))
	else:
		message = message[0].split('[')
		if len(message) > 1: #array
			message = message[1].split(']')
			message = message[0].split(', ')
			for val in message:
				ret_msg.append(float(val))
		else:
			ret_msg = (float(message[0]))
	return ret_msg

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1: #nested
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
	else:
		message = message[0].split('[\'')
		if len(message) > 1: #array
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg

