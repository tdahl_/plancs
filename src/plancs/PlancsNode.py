##
# @file PlancsNode.py
# The Plancs node class handle all plancs low level
# @author mthibaud
# @date 21 jul 2015

import sys
import os
import rospy
import signal
import threading
import time
from .msg import *


class PlancsNode(object):
    Standard_decay_period = 0.1
    ##
    # Constructor
    def __init__(self, argv = None, name = ''):
        # Declare and initialise the class variables to default values
        self.__initialised = False
        self.__name = name
        self.__verbose = False
        self.__decay_period = PlancsNode.Standard_decay_period
        self.__inhibition_offset = 0
        self.__excitation_offset = 1
        self.__inhibition_count = self.__excitation_offset - self.__inhibition_offset
        self.__previous_state = self.__inhibition_count > 0
        self.__sleep_time = 0
        self.__was_inhibited = False
        self.__inhibition_list = []
        self.__excitation_list = []
        self.__inhibiter_list = []
        self.__exciter_list = []
        self.__active = False
        self.__last_plancs_do = time.time()
        
        # Check the name
        if not self.__name:
            rospy.logfatal("You must specify an name for your node")
        
        # Initialise ros
        rospy.init_node(name, argv)
        
    def init(self):      
        raise Exception('init method MUST be overridden in the child class')
    
    def plancs_do(self):
        raise Exception('plancs_do method MUST be overridden in the child class')
    
    def on_pause(self):
        pass    # Ignore if not oferridden
    
    def on_resume(self):
        pass    # Ignore if not oferridden
        
    def start(self):
        # Call the overridden init method
        self.init()
        
        # Tell the node the initialization is finished
        self.__initialised = True
        
        # Create its own inhibition and excitation receptor
        rospy.Subscriber(self.__name+"/inhibit", Plancs_Inhibition, self.inhibition_callback)
        rospy.Subscriber(self.__name+"/excite", Plancs_Excitation, self.excitation_callback)
        
        # Open all the publishers from the list specified in the init
        self.open_publishers()
        
        
        # Run the custom loop function
        while not rospy.is_shutdown():        
            self.__active = False
            
            self.update_inhibition_count()
            
            # Check if activated
            if self.__inhibition_count > 0:
                # Get time
                t = time.time()
                # If time > last call + sleep time
                if (self.__last_plancs_do + self.__sleep_time-0.01) <= t:
                    # Save the last call time
                    self.__last_plancs_do = t
                    # Call plancs_do
                    self.plancs_do()
                    # If user activated the node, then call all inhibiters and exciters
                    if self.__active:
                        self.notify_subscribers()
            else:   # Otherwise, notify it has been inhibited and could not be run
                self.__was_inhibited = True
            
            # Apply decay time between two iterations of the plancs_do
            
            if self.__sleep_time > 0:
                time.sleep(self.__sleep_time)
            # 10 ms
            time.sleep(0.01)
        
        # Notify all the sub classes that the node is not usable anymore
        self.__initialised = False

    
    def add_publisher(self, name):
        if not self.__initialised:
            pub = [name, None, None]
            self.__publisher_list.append(pub)
        else:
            rospy.logwarn("You must specify the subscribers in the init Function")
            
    def add_inhibiter(self, names):
        if not self.__initialised:
            if isinstance(names, str):
                pub = [names, None]
                self.__inhibiter_list.append(pub)
            elif isinstance(names, list):
                for name in names:
                    pub = [name, None]
                    self.__inhibiter_list.append(pub)
        else:
            rospy.logwarn("You must specify the subscribers in the init Function")
                
    
    def add_exciter(self, names):
        if not self.__initialised:
            if isinstance(names, str):
                pub = [names, None]
                self.__exciter_list.append(pub)
            elif isinstance(names, list):
                for name in names:
                    pub = [name, None]
                    self.__exciter_list.append(pub)
        else:
            rospy.logwarn("You must specify the subscribers in the init Function")
            
    # Send all inhibitions and excitations to the specified nodes
    def notify_subscribers(self):
        msg = self.__name
        for i in self.__inhibiter_list:
            i[1].publish(msg)
        for i in self.__exciter_list:
            i[1].publish(msg)

    
    def open_publishers(self):
        for i in self.__inhibiter_list:        
            topic_name = i[0]+"/inhibit"
            i[1] = rospy.Publisher(topic_name, Plancs_Inhibition, queue_size=1)
        for i in self.__exciter_list:        
            topic_name = i[0]+"/excite"
            i[1] = rospy.Publisher(topic_name, Plancs_Inhibition, queue_size=1)
        
    
    def inhibition_callback(self, msg):
        if self.__verbose:
            rospy.loginfo("Received inhibition from: %s" % msg.emitter)
        
        event_name = msg.emitter
        event_time = time.time()
        event = [event_name, event_time]
        
        new_event = True
        for i in self.__inhibition_list:
            if event[0] == i[0]:
                new_event = False
                i[1] = time.time()
        if new_event:
            self.__inhibition_list.append(event)
    
    def excitation_callback(self, msg):
        if self.__verbose:
            rospy.loginfo("Received excitation from: %s" % msg.emitter)
        
        event_name = msg.emitter
        event_time = time.time()
        event = [event_name, event_time]
        
        new_event = True
        for i in self.__excitation_list:
            if event[0] == i[0]:
                new_event = False
                i[1] = time.time()
        if new_event:
            self.__excitation_list.append(event)
            
    def update_inhibition_count(self):
        current_time = time.time()
        for i in self.__inhibition_list:
            if current_time >= i[1] + self.__decay_period:
                self.__inhibition_list.remove(i)
                    
        for i in self.__excitation_list:
            if current_time >= i[1] + self.__decay_period:
                self.__excitation_list.remove(i)
        
        self.__inhibition_count = self.__excitation_offset + len(self.__excitation_list) - self.__inhibition_offset - len(self.__inhibition_list) 
    
        if self.__inhibition_count > 0 and not self.__previous_state:
            self.on_resume()
        elif self.__inhibition_count <= 0 and self.__previous_state:
            self.on_pause()
        self.__previous_state = self.__inhibition_count > 0
            
    # Accessors
    def get_name(self):
        return self.__name
    def get_verbose(self):
        return self.__verbose
    def get_decay_period(self):
        return self.__decay_period
    def get_Standard_decay_period(self):
        return self.Standard_decay_period
    def get_inhibition_offset(self):
        return self.__inhibition_offset
    def get_excitation_offset(self):
        return self.__excitation_offset
    def get_activation_count(self):
        return self.__inhibition_count 
    def get_inhibition_count(self):
        return len(self.__inhibition_list)
    def get_excitation_count(self):
        return len(self.__excitation_list)
    def get_sleep_time(self):
        return self.__sleep_time
    def get_asynchronous_mode(self):
        return True
    def was_interrupted(self):
        if self.__was_inhibited:
            self.__was_inhibited = False
            return True
        else:
            return False
    
    def set_verbose(self, verbose):
        self.__verbose = verbose
    def set_decay_period(self, period):
        self.__decay_period = period
    def reset_decay_petiod(self):
        self.__decay_period = self.Standard_decay_period
    def set_inhibition_offset(self, offset):
        self.__inhibition_offset = offset
    def set_excitation_offset(self, offset):
        self.__excitation_offset = offset
    def set_sleep_time(self, time):
        self.__sleep_time = time
    def set_asynchronous_mode(self, mode):
        if not mode:
            rospy.logwarn('Sorry but the asynchronous mode cannot be disabled in Python')
    
    def activate_node(self):
        self.__active = True
    def is_activated(self):
        return self.__active
    def is_inhibited(self):
        return self.__inhibition_count <= 0
    