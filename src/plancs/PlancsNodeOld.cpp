/*
 * @file PlancsNode.cpp
 * @date 01/06/2015
 * @author mthibaud
 * Deprecated
 */

#include "PlancsNodeOld.h"

#include <boost/bind.hpp>

using namespace std;
using namespace ros;

namespace plancsOld
{

PlancsNode::PlancsNode(int argc, char *argv[], string name, bool verbose) {
	int ret_op;

	running = true;
	this->verbose = verbose;
	inhibited = false;
	decay_period = Standard_decay_period;
	this->name = name;
	worker = NULL;
	ros::init(argc, argv,name.c_str());
	if(verbose)
		ROS_INFO("Node %s initialised", name.c_str());

	nh = new NodeHandle();

	// Subscribe inhibiter
	inhibit_sub = nh->subscribe(string(name+"/inhibit").c_str(),1, &PlancsNode::inhibit, this);
}

PlancsNode::~PlancsNode() {
	if(worker)
	{
		running = false;
		worker->join();
		delete worker;
		worker = NULL;
	}
	delete nh;
}

string PlancsNode::get_name()				{return name;}
NodeHandle *PlancsNode::get_node_handler()	{return nh;}
bool PlancsNode::get_verbose()				{return verbose;}
bool PlancsNode::is_inhibited()				{return inhibited;}
double PlancsNode::get_decay_period()			{return decay_period;}
double PlancsNode::get_Standard_decay_period()	{return Standard_decay_period;}

void PlancsNode::set_verbose(bool verbose)	{this->verbose = verbose;}
void PlancsNode::set_decay_period(double decay_period) {this->decay_period = decay_period;}
void PlancsNode::reset_decay_period() {this->decay_period = Standard_decay_period;}


void PlancsNode::inhibit(const std_msgs::String::ConstPtr& msg)
{
	last_inhibition = ros::Time::now();
	if(!inhibited)
	{
		inhibited = true;
		// Launch thread
		if(worker != NULL)
		{
			worker->join();
			delete worker;
			worker = NULL;
		}
		if(verbose)
			ROS_INFO("%s stopped", name.c_str());
		worker = new boost::thread(&PlancsNode::release_inhibit,this);
	}else
	{
		if(verbose)
			ROS_INFO("%s re-stopped", name.c_str());
	}
}

void PlancsNode::release_inhibit()
{
	ros::Duration(0, (int)(decay_period*900000000)).sleep();	// decay Time x0.9 in case of preemption time
	while(ros::Time::now() <=  last_inhibition + ros::Duration(decay_period))
	{
		ros::Duration(0, 10000000).sleep();	// 10ms
	}
	inhibited = false;
	if(verbose)
		ROS_INFO("%s release", name.c_str());
}

void PlancsNode::wait_inhibition()
{
	// Blocks the thread until the inhibition is not released
	while(inhibited)
	{
		ros::Duration(0, 10000000).sleep();	// 10ms
	}

}

}
