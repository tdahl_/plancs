import rospy

import roslib; roslib.load_manifest('plancs')
from .srv import *
from .plancs_nao_ALMotion import ALMotion
from .plancs_nao_ALMotion_1 import ALMotion_1
from .plancs_nao_ALMotion_2 import ALMotion_2
from .plancs_nao_ALRobotPosture import ALRobotPosture
from .plancs_nao_ALAudioPlayer import ALAudioPlayer

class PALProxy(ALMotion, ALMotion_1, ALMotion_2, ALRobotPosture, ALAudioPlayer):
	def __init__(self, name, *rest):
		if name == "ALMotion":
			ALMotion.__init__(self)
		elif name == "ALRobotPosture":
			ALRobotPosture.__init__(self)
		elif name == "ALAudioPlayer":
			ALAudioPlayer.__init__(self)
		elif name == "ALMotion_1":
			ALMotion_1.__init__(self)
		elif name == "ALMotion_2":
			ALMotion_2.__init__(self)
