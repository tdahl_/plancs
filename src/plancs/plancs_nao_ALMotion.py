import rospy

import roslib; roslib.load_manifest('plancs')
from .srv import *


class ALMotion(object):
	def __init__(self):
		self.ALMotion_setWalkTargetVelocity = rospy.ServiceProxy("nao_ALMotion_setWalkTargetVelocity", SALMotion_setWalkTargetVelocity)
		self.ALMotion_setWalkArmsEnabled = rospy.ServiceProxy("nao_ALMotion_setWalkArmsEnabled", SALMotion_setWalkArmsEnabled)
		self.ALMotion_setStiffnesses = rospy.ServiceProxy("nao_ALMotion_setStiffnesses", SALMotion_setStiffnesses)
		self.ALMotion_wbEnable = rospy.ServiceProxy("nao_ALMotion_wbEnable", SALMotion_wbEnable)
		self.ALMotion_wbFootState = rospy.ServiceProxy("nao_ALMotion_wbFootState", SALMotion_wbFootState)
		self.ALMotion_wbEnableBalanceConstraint = rospy.ServiceProxy("nao_ALMotion_wbEnableBalanceConstraint", SALMotion_wbEnableBalanceConstraint)
		self.ALMotion_wbGoToBalance = rospy.ServiceProxy("nao_ALMotion_wbGoToBalance", SALMotion_wbGoToBalance)
		self.ALMotion_positionInterpolation = rospy.ServiceProxy("nao_ALMotion_positionInterpolation", SALMotion_positionInterpolation)
		self.ALMotion_wbEnableEffectorOptimization = rospy.ServiceProxy("nao_ALMotion_wbEnableEffectorOptimization", SALMotion_wbEnableEffectorOptimization)
		self.ALMotion_wakeUp = rospy.ServiceProxy("nao_ALMotion_wakeUp", SALMotion_wakeUp)
		self.ALMotion_rest = rospy.ServiceProxy("nao_ALMotion_rest", SALMotion_rest)
		self.ALMotion_angleInterpolation = rospy.ServiceProxy("nao_ALMotion_angleInterpolation", SALMotion_angleInterpolation)
		self.ALMotion_angleInterpolationWithSpeed = rospy.ServiceProxy("nao_ALMotion_angleInterpolationWithSpeed", SALMotion_angleInterpolationWithSpeed)
		self.ALMotion_setAngles = rospy.ServiceProxy("nao_ALMotion_setAngles", SALMotion_setAngles)
		self.ALMotion_changeAngles = rospy.ServiceProxy("nao_ALMotion_changeAngles", SALMotion_changeAngles)
		self.ALMotion_getAngles = rospy.ServiceProxy("nao_ALMotion_getAngles", SALMotion_getAngles)
		self.ALMotion_move = rospy.ServiceProxy("nao_ALMotion_move", SALMotion_move)
		self.ALMotion_moveTo = rospy.ServiceProxy("nao_ALMotion_moveTo", SALMotion_moveTo)
		self.ALMotion_moveIsActive = rospy.ServiceProxy("nao_ALMotion_moveIsActive", SALMotion_moveIsActive)
		self.ALMotion_killTasksUsingResources = rospy.ServiceProxy("nao_ALMotion_killTasksUsingResources", SALMotion_killTasksUsingResources)
		self.ALMotion_killMove = rospy.ServiceProxy("nao_ALMotion_killMove", SALMotion_killMove)
		self.ALMotion_setFallManagerEnabled = rospy.ServiceProxy("nao_ALMotion_setFallManagerEnabled", SALMotion_setFallManagerEnabled)
		self.post = ALMotionPost(self)

	def setWalkTargetVelocity(self, x, y, theta, frequency):
		return self.ALMotion_setWalkTargetVelocity(x, y, theta, frequency, False).reply
	def setWalkArmsEnabled(self, leftArmEnable, rightArmEnable):
		return self.ALMotion_setWalkArmsEnabled(leftArmEnable, rightArmEnable, False).reply
	def setStiffnesses(self, names, stiffnesses):
		names = str(names)
		stiffnesses = str(stiffnesses)
		return self.ALMotion_setStiffnesses(names, stiffnesses, False).reply
	def wbEnable(self, isEnabled):
		return self.ALMotion_wbEnable(isEnabled, False).reply
	def wbFootState(self, stateName, supportLeg):
		return self.ALMotion_wbFootState(stateName, supportLeg, False).reply
	def wbEnableBalanceConstraint(self, isEnable, supportLeg):
		return self.ALMotion_wbEnableBalanceConstraint(isEnable, supportLeg, False).reply
	def wbGoToBalance(self, supportLeg, duration):
		return self.ALMotion_wbGoToBalance(supportLeg, duration, False).reply
	def positionInterpolation(self, chainName, space, path, axisMask, durations, isAbsolute):
		path = str(path)
		durations = str(durations)
		return self.ALMotion_positionInterpolation(chainName, space, path, axisMask, durations, isAbsolute, False).reply
	def wbEnableEffectorOptimization(self, effectorName, isEnabled):
		return self.ALMotion_wbEnableEffectorOptimization(effectorName, isEnabled, False).reply
	def wakeUp(self, *msg):
		return self.ALMotion_wakeUp('', False).reply
	def rest(self, *msg):
		return self.ALMotion_rest('', False).reply
	def angleInterpolation(self, names, angleLists, timeLists, isAbsolute):
		names = str(names)
		angleLists = str(angleLists)
		timeLists = str(timeLists)
		return self.ALMotion_angleInterpolation(names, angleLists, timeLists, isAbsolute, False).reply
	def angleInterpolationWithSpeed(self, names, targetAngles, maxSpeedFraction):
		names = str(names)
		targetAngles = str(targetAngles)
		return self.ALMotion_angleInterpolationWithSpeed(names, targetAngles, maxSpeedFraction, False).reply
	def setAngles(self, names, angles, fractionMaxSpeed):
		names = str(names)
		angles = str(angles)
		return self.ALMotion_setAngles(names, angles, fractionMaxSpeed, False).reply
	def changeAngles(self, names, changes, fractionMaxSpeed):
		names = str(names)
		changes = str(changes)
		return self.ALMotion_changeAngles(names, changes, fractionMaxSpeed, False).reply
	def getAngles(self, names, useSensors):
		names = str(names)
		return make_float_array(self.ALMotion_getAngles(names, useSensors, False).reply)
	def move(self, x, y, theta):
		return self.ALMotion_move(x, y, theta, False).reply
	def moveTo(self, x, y, theta):
		return self.ALMotion_moveTo(x, y, theta, False).reply
	def moveIsActive(self, *msg):
		return self.ALMotion_moveIsActive('', False).reply
	def killTasksUsingResources(self, resourceNames):
		return self.ALMotion_killTasksUsingResources(resourceNames, False).reply
	def killMove(self, *msg):
		return self.ALMotion_killMove('', False).reply
	def setFallManagerEnabled(self, *msg):
		return self.ALMotion_setFallManagerEnabled('', False).reply


class ALMotionPost(object):
	def __init__(self, proxy):
		self.proxy = proxy
	def setWalkTargetVelocity(self, x, y, theta, frequency):
		return self.ALMotion_setWalkTargetVelocity(x, y, theta, frequency, True).taskid
	def setWalkArmsEnabled(self, leftArmEnable, rightArmEnable):
		leftArmEnable = str(leftArmEnable)
		rightArmEnable = str(rightArmEnable)
		return self.ALMotion_setWalkArmsEnabled(leftArmEnable, rightArmEnable, True).taskid
	def setStiffnesses(self, names, stiffnesses):
		names = str(names)
		stiffnesses = str(stiffnesses)
		return self.proxy.ALMotion_setStiffnesses(names, stiffnesses, True).taskid
	def wbEnable(self, isEnabled):
		return self.proxy.ALMotion_wbEnable(isEnabled, True).taskid
	def wbFootState(self, stateName, supportLeg):
		return self.proxy.ALMotion_wbFootState(stateName, supportLeg, True).taskid
	def wbEnableBalanceConstraint(self, isEnable, supportLeg):
		return self.proxy.ALMotion_wbEnableBalanceConstraint(isEnable, supportLeg, True).taskid
	def wbGoToBalance(self, supportLeg, duration):
		return self.proxy.ALMotion_wbGoToBalance(supportLeg, duration, True).taskid
	def positionInterpolation(self, chainName, space, path, axisMask, durations, isAbsolute):
		path = str(path)
		durations = str(durations)
		return self.proxy.ALMotion_positionInterpolation(chainName, space, path, axisMask, durations, isAbsolute, True).taskid
	def wbEnableEffectorOptimization(self, effectorName, isEnabled):
		return self.proxy.ALMotion_wbEnableEffectorOptimization(effectorName, isEnabled, True).taskid
	def wakeUp(self, msg):
		return self.proxy.ALMotion_wakeUp('', True).taskid
	def rest(self, msg):
		return self.proxy.ALMotion_rest('', True).taskid
	def angleInterpolation(self, names, angleLists, timeLists, isAbsolute):
		names = str(names)
		angleLists = str(angleLists)
		timeLists = str(timeLists)
		return self.proxy.ALMotion_angleInterpolation(names, angleLists, timeLists, isAbsolute, True).taskid
	def angleInterpolationWithSpeed(self, names, targetAngles, maxSpeedFraction):
		names = str(names)
		targetAngles = str(targetAngles)
		return self.proxy.ALMotion_angleInterpolationWithSpeed(names, targetAngles, maxSpeedFraction, True).taskid
	def setAngles(self, names, angles, fractionMaxSpeed):
		names = str(names)
		angles = str(angles)
		return self.proxy.ALMotion_setAngles(names, angles, fractionMaxSpeed, True).taskid
	def changeAngles(self, names, changes, fractionMaxSpeed):
		names = str(names)
		changes = str(changes)
		return self.proxy.ALMotion_changeAngles(names, changes, fractionMaxSpeed, True).taskid
	def getAngles(self, names, useSensors):
		names = str(names)
		return self.proxy.ALMotion_getAngles(names, useSensors, True).taskid
	def move(self, x, y, theta):
		return self.proxy.ALMotion_move(x, y, theta, True).taskid
	def moveTo(self, x, y, theta):
		return self.proxy.ALMotion_moveTo(x, y, theta, True).taskid
	def moveIsActive(self, msg):
		return self.proxy.ALMotion_moveIsActive('', True).taskid
	def killTasksUsingResources(self, resourceNames):
		return self.proxy.ALMotion_killTasksUsingResources(resourceNames, True).taskid
	def killMove(self, msg):
		return self.proxy.ALMotion_killMove('', True).taskid
	def setFallManagerEnabled(self, msg):
		return self.proxy.ALMotion_setFallManagerEnabled('', True).taskid


def make_float_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[')
	if len(message) > 1: #nested
		message = message[1].split(']]')
		message = message[0].split('], [')
		for line in message:
			line = line.split(', ')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(float(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(float(line[0]))
	else:
		message = message[0].split('[')
		if len(message) > 1: #array
			message = message[1].split(']')
			message = message[0].split(', ')
			for val in message:
				ret_msg.append(float(val))
		else:
			ret_msg = (float(message[0]))
	return ret_msg

def make_string_array(msg):
	ret_msg = []
	message = []
	message = msg.split('[[\'')
	if len(message) > 1: #nested
		message = message[1].split('\']]')
		message = message[0].split('\'], [\'')
		for line in message:
			line = line.split('\', \'')
			if len(line) > 1: #more than one val
				vals = []
				for val in line:
					vals.append(str(val))
				ret_msg.append(vals)
			else:
				ret_msg.append(str(line[0]))
	else:
		message = message[0].split('[\'')
		if len(message) > 1: #array
			message = message[1].split('\']')
			message = message[0].split('\', \'')
			for val in message:
				ret_msg.append(str(val))
		else:
			ret_msg = (str(message[0]))
	return ret_msg

