/**
 * @file PlancsNode.h
 * @brief Header file of the PlancsNode class
 * @author mthibaud
 * @date 11/06/2015
 */

#include <ros/ros.h>
#include <string>
#include <vector>
#include <boost/thread.hpp>
#include "plancs/Plancs_Inhibition.h"
#include "plancs/Plancs_Excitation.h"


#ifndef PLANCS_SRC_PLANCS_PLANCSNODEV2_H_
#define PLANCS_SRC_PLANCS_PLANCSNODEV2_H_

namespace plancs
{

	/**
	 * @class PlancsNode
	 * @brief Contains the required tools to create a plancs node
	 * (Uses Ros)
	 * Must be used as an abstract class (run and init must be overridden)
	 */
	class PlancsNode
	{
		typedef struct
		{
			std::string emitter;	/// The name of the emitter of the event
			ros::Time last_event;	/// The time it received it
		}event_t;

		typedef struct
		{
			std::string dest;			/// The name of the node to send
			ros::Publisher pub;	/// The publisher to send inhibit
		}publisher_t;

	// Attributes: Can only be accessed with the accessors
	private:
		// General
		bool initialised;			/// Is the node initialised
		std::string name;			/// Name of the node
		bool verbose;				/// Verbose mode
		int sleep_time;				/// Sleep time between two executions
		bool asynchrone_mode;		/// Dedicate a thread to the spinner

	protected:
		ros::NodeHandle *nh;		/// Ros node handler

		// Inhibition and excitation
		double decay_period;		/// Decay period of an inhibition
		ros::Subscriber inhibit_sub;/// Inhibition receiver
		ros::Subscriber excite_sub;	/// excitation receiver
		boost::thread *worker;		/// Contains the thread which count the decay period
		std::vector <event_t> inhibition_list;	/// The list of inhibition
		std::vector <event_t> excitation_list;	/// The list of excitations
		std::vector <publisher_t> inhibiter_list; /// The list of inhibiters to send inhibition
		std::vector <publisher_t> exciter_list; /// The list of exciters to send excitation
		int inhibition_offset;		/// Add a permanent inhibition
		int excitation_offset;		/// Add a permanent excitation
		int inhibition_count;		/// Sum of the excitation - inhibition: command the activation of the node
		bool was_inhibited;
		bool active = false;

		// Default values
		const double Standard_decay_period = 0.1;

	// Constructors
	protected:
		/**
		 * @brief The PlancsNode constructor
		 * @param argc The number of arguments of the program
		 * @param argv The entry arguments of the program
		 * @param name The name of the node (used in the ros topic/services and node name)
		 * It initialises ros
		 */
		PlancsNode (int argc, char *argv[], std::string name);

		/**
		 * @brief The PlancsNode Destructor
		 * You shouldn't have to call it as the program calls it automatically when start() ends
		 */
		~PlancsNode ();

	// Base methods (to be overridden)
	protected:
		/**
		 * @brief The init virtual method is called at the beginning after the constructor into the start function
		 * It must be overridden by the user node class which inherits the current
		 */
		virtual void init() = 0;			// Must be overridden

		/**
		 * @brief The plancs_do virtual method is called into a loop. It must contain the core of the node
		 * It must be overridden by the user node class which inherits the current
		 */
		virtual void plancs_do() = 0;		// Must be overridden

		/**
		 * @brief The on_pause method is a method called when the nodes gets deactivated because of inhibition
		 * by default, this method is empty but the user can override it in its own node to react on deactivation
		 * like stopping an action or saving a context
		 */
		virtual void on_pause(){}			// Can be overridden if needed

		/**
		 * @brief The on_resume method is a method called when the nodes gets activated after a deactivation because of end if inhibition or excitation
		 * by default, this method is empty but the user can override it in its own node to react on activation
		 * like resuming an action an action or loading a context
		 */
		virtual void on_resume(){}			// Can be overridden if needed

	// Shared methods
	public:
		/**
		 * @brief The start method contains the init function, the launch and the thread and then the plancs_do loop
		 * This is the most important method of the class and the only we need to call from the main to use the node
		 * Typically we only call the constructor of the inherited class followed by 'start' (cf wiki page for more details on the use)
		 */
		void start();				// Starts the node

		void add_inhibiter(std::string name);

		void add_exciter(std::string name);

	// Internal methods
	private:
		void notify_subscribers();

		/**
		 * @brief The open_publishers method creates the topics publishers for inhibition and excitation from the publisher list
		 */
		void open_publishers();

		/**
		 * @brief The inhibition_callback method is called when the node receives an inhibition
		 * @param msg The inhibition message containing the name of the sender
		 */
		void inhibition_callback(const plancs::Plancs_Inhibition::ConstPtr& msg);

		/**
		 * @brief The excitation_callback method  is called when the node receives an excitation
		 * @param msg The excitation message containing the name of the sender
		 */
		void excitation_callback(const plancs::Plancs_Excitation::ConstPtr& msg);

		/**
		 * @brief The activation_watcher method is the base of the second thread: It loops on checking the inhibitions/excitations and call all callbacks
		 */
		void activation_watcher();

	// Accessors
	public:
		std::string get_name();
		ros::NodeHandle *get_node_handler();
		bool get_verbose();
		double get_decay_period();
		double get_Standard_decay_period();
		int get_inhibition_offset();
		int get_excitation_offset();
		int get_activation_count();
		int get_inhibition_count();
		int get_excitation_count();
		int get_sleep_time();
		bool get_asynchrone_mode();
		bool was_interrupted();

		void set_verbose(bool verbose);
		void set_decay_period(double decay_period);
		void reset_decay_period();
		void set_inhibition_offset(int offset);
		void set_excitation_offset(int offset);
		void set_sleep_time(int us);
		void set_asynchrone_mode(bool mode);

		void activate_node();
		bool is_activated();
		/**
		 * @brief The is_inhibited method simply gives the state of the current node
		 * @return The state of the node: [true/false]=[active/inactive]
		 */
		bool is_inhibited();


	};

} /* namespace plancsV2 */

#endif /* PLANCS_SRC_PLANCS_PLANCSNODEV2_H_ */
