#include "circle_detection.h"
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;
using namespace std;

CircleDetection::CircleDetection(void)
{
	min_dist = DMINDIST;
	param1 = DP1;
	param2 = DP2;
	minRadius = DMINRAD;
	maxRadius = DMAXRAD;

	/*
	namedWindow(CIRCLE_WINDOW_NAME, WINDOW_AUTOSIZE);
	createTrackbar("min_dist",CIRCLE_WINDOW_NAME, &min_dist, 255);
	createTrackbar("param1",CIRCLE_WINDOW_NAME, &param1, 255);
	createTrackbar("param2",CIRCLE_WINDOW_NAME, &param2, 255);
	createTrackbar("minRadius",CIRCLE_WINDOW_NAME, &minRadius, 255);
	createTrackbar("maxRadius",CIRCLE_WINDOW_NAME, &maxRadius, 255);
	*/
}

vector<cv::Vec3f> CircleDetection::getCircles(Mat source)
{
	vector<cv::Vec3f> circles;
	HoughCircles(source, circles, CV_HOUGH_GRADIENT, 1,(double)min_dist,(double)param1, (double)param2, minRadius, maxRadius);
	//cv::HoughCircles(thresholded, circles, CV_HOUGH_GRADIENT, 2, 32, low,high);
	return circles;
}

Point CircleDetection::intelligentCircleSelection(std::vector<cv::Vec3f> circles)
{
	Point ret;
	static vector<cv::Vec3f> bestCircle;
	static int missing = 0;

	if(bestCircle.size() > 4)
	{
		bestCircle.erase(bestCircle.begin());
	}
	if(circles.size() != 0)
	{
		bestCircle.push_back(circles.front());
		if(missing > 0)
			missing --;
	}else{
		missing++;
	}
	if(missing == 4)
	{
		while(bestCircle.size() >0)
		{
			bestCircle.erase(bestCircle.begin());
		}
		missing = 0;
	}

	if(bestCircle.size() != 0)
	{
		for (int i = 0; i< bestCircle.size();i++)
		{
			ret.x += bestCircle[i][0];
			ret.y += bestCircle[i][1];
		}

		ret.x/=bestCircle.size();
		ret.y/=bestCircle.size();

		return ret;
	}else{
		return Point(-1,-1);
	}
}


Mat CircleDetection::drawCircles(Mat source, vector <Vec3f> circles)
{
	// Draw circles
	for(size_t i = 0; i < circles.size(); i++)
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		int radius = cvRound(circles[i][2]);

		circle(source, center, 3, Scalar(255,0,0), -1, 8, 0); // Center
		circle(source, center, radius, Scalar(255,0,255), 3, 8, 0); // outline
	}
	return source;
}

Mat CircleDetection::drawCircle(Mat source, cv::Point centre, int radius)
{
	if(centre.x > 0 && centre.y >0)
	{
		// Draw circle
		circle(source, centre, 3, Scalar(255,255,0), -1, 8, 0); // Center
		circle(source, centre, radius, Scalar(0,255,255), 3, 8, 0); // outline
	}

	return source;
}

