/**
 * @file scan_line_config.h
 * @brief The file scan_line_config.h
 * @author mthibaud
 * @date 13/05/2015
 */


#ifndef BALL_TRACKING_SRC_SCANLINE_SCAN_LINE_CONFIG_H_
#define BALL_TRACKING_SRC_SCANLINE_SCAN_LINE_CONFIG_H_


#define XOFFSET 4		// Pixels ignored from the border of the screen
#define YOFFSET 4		// because of a border effect which creates pixels with a wrong color



#endif /* BALL_TRACKING_SRC_SCANLINE_SCAN_LINE_CONFIG_H_ */
