/**
 * @file ball_detection.cpp
 * @date 03/06/2015
 * @author mthibaud
 */

#include "ball_detection/ball_detection.h"
#include <string>
#include <ros/ros.h>
#include <boost/bind.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "ball_detection/scan_line.h"
#include "ball_detection/circle_detection.h"
#include "ball_detection/processing_time.h"
#include "common/thresholding.h"

using namespace std;
using namespace ros;
using namespace plancs;
using namespace cv;
using namespace NaoCam;


#define BLUR_LEVEL 7 //17


BallDetectionNode::BallDetectionNode(int argc, char *argv[], string name, string resloc): PlancsNode(argc, argv, name)
{
	diffusionService = NULL;
	cam = NULL;
	image_received = false;
	this->resloc = resloc;
}

BallDetectionNode::~BallDetectionNode()
{
	delete cam;
}

void BallDetectionNode::init()
{
	set_decay_period(0.1);
	set_sleep_time(100000);		// 100 ms
	set_excitation_offset(1);
	set_inhibition_offset(0);
	set_asynchrone_mode(false);
	set_verbose(true);

	ros::param::set("~display_window",1);
	namedWindow("img",WINDOW_AUTOSIZE);
	diffusionService = new BallPosDiffusion(*get_node_handler());
	cam = new NaoCamera(*get_node_handler(),&BallDetectionNode::image_callback, this);
}

void BallDetectionNode::plancs_do()
{
	static ScanLine scan_line(resloc);
	static ThresholdingBGR to;
	static CircleDetection circle;

	int disp = 1;
	int k;
	Point ballPos;

	if(image_received)
	{
		// Copy of source image and blur it
		Mat blured;
		image_mutex.lock();		// Protect access to frame
		blured.create(frame.size(), frame.type());
		GaussianBlur(frame, blured, Size(BLUR_LEVEL, BLUR_LEVEL), 0, 0);
		image_received = false;
		image_mutex.unlock();	// Unlock access protection on frame

		/****************************************
		 * Scan Line method
		 * **************************************/
		// Process
		// If you want to add a method you can do it in this switch
		switch(method)
		{
			case SCAN_LINE:	// Scan line
				ballPos = scan_line.process(blured,blured);
				break;

			case CIRCLES:	// Hough circles
				{
					Mat img = to.threshold_image(blured);
					vector<cv::Vec3f> circles = circle.getCircles(img);
					ballPos = circle.intelligentCircleSelection(circles);
					blured = circle.drawCircle(blured,ballPos, cvRound((circles.size()>0)?circles[0][2]:10));
				}
				break;

			default:		// Default state = ERROR
				ROS_FATAL("Default state error");
				break;
		}

		// Publish the result
		diffusionService->sendBallPosition(ballPos.x,ballPos.y);

		/****************************************
		 * Common
		 * **************************************/

		if(disp)
			imshow("img", blured);
	}

	k = waitKey(10);

	switch(k)
	{
		case 27:
			ros::shutdown();
			break;

		case 'a':
			method = SCAN_LINE;
			break;

		case 'z':
			method = CIRCLES;
			break;

		default:
			// Do nothing
			break;
	}

}

void BallDetectionNode::image_callback(cv::Mat source)
{
	image_mutex.lock();		// Protect copy of the source into frame
	frame = source.clone();
	image_received = true;
	image_mutex.unlock();	// Release memory protection
}
