/**
 * @file ballposdiffusion.h
 * @brief The file ballposdiffusion.h
 * @author mthibaud
 * @date 18/06/2015
 */

#ifndef BALLPOSDIFFUSION_H
#define BALLPOSDIFFUSION_H

#include <ros/ros.h>
#include "plancs/GetScreenSize.h"

class BallPosDiffusion
{
private:
	ros::NodeHandle nh;						/** The node handler */
	ros::ServiceServer screenSizeServer;	/** Server which returns the size of the screen */
	ros::Publisher positionChatter;			/** Sends the position of the ball (general) */
	ros::Publisher head_tracking;			/** Sends the position of the ball (for the head object tracking) */

public:
	/**
	 * @brief The BallPosDiffusion constructor
	 * @param nh The node handler
	 */
	BallPosDiffusion(ros::NodeHandle nh);

private:
	/**
	 * @brief The screenSizeCallback method Sends the size of the screen to the client
	 * @param req The request (should be empty)
	 * @param res The response with the size of the screen
	 * @return Ackn
	 */
	static bool screenSizeCallback(plancs::GetScreenSize::Request  &req,
								   plancs::GetScreenSize::Response &res);
public:
	/**
	 * @brief The sendBallPosition method sends the position of the ball to the subscribers
	 * @param detected If the ball is detected
	 * @param xpos the position in x (default -1)
	 * @param ypos the position in y (default -1)
	 */
	void sendBallPosition(bool detected, int xpos = -1, int ypos = -1);

	/**
	 * @brief The sendBallPosition method sends the position of the ball to the subscribers
	 * @param xpos the position in x
	 * @param ypos the position in y
	 */
	void sendBallPosition(int xpos, int ypos);
};

#endif // BALLPOSDIFFUSION_H
