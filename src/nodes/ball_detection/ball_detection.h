/**
 * @file ball_detection.h
 * @brief The file ball_detection.h
 * @author mthibaud
 * @date 03/06/2015
 */

#ifndef BALL_DETECTION_H
#define BALL_DETECTION_H
#include <opencv2/core/core.hpp>
#include <boost/thread.hpp>
#include "PlancsNode.h"
#include "common/nao_camera.h"
#include "ball_detection/ball_pos_diffusion.h"

typedef enum {
	CIRCLES = 0,
	SCAN_LINE
}detection_method_e;

class BallDetectionNode: public plancs::PlancsNode
{
private:
	NaoCam::NaoCamera *cam;			 	/** The nao camera to get the image */
	BallPosDiffusion *diffusionService;	/** The object to diffuse the position via a topic */
	cv::Mat frame;						/** The current image f the camera */
	boost::mutex image_mutex;			/** A mutex to protect the access to the image */
	bool image_received = false;		/** Tells the main thread if a new image is available */
	std::string resloc;					/** The location of the res folder */
	detection_method_e method = CIRCLES;/** The type of detection method to use */

public:
	/**
	 * @brief The BallDetectionNode constructor
	 * @param argc Number of node entry arguments
	 * @param argv List of entry arguments
	 * @param name The name of the current node
	 * @param resloc The location of the res folder
	 */
	BallDetectionNode(int argc, char *argv[], std::string name, std::string resloc);

	/**
	 * @brief The BallDetectionNode Destructor
	 */
	~BallDetectionNode();

private:
	/**
	 * @brief The init method initialises the ball_detection node
	 * Overrides the plancs init method
	 */
	void init();

	/**
	 * @brief The plancs_do method Runs the node
	 * Overrides the plancs plancs_do method
	 */
	void plancs_do();

private:
	/**
	 * @brief The image_callback method method receives the image from the camera when available
	 * @param source The Mat which will contain the image
	 */
	void image_callback(cv::Mat source);
};

#endif // BALL_DETECTION_H
