/**
 * @file scan_line.h
 * @brief The file scan_line.h
 * @author mthibaud
 * @date 26/06/2015
 */

#ifndef SCAN_LINE_H
#define SCAN_LINE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "common/thresholding.h"	// For the threshold structure
#include "rapidxml/rapidxml.hpp"

class ScanLine
{
	// Types
public:
	typedef enum{
		OTHER = 0,
		ORANGE,
		GREEN,
		WHITE,
		YELLOW
	}color_t;

	//Attributes
private:
	bool initialised = false;	/** is the node initialised or not */
	int lineDistance;			/** distance between scan lines */
	ThresholdingBGR ballColor;	/** thresholds of the ball color */
	ThresholdingBGR fieldColor;	/** thresholds of the field color */
	ThresholdingBGR lineColor;	/** thresholds of the lines color */
	ThresholdingBGR goalColor;	/** thresholds of the goal color */

	// Constructors + init functions
public:
	/**
	 * @brief The ScanLine constructor
	 * @param resloc The location of the res folder
	 */
	ScanLine(std::string resloc);

private:
	/**
	 * @brief The initThresholds method initialises the thresholds by reading the xml file specified
	 * @param the xml file
	 */
	void initThresholds(std::string filename);

	/**
	 * @brief The getColor method get the thresholds of a particular color in the xml tree
	 * @param cur_node A node in the xml tree which contains the color
	 * @param colorName The specified color name
	 */
	ThresholdingBGR getColor(rapidxml::xml_node<>* cur_node, std::string colorName);

	// Process function : return the position of the ball on the screen
public:
	/**
	 * @brief The process method scan different lines in the image to extract the ball position and gives it back
	 * @param source The source image
	 * @param toDraw The image to draw the points
	 * @return The position of the ball
	 */
	cv::Point process(cv::Mat source, cv::Mat toDraw);

	// Other useful functions for the functioning of the class
private:
	/**
	 * @brief The scanOneLine method gets a line and scan the transitions and put it in a vector
	 * @param source The source image
	 * @param line The number of the line to scan
	 * @return The vector of points seen
	 */
	std::vector <cv::Point> scanOneLine(cv::Mat source, int line);

	/**
	 * @brief The scanOneRow method gets a row and scan the transitions and put it in a vector
	 * @param source The source image
	 * @param line The number of the line to scan
	 * @return The vector of points seen
	 */
	std::vector <cv::Point> scanOneRow(cv::Mat source, int row);

	/**
	 * @brief The isInRange method compares a pixel and give his color
	 * @param pixel The pixel to scan
	 * @return The color of the pixel
	 */
	color_t isInRange(cv::Vec3b pixel);

	/**
	 * @brief The drawPoints method draws the points on the toDraw image
	 * @param source The source image
	 * @param pointList The list of points to draw
	 * @param color The color of the points
	 */
	void drawPoints(cv::Mat source, std::vector <cv::Point> pointList, cv::Scalar colour);

	/**
	 * @brief The getCenter method takes the list of points detected and computes a center for the ball
	 * @param ptsLine Array of vectors containing the points of the lines
	 * @param ptsColumn Array of vectors containing the points of the rows
	 * @return The computed center of the ball
	 */
	cv::Point getCenter(std::vector<std::vector<cv::Point> > ptsLine,  std::vector<std::vector<cv::Point> > ptsColumn);

};

#endif // SCAN_LINE_H
