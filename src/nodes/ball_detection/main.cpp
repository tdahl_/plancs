/**
 * @file main.cpp
 * @date 26/06/2015
 * @author mthibaud
 */

#include "ball_detection/ball_detection.h"
#include "common/ArgumentExtractor.h"

/**
 * @brief The program entry
 */
int main(int argc, char *argv[])
{
	// Get the name of the node
	std::string name = "ball_detection";

	for(int i = 1; i < argc; i++)
	{
		arg_t arg = ArgumentExtractor::getArgument(argv[i]);
		if(!arg.name.compare("rrobot"))
		{
			name = name + "_" + arg.value; break;
		}
	}

	// Get the res folder location
	std::string resloc = "src/plancs/res";
	for(int i = 1; i < argc; i++)
	{
		arg_t arg = ArgumentExtractor::getArgument(argv[i]);
		if(!arg.name.compare("resloc"))
		{
			resloc = arg.value; break;
		}
	}

	// Create the node
	BallDetectionNode node(argc, argv, name, resloc);
	// Launch it
	node.start();
	return 0;
}
