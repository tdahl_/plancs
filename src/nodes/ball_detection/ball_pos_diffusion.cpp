/**
 * @file ballposdiffusion.cpp
 * @date 18/06/2015
 * @author mthibaud
 */

#include "ball_detection/ball_pos_diffusion.h"
#include "plancs/ObjectPositionOnCamera.h"

using namespace ros;
using namespace std;

BallPosDiffusion::BallPosDiffusion(ros::NodeHandle nh)
{
	this->nh = nh;
	screenSizeServer = nh.advertiseService("getScreenSize", screenSizeCallback);
	ROS_INFO("Service opened");
	positionChatter = nh.advertise<plancs::ObjectPositionOnCamera>("ball_position_on_screen",1);
	positionChatter = nh.advertise<plancs::ObjectPositionOnCamera>("ball_position_on_screen",1);
}


bool BallPosDiffusion::screenSizeCallback(plancs::GetScreenSize::Request  &req,
		   	   	   	   	   	   	   	   	  plancs::GetScreenSize::Response &res)
{
	res.height = 240;
	res.width = 320;
	return true;
}


void BallPosDiffusion::sendBallPosition(bool detected, int xpos, int ypos)
{
	plancs::ObjectPositionOnCamera msg;
	msg.detected = detected;
	msg.xPos = xpos;
	msg.yPos = ypos;

	positionChatter.publish(msg);
}

void BallPosDiffusion::sendBallPosition(int xpos, int ypos)
{
	sendBallPosition(((xpos+ypos)>0)?true:false,xpos,ypos);
}
