/**
 * @file processingtime.h
 * @brief The file processingtime.h
 * @author mthibaud
 * @date 18/06/2015
 */

#ifndef PROCESSINGTIME_H
#define PROCESSINGTIME_H
#include <time.h>

class processingTime
{
private:

	clock_t start;	/** Starting time */
	double sum;		/** sum of different times */
	double average;	/** The average of 10 times */
	int iteration; /** The number of iterations of the count */

public:
	/**
	 * @brief The processingTime constructor
	 */
	processingTime();

public:
	/**
	 * @brief The startCount method starts to count the time
	 */
	void startCount();

	/**
	 * @brief The stopCount method stops counting the time and gives the difference with the start time
	 * @return The time between the start and the stop
	 */
	double stopCount();

	/**
	 * @brief The getAverage method gives the average time for the last 10 iterations
	 * @return The average time of 10 iterations
	 */
	double getAverage(void);

	/**
	 * @brief The reset method resets the count to 0
	 */
	void reset();
};

#endif // PROCESSINGTIME_H
