/**
 * @file scan_line.cpp
 * @date 29/06/2015
 * @author mthibaud
 */

#include "ball_detection/scan_line.h"
#include <cstdlib>
#include <math.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include "ball_detection/scan_line_config.h"


using namespace std;
using namespace cv;
using namespace rapidxml;

//#define ADAPTIVE_SCAN_LINE

#define DISTANCE2PTS(ax,ay,bx,by) (sqrt((ax-bx)*(ax-bx)+(ay-by)*(ay-by)))
#define DIST 5


ScanLine::ScanLine(string resloc)
{
	lineDistance = DIST;
	initThresholds(resloc + "/colorThreshold.xml");
	initialised = true;
}

void ScanLine::initThresholds(std::string filename)
{
	string input_xml;
	string line;
	ifstream in(filename.c_str());

	while(getline(in,line))
		input_xml += line;
	vector<char> xml_copy(input_xml.begin(), input_xml.end());
	xml_copy.push_back('\0');

	xml_document<> doc;
	doc.parse<parse_declaration_node | parse_no_data_nodes>(&xml_copy[0]);
	xml_node<>* cur_node = doc.first_node("colorThreshold");	// Select the thresholds

	ballColor = getColor(cur_node, "orange");
	fieldColor = getColor(cur_node, "green");


	printf("orange:\n%d\t%d\n%d\t%d\n%d\t%d\n", ballColor.values.layer1_min, ballColor.values.layer1_max,
		   ballColor.values.layer2_min, ballColor.values.layer2_max,
		   ballColor.values.layer3_min, ballColor.values.layer3_max);
	printf("green:\n%d\t%d\n%d\t%d\n%d\t%d\n", fieldColor.values.layer1_min, fieldColor.values.layer1_max,
		   fieldColor.values.layer2_min, fieldColor.values.layer2_max,
		   fieldColor.values.layer3_min, fieldColor.values.layer3_max);

}

ThresholdingBGR ScanLine::getColor(xml_node<>* cur_node, std::string colorName)
{
	ThresholdingBGR threshold;
	cur_node = cur_node->first_node(colorName.c_str());

	// Get the blue component
	cur_node = cur_node->first_node("blueMin");
	threshold.values.layer1_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("blueMax");
	threshold.values.layer1_max = atoi(cur_node->value());

	// Get the green component
	cur_node = cur_node->next_sibling("greenMin");
	threshold.values.layer2_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("greenMax");
	threshold.values.layer2_max = atoi(cur_node->value());

	// Get the red component
	cur_node = cur_node->next_sibling("redMin");
	threshold.values.layer3_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("redMax");
	threshold.values.layer3_max = atoi(cur_node->value());

	// Restore the initial state
	cur_node = cur_node->parent()->parent();

	return threshold;
}

Point ScanLine::process(cv::Mat source, Mat toDraw)
{
	vector <Point> pts;
	vector<vector<Point> > ptsLine;
	vector<vector<Point> > ptsColumn;
	Point pcent;

	pcent.x = -1;
	pcent.y = -1;
	if(initialised)
	{
		for(int i = lineDistance; i<240-YOFFSET; i+=lineDistance)
		{
			// Get all transitions on one line
			pts = scanOneLine(source, i);

			// If there are more than two points (needed for a segment)
			if(pts.size()>1)
			{
				// Add it to the global list of points
				ptsLine.push_back(pts);
				drawPoints(toDraw, ptsLine.back(), Scalar(255,255,0));
			}
		}

		for(int i = lineDistance; i<320-XOFFSET; i+=lineDistance)
		{
			pts = scanOneRow(source, i);
			if(pts.size()>1)
			{
				ptsColumn.push_back(pts);
				drawPoints(toDraw, ptsColumn.back(), Scalar(0,255,255));
			}
		}
		pcent = getCenter(ptsLine,ptsColumn);

		if(pcent.x>0 && pcent.y>0)
		{
			circle(toDraw, pcent, 1, Scalar(255,0,0), 2, 8, 0);
		}
	}
	return pcent;
}

vector <cv::Point> ScanLine::scanOneLine(cv::Mat source, int line)
{
	Vec3b pixel;
	Vec3b previousPixel;
	vector <cv::Point> points;

	// For each pixel of the row
	for(int i = XOFFSET; i<source.cols-1-XOFFSET; i++)
	{
		pixel = source.at<Vec3b>(line, i);
		previousPixel = source.at<Vec3b>(line, i-1);

		// Detect a transition
		color_t pir = isInRange(pixel);
		color_t ppir = isInRange(previousPixel);
		if((pir == GREEN && ppir == ORANGE) || (ppir == GREEN && pir == ORANGE))	// from green to orange or orange to green
		//if((pir != ppir)&& (pir==ORANGE||ppir==ORANGE))
		{
			points.push_back(Point(i,line));
		}
	}
	return points;
}

vector <cv::Point> ScanLine::scanOneRow(cv::Mat source, int row)
{
	Vec3b pixel;
	Vec3b previousPixel;
	vector <cv::Point> points;

	// For each pixel of the row
	for(int i = YOFFSET; i<source.rows-1-YOFFSET; i++)
	{
		pixel = source.at<Vec3b>(i, row);
		previousPixel = source.at<Vec3b>(i-1, row);

		// Detect a transition
		color_t pir = isInRange(pixel);
		color_t ppir = isInRange(previousPixel);

		if((pir == GREEN && ppir == ORANGE) || (ppir == GREEN && pir == ORANGE))	// from green to orange or orange to green
		//if((pir != ppir)&& (pir==ORANGE||ppir==ORANGE))
		{
			points.push_back(Point(row,i));
		}
	}
	return points;
}

ScanLine::color_t ScanLine::isInRange(cv::Vec3b pixel){
	color_t ret = ORANGE;
	// Blue
	if(!(pixel.val[0] >= ballColor.values.layer1_min && pixel.val[0] <= ballColor.values.layer1_max))
		ret = OTHER;
	// Green
	if(!(pixel.val[1] >= ballColor.values.layer2_min && pixel.val[1] <= ballColor.values.layer2_max))
		ret = OTHER;
	// Red
	if(!(pixel.val[2] >= ballColor.values.layer3_min && pixel.val[2]<= ballColor.values.layer3_max))
		ret = OTHER;

	if(ret == OTHER)	// It is not the ball, is it the field?
	{
		ret = GREEN;
		if(!(pixel.val[0] >= fieldColor.values.layer1_min && pixel.val[0] <= fieldColor.values.layer1_max))
			ret = OTHER;
		// Green
		if(!(pixel.val[1] >= fieldColor.values.layer2_min && pixel.val[1] <= fieldColor.values.layer2_max))
			ret = OTHER;
		// Red
		if(!(pixel.val[2] >= fieldColor.values.layer3_min && pixel.val[2] <= fieldColor.values.layer3_max))
			ret = OTHER;
	}

	return ret;
}

void ScanLine::drawPoints(cv::Mat source,vector <cv::Point> pointList, Scalar colour)
{
	for(int i = 0; i<pointList.size(); i++)
	{
		circle(source, pointList[i], 1, colour, 2, 8, 0);
	}
}

Point ScanLine::getCenter(vector<vector<Point> > ptsLine,  vector<vector<Point> > ptsColumn)
{
	Point blp1;
	Point blp2;
	Point bcp1;
	Point bcp2;

	// Select best line points: with the largest distance
	int size = 0;
	for(int i = 0; i < ptsLine.size(); i+=2)
	{
		int prevSize = size;
		size = ptsLine[i][1].x - ptsLine[i][0].x;
		if(size > prevSize)
		{
			blp1 = ptsLine[i][0];
			blp2 = ptsLine[i][1];
		}
	}

	// Select best row points: with the largest distance
	size = 0;
	for(int i = 0; i < ptsColumn.size(); i+=2)
	{
		int prevSize = size;
		size = ptsColumn[i][1].y - ptsColumn[i][0].y;
		if(size > prevSize)
		{
			bcp1 = ptsColumn[i][0];
			bcp2 = ptsColumn[i][1];
		}
	}

	if(ptsLine.size()>0 && ptsColumn.size()>0)
	{
		Point pcent;
		int rad[4];
		pcent.x = (blp1.x + blp2.x)/2;
		pcent.y = (bcp1.y + bcp2.y)/2;

		// Adapt the distance between lines to have 8 points all the time
		rad[0] = DISTANCE2PTS(blp1.x,blp1.y,pcent.x,pcent.y);
		rad[1] = DISTANCE2PTS(blp2.x,blp2.y,pcent.x,pcent.y);
		rad[2] = DISTANCE2PTS(bcp1.x,bcp1.y,pcent.x,pcent.y);
		rad[3] = DISTANCE2PTS(bcp2.x,bcp2.y,pcent.x,pcent.y);

#ifdef ADAPTIVE_SCAN_LINE

		lineDistance = rad[0];
		for(int i = 1; i< 4; i++)
		{
			if(rad[i] < lineDistance)
				lineDistance = rad[i];	// Assign the smallest radius
		}
#endif

		return pcent;
	}
	else
	{
		// If no ball detected, set the distance between lines at the default value
		lineDistance = DIST;
		return Point(-1,-1);
	}
}

