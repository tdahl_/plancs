/**
 * @file processingtime.cpp
 * @date 18/06/2015
 * @author mthibaud
 */

#include "ball_detection/processing_time.h"


processingTime::processingTime()
{
	sum = 0;
	iteration = 0;
	average = 0;
	start = (clock_t)0;
}


void processingTime::startCount()
{
	// Get clock
	start = clock();
}

double processingTime::stopCount()
{
	// Get clock
	clock_t end = clock();

	// Compare with saved time
	double cpu_time = ((double) (end-start)) / CLOCKS_PER_SEC;

	// Add to the sum, increment the number of iterations and compute the average
	sum += cpu_time;
	iteration++;
	average = sum/(double)iteration;

	// Return cpu time
	return cpu_time;
}

double processingTime::getAverage(void)
{
	// Return average of cpu time from the beginning or reset
	return average;
}

void processingTime::reset(void)
{
	// Reset all values
	sum = 0;
	iteration = 0;
	average = 0;
}
