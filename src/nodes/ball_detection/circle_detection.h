#ifndef CIRCLE_DETECTION_H
#define CIRCLE_DETECTION_H
#include <stdlib.h>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


class CircleDetection
{
private:
#define DMINDIST 16.0
#define DP1 15.0
#define DP2 2.0
#define DMINRAD 0
#define DMAXRAD 0
#define CIRCLE_WINDOW_NAME "Hough Params"

private:
	int min_dist;
	int param1;
	int param2;
	int minRadius;
	int maxRadius;

public:
	CircleDetection(void);

public:
	std::vector<cv::Vec3f> getCircles(cv::Mat source);
	cv::Point intelligentCircleSelection(std::vector<cv::Vec3f> circles);
	cv::Mat drawCircles(cv::Mat source, std::vector<cv::Vec3f> circles);
	cv::Mat drawCircle(cv::Mat source, cv::Point centre, int radius);
};

#endif //CIRCLE_DETECTION_H
