/**
 * @file PositionGetter.cpp
 * @date 03/06/2015
 * @author mthibaud
 */


#include "PositionGetter.h"
#include <iostream>
#include <ros/ros.h>
#include <cmath>
#include <cstdlib>
#include <string>
#include <string.h>
#include <cstdio>

#define VFOV 60.97 // Degrees
#define HFOV 34.80
#define HALF_VFOV VFOV/2
#define HALF_HFOV HFOV/2
#define WIDTH 320
#define HEIGHT 240
#define HDEGPP HFOV/WIDTH	// Degree per pixel
#define VDEGPP VFOV/HEIGHT	// Degree per pixel

#define ROBOT_SIZE 54	// Size of the camera

#define PI 3.14152
#define DEG2RAD(a) (a*PI/180)
#define RAD2DEG(a) (a*180/PI)

using namespace ros;
using namespace std;
using namespace plancs;

PositionGetterNode::PositionGetterNode(int argc, char *argv[], std::string name, string resloc): PlancsNode(argc, argv, "ball_position")
{
	res_location = resloc;
}

PositionGetterNode::~PositionGetterNode()
{

}

void PositionGetterNode::init()
{
	set_verbose(false);
	set_decay_period(0.1);
	set_sleep_time(100000);
	set_excitation_offset(1);
	set_inhibition_offset(0);
	set_asynchrone_mode(false);

	ballPosSub = nh->subscribe("ball_position_on_screen",1,&PositionGetterNode::callback,this);
	ballPosPub = nh->advertise<plancs::BallRelativePosition>("ball_relative_position",1);
	camera_id_sub = nh->subscribe("nao_camera_1/camera_id",1,&PositionGetterNode::camera_id_callback,this);

	ROS_INFO("Camera Top:");
	extract_lut(res_location+"/distance_points_top.txt", &distance_lut_top);
	extract_matrix(res_location+"/rotation_matrix_top.txt", rotation_matrix_top);
	ROS_INFO("Camera Bot:");
	extract_lut(res_location+"/distance_points_bot.txt", &distance_lut_bot);
	extract_matrix(res_location+"/rotation_matrix_bot.txt", rotation_matrix_bot);

	usleep(100000);
	ask_camera_id();
	usleep(100000);
}

void PositionGetterNode::plancs_do()
{

}

void PositionGetterNode::extract_lut(string file_name, vector <point2d_t> *lut)
{
	point2d_t point;
	char buf[64];

	FILE *file = NULL;
	file = fopen(file_name.c_str(),"r");
	if(!file)
	{
		ROS_ERROR("Couldn't open the specified file: %s:%d",__FILE__, __LINE__);
	}

	while(fgets(buf, 64,file))
	{
		sscanf(buf,"%lf %lf", &point.y, &point.x);
		lut->push_back(point);
	}
	fclose(file);

	for(int i = 0; i < lut->size();i++)
		printf("%lf %lf \n", lut->at(i).x, lut->at(i).y);

}

void PositionGetterNode::extract_matrix(string file_name, double * matrix)
{
	FILE *file = NULL;
	file = fopen(file_name.c_str(),"r");
	if(!file)
	{
		ROS_ERROR("Couldn't open the specified file: %s:%d",__FILE__, __LINE__);
	}

	fscanf(file,"%lf %lf", &matrix[0], &matrix[1]);
	fclose(file);

	printf("%lf %lf \n", matrix[0], matrix[1]);
}


void PositionGetterNode::callback(const plancs::ObjectPositionOnCamera &msg)
{
	plancs::BallRelativePosition data;
	double angle = 0.0;
	double distance = -1;
	double vAngle;
	double ypos;

	if(!this->is_inhibited())
	{
		// Get the head angle
		ros::ServiceClient client = nh->serviceClient<plancs::SALMotion_getPosition>("nao_ALMotion_getPosition_1");
		plancs::SALMotion_getPosition srv;
		srv.request.name = "CameraTop";
		srv.request.space = 1;
		srv.request.useSensors = true;
		srv.request.post = false;
		if(!client.call(srv))
			ROS_WARN("ALMotion unreachable");

		ros::ServiceClient client2 = nh->serviceClient<plancs::SALMotion_getAngles>("nao_ALMotion_getAngles_1");
		plancs::SALMotion_getAngles srv2;
		srv2.request.names = "Head";
		srv.request.useSensors = true;
		srv.request.post = false;
		float currentPitchAngle;
		float currentYawAngle;

		if (client2.call(srv2)){

			string token;
			srv2.response.reply.erase(std::remove(srv2.response.reply.begin(), srv2.response.reply.end(),'['));
			srv2.response.reply.erase(std::remove(srv2.response.reply.begin(), srv2.response.reply.end(),']'));
			srv2.response.reply.erase(std::remove(srv2.response.reply.begin(), srv2.response.reply.end(),','));
			size_t space = srv2.response.reply.find(" ");
			token = srv2.response.reply.substr(0,space);
			currentYawAngle = boost::lexical_cast<float>(token);
			token = srv2.response.reply.substr(space+1, srv.response.reply.size());
			currentPitchAngle = boost::lexical_cast<float>(token);
		}
		else
		{
			ROS_ERROR("ALMOTION NODE NOT REACHABLE");
			return;
		}

		data.detected = msg.detected;

		// Get the position on the screen
		if(msg.detected)
		{
			angle = DEG2RAD((msg.xPos - 160) * HDEGPP);
			if(get_verbose())
				ROS_INFO("Angle = %lf - %lf = %lf",angle, currentYawAngle, angle - currentYawAngle);
			angle -= currentYawAngle;
			vAngle = srv.response.reply.at(4)*100;
			ypos = msg.yPos;

			if(camera_id == 0)
			{
				int start = (int)distance_lut_top.at(0).x;
				double rot = rotation_matrix_top[0]*vAngle + rotation_matrix_top[1]*ypos;
				int ptr = (int)(rot - start);
				if(ptr < 0)
					distance =  distance_lut_top[0].y;
				else if(ptr >= distance_lut_top.size())
					distance =  distance_lut_top.back().y;
				else
					distance =  distance_lut_top[ptr].y;
			}
			else
			{
				int start = (int)distance_lut_bot.at(0).x;
				double rot = rotation_matrix_bot[0]*vAngle + rotation_matrix_bot[1]*ypos;
				int ptr = (int)(rot - start);
				if(ptr < 0)
					distance =  distance_lut_bot[0].y;
				else if(ptr >= distance_lut_bot.size())
					distance =  distance_lut_bot.back().y;
				else
					distance =  distance_lut_bot[ptr].y;
			}
		}

		//ROS_INFO("Distance: %d", (int)distance);
		data.angle = angle;
		data.distance = distance;
		ballPosPub.publish(data);
	}
}

void PositionGetterNode::camera_id_callback(const std_msgs::Int32 &id)
{
	camera_id = (int)id.data;
	cout << "camera changed" << endl;
}

void PositionGetterNode::ask_camera_id()
{
	ros::ServiceClient camera_id_client;
	std_srvs::Empty msg;
	camera_id_client = nh->serviceClient<std_srvs::Empty>("nao_camera_1/request_camera_id");
	camera_id_client.call(msg);
}
