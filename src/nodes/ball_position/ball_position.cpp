/**
 * @file ball_position.cpp
 * @date 25/06/2015
 * @author mthibaud
 */

#include <ros/ros.h>
#include <string>

#include "PositionGetter.h"
#include "common/ArgumentExtractor.h"

using namespace std;

/**
 * @brief The program entry
 */
int main(int argc, char * argv[])
{
	// Get the name of the node
	std::string name = "ball_position";
	std::string resloc = "src/plancs/res";
	for(int i = 1; i < argc; i++)
	{
		arg_t arg = ArgumentExtractor::getArgument(argv[i]);
		if(!arg.name.compare("rrobot"))
		{
			name = name + "_" + arg.value; break;
		}
	}

	// Get the res folder location
	for(int i = 1; i < argc; i++)
	{
		arg_t arg = ArgumentExtractor::getArgument(argv[i]);
		if(!arg.name.compare("resloc"))
		{
			resloc = arg.value; break;
		}
	}

	// Create the ball_position node
	PositionGetterNode node(argc, argv, name, resloc);
	// Start it
	node.start();
	return 0;
}
