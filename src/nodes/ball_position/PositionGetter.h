/**
 * @file PositionGetter.h
 * @date 03/06/2015
 * @author mthibaud
 */

#ifndef PLANCS_SRC_BALL_TRACKING_BALL_POSITION_POSITIONGETTER_H_
#define PLANCS_SRC_BALL_TRACKING_BALL_POSITION_POSITIONGETTER_H_

#include <ros/ros.h>
#include <string>
#include <vector>
#include "PlancsNode.h"
#include "plancs/BallRelativePosition.h"
#include "plancs/ObjectPositionOnCamera.h"
#include "plancs/SALMotion_getAngles.h"
#include "plancs/SALMotion_getPosition.h"
#include "std_msgs/Int32.h"
#include "std_srvs/Empty.h"

typedef struct
{
	double x;
	double y;
}point2d_t;

using namespace std;
using namespace ros;
using namespace plancs;

class PositionGetterNode: public plancs::PlancsNode {
protected:
	string res_location;
	ros::Subscriber ballPosSub;				/** subscriber to the ball position on the screen */
	ros::Publisher ballPosPub;				/** publisher of the ball position relatively to the robot */
	ros::ServiceClient motionClient;		/** client to send movement orders to the robot */
	ros::Subscriber camera_id_sub;			/** subscriber to the camera id: notify when the camera change or a notification order happens */
	vector <point2d_t> distance_lut_top;	/** vector containing the list of points to quickly get the distance from the angle and the screen pos for the top camera */
	double rotation_matrix_top[2];			/** Rotation matrix for the top camera */
	vector <point2d_t> distance_lut_bot;	/** vector containing the list of points to quickly get the distance from the angle and the screen pos for the bottom camera */
	double rotation_matrix_bot[2];			/** Rotation matrix for the bottom camera */
	int camera_id = 0;


public:
	/**
	 * @brief The PositionGetterNode constructor
	 */
	PositionGetterNode(int argc, char *argv[], std::string name, string resloc);

	/**
	 * @brief The PositionGetterNode Destructor
	 */
	~PositionGetterNode();

private:
	/**
	 * @brief The init method
	 * Overrides the plancs node method
	 */
	void init();

	/**
	 * @brief The plancs_do method
	 * Overrides the plancs node method
	 */
	void plancs_do();

	/**
	 * @brief The extract_lut method gets the list of points from the specified file
	 * @param file_name The name of the file to read
	 * @param lut The vector to store the look up table
	 */
	void extract_lut(string file_name, vector <point2d_t> *lut);

	/**
	 * @brief The extract_matrix method gets the rotation matrix from the specified file
	 * @param file_name The name of the file to read
	 * @param matrix The the matrix of rotation
	 */
	void extract_matrix(string file_name, double *matrix);

private:
	/**
	 * @brief The callback method receives the position of the ball and copies it
	 * @param msg The ball position form the topic
	 */
	void callback(const plancs::ObjectPositionOnCamera &msg);

	/**
	 * @brief The camera_id_callback method gets the change of camera from the topic
	 * @param id The id of the camera
	 */
	void camera_id_callback(const std_msgs::Int32 &id);

	/**
	 * @brief The ask_camera_id method asks for a notification of the camera id
	 */
	void ask_camera_id();
};

#endif /* PLANCS_SRC_BALL_TRACKING_BALL_POSITION_POSITIONGETTER_H_ */
