/**
 * @file ball_reaching_trigger.cpp
 * @brief The ball_reaching_trigger.cpp file
 * @author mthibaud
 * @date 5 Aug 2015
 */

#include <ros/ros.h>
#include <vector>
#include <boost/thread.hpp>
#include "PlancsNode.h"
#include "plancs/BallRelativePosition.h"

namespace plancs
{
	using namespace ros;
	using namespace std;

class BallReachingTrigger: public PlancsNode
{
private:
	Subscriber ball_pos_sub;		 /** subscriber to ball position */
	Publisher ball_pos_pub;
	vector <BallRelativePosition> position_stack;
	boost::mutex mtx;
	enum{
		IDLE = 0,
		MOVING
	}state = IDLE;

public:
	BallReachingTrigger (int argc, char *argv[]):
		PlancsNode(argc, argv, "ball_reaching_trigger")
	{

	}

protected:
	void init()
	{
		set_sleep_time(100000);
		set_decay_period(1.0);
		set_excitation_offset(1);
		set_inhibition_offset(0);
		set_asynchrone_mode(false);
		set_verbose(true);

		ball_pos_sub = nh->subscribe("ball_relative_position",1,&BallReachingTrigger::ball_position_callback, this);
		ball_pos_pub = nh->advertise<BallRelativePosition>("ball_relative_position_trigger", 1);
	}

	void plancs_do()
	{
		int dist_sum = 0;
		double angle_sum = 0;
		int i;

		mtx.lock();
		if(position_stack.size() >= 10)
		{
			for(i = 0; i<position_stack.size(); i++)
			{
				dist_sum += position_stack[i].distance;
				angle_sum += position_stack[i].angle;
			}
			dist_sum /= i;
			angle_sum /= i;

			ROS_INFO("FLUSH: %d, %lf", dist_sum, angle_sum);

			BallRelativePosition to_send;

			to_send.detected = true;
			to_send.distance = dist_sum;
			to_send.angle = angle_sum;

			ball_pos_pub.publish(to_send);
			position_stack.clear();

			activate_node();
		}
		mtx.unlock();
	}


	void ball_position_callback(const plancs::BallRelativePosition& msg)
	{
		mtx.lock();
		if(msg.detected)
		{
			position_stack.push_back(msg);
			if(position_stack.size() > 11)
				position_stack.erase(position_stack.begin());
		}
		mtx.unlock();
	}
};

} /* namespace plancs */



int main(int argc, char *argv[])
{
	plancs::BallReachingTrigger trigger(argc, argv);
	trigger.start();
	return 0;
}
