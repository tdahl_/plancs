/**
 * @file ball_reaching.h
 *
 * @date 30/06/2015
 * @author mthibaud
 */

#ifndef PLANCS_SRC_BALL_REACHING_BALL_REACHING_H_
#define PLANCS_SRC_BALL_REACHING_BALL_REACHING_H_

#include <ros/ros.h>
#include "PlancsNode.h"
#include "plancs/BallRelativePosition.h"

using namespace plancs;
using namespace ros;

class BallReachingNode :public PlancsNode{
private:
	Subscriber ball_pos_sub;		 /** subscriber to ball position */
	bool new_data = false;
	int dist = 0;
	double angle = 0.0;

public:
	/**
	 * @brief The BallReachingNode constructor
	 */
	BallReachingNode(int argc, char *argv[]);

private:
	/**
	 * @brief The init method initialise the node
	 * override plancs init
	 */
	void init();

	/**
	 * @brief The plancs_do method runs the node
	 * Override plancs_do from plancs
	 */
	void plancs_do();

	/**
	 * @brief The ball_pos_callback method receives the position of the ball
	 * @param pos The position of the ball
	 */
	void ball_pos_callback(const BallRelativePosition& pos);

	/**
	 * @brief The get_movement_status returns if the task with task_id is running or not
	 * @param task_id The ID of the task to check (cf ALProxy)
	 * @return The status (running or not)
	 */
	bool get_movement_status(unsigned int task_id);
};

#endif /* PLANCS_SRC_BALL_REACHING_BALL_REACHING_H_ */
