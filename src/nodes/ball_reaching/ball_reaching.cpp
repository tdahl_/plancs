/**
 * @file ball_reaching.cpp
 *
 * @date 30/06/2015
 * @author mthibaud
 */

#include "ball_reaching/ball_reaching.h"
#include "plancs/SALMotion_moveTo.h"
#include "plancs/SALMotion_getTaskStatusFromID.h"

using namespace plancs;
using namespace ros;
using namespace std;

#define PI 3.14152
#define DEG2RAD(a) (a*PI/180)
#define RAD2DEG(a) (a*180/PI)

BallReachingNode::BallReachingNode(int argc, char *argv[]) : PlancsNode(argc, argv, "ball_reaching")
{
}

void BallReachingNode::init()
{
	set_decay_period(0.1);
	set_excitation_offset(1);
	set_inhibition_offset(0);
	set_verbose(false);
	set_sleep_time(100000);

	add_inhibiter("ball_reaching_trigger");
	add_inhibiter("ball_searching");

	ball_pos_sub = nh->subscribe("ball_relative_position_trigger",1,&BallReachingNode::ball_pos_callback, this);
}

void BallReachingNode::plancs_do()
{
	int task_id;
	if(new_data)
	{
		ros::ServiceClient client = nh->serviceClient<plancs::SALMotion_moveTo>("nao_ALMotion_moveTo_1");
		SALMotion_moveTo srv;
		float a = angle;
		if(dist > 100)
			dist = 100;
		float d = (float)(dist-10.0)/50.0;
		new_data = false;
		srv.request.x = d*cos(a);
		srv.request.y = -d*sin(a);
		srv.request.theta = -a;
		srv.request.post = true;
		ROS_INFO("Goto pose: %f,%f,%f",srv.request.x, srv.request.y, srv.request.theta);
		task_id = client.call(srv);
		activate_node();
	}else if(get_movement_status(task_id))
	{
		activate_node();
	}
}

void BallReachingNode::ball_pos_callback(const BallRelativePosition& pos)
{
	if(!new_data)
	{
		angle = pos.angle;
		dist = pos.distance;
		new_data = true;
	}
}

bool BallReachingNode::get_movement_status(unsigned int task_id)
{
	ServiceClient statusClient = nh->serviceClient<plancs::SALMotion_getTaskStatusFromID>("ALMotion_getTaskStatus");
	plancs::SALMotion_getTaskStatusFromID status;
	status.request.task_id = task_id;
	statusClient.call(status);
	return status.response.status;
}


int main(int argc, char *argv[])
{
	BallReachingNode node(argc, argv);
	node.start();
	return 0;
}

