/**
 * @file ball_searching.cpp
 * @date 05/06/2015
 * @author mthibaud
 */

#include <ros/ros.h>
#include <stdio.h>
#include <string>
#include "PlancsNode.h"
#include "plancs/SALMotion_setAngles.h"
#include "plancs/SALMotion_getAngles.h"
#include "plancs/SALMotion_setStiffnesses.h"
#include "plancs/NaoCameraID.h"
#include "std_msgs/Int32.h"
#include "plancs/SALMotion_getTaskStatusFromID.h"


using namespace ros;
using namespace std;
using namespace plancs;

typedef struct{
	float yaw;		// Horizontal
	float pitch; 	// Vertical
	int cam;		// Camera Number
}joint_pos_t;


#define LIST_SIZE 2

joint_pos_t point_list[LIST_SIZE] = {
		{ 1.0, 0.0, 0},
		{ -1.0, 0.0, 1}
};

class BallSearchingNode: public PlancsNode
{
private:
	ros::Subscriber camera_id_sub;
	plancs::SALMotion_setAngles motionSrv;		/** The service message for almotion almotion */
	plancs::SALMotion_setStiffnesses stiffSrv;	/** The service message for the stiffness */
	ros::ServiceClient stiffnessClient;			/** The client for the stifness */
	ros::ServiceClient motionClient;			/** The service cilent to almotion */
	double waitTime = 0.1;						/** The delay time between two iterations */
	char buf[64];								/** A buffer */
	int i = 0;									/** A variable */
	int camera_id = 0;
	ros::Time start_move_time;

public:
	/**
	 * @brief The BallSearchingNode constructor
	 */
	BallSearchingNode(int argc, char* argv[]) : PlancsNode(argc, argv,"ball_searching"){}

private:
	/**
	 * @brief The init method initialises the node
	 * Overrides the plancs init
	 */
	void init()
	{
		ROS_INFO("Init ball_searching");
		set_sleep_time(300000);		// 300ms
		set_verbose(false);			// Deactivate the verbose mode
		set_decay_period(1.0);		// 1s
		set_excitation_offset(1);	// Always running unless getting an inhibition
		set_inhibition_offset(0);	// No offset for inhibition

		// Stiffness client service
		stiffnessClient = nh->serviceClient<plancs::SALMotion_setStiffnesses>("nao_ALMotion_setStiffnesses_1");

		// Init head movement client
		motionClient = nh->serviceClient<plancs::SALMotion_setAngles>("nao_ALMotion_setAngles_1");

		// Enable stiffness (required for any movement)
		stiffSrv.request.names = "Head";
		stiffSrv.request.stiffnesses = "1";
		stiffSrv.request.post = true;
		stiffnessClient.call(stiffSrv);
		motionSrv.request.fractionMaxSpeed = 0.2;
		motionSrv.request.post = true;

		camera_id_sub = nh->subscribe("nao_camera_1/camera_id",1,&BallSearchingNode::camera_id_callback,this);
		start_move_time = ros::Time::now();

	}

	/**
	 * @brief The plancs_do method runs the node
	 * Overrides the plancs plancs_do
	 */
	void plancs_do()
	{
		ros::Time current_time = ros::Time::now();
		activate_node();

		if(current_time >= start_move_time + ros::Duration(waitTime))
		{
			start_move_time = ros::Time::now();
			set_camera_id(point_list[i].cam);
			set_angle(i);
			waitTime = 4.0;
			i = (i+1)%LIST_SIZE;
		}
	}

	void camera_id_callback(const std_msgs::Int32 &id)
	{
		ROS_INFO("camera = %d", (int)id.data);
		camera_id = (int)id.data;
	}

	void set_camera_id(int id)
	{
		ServiceClient changeCamClient = nh->serviceClient<plancs::NaoCameraID>("nao_camera_1/camera_id");
		plancs::NaoCameraID cam_request;
		cam_request.request.id = id;
		changeCamClient.call(cam_request);
	}

	void set_angle(int count)
	{
		motionSrv.request.names = "Head";
		sprintf(buf, "[%f, 0.3]", point_list[count].yaw);
		ROS_INFO("%s", buf);
		motionSrv.request.angles = string(buf);
		motionClient.call(motionSrv);
	}

	bool get_movement_status(unsigned int task_id)
	{
		ServiceClient statusClient = nh->serviceClient<plancs::SALMotion_getTaskStatusFromID>("ALMotion_getTaskStatus");
		plancs::SALMotion_getTaskStatusFromID status;
		status.request.task_id = task_id;
		statusClient.call(status);
		return status.response.status;
	}
};

/**
 * @brief Program entry
 */
int main(int argc, char *argv[])
{
	BallSearchingNode node(argc, argv);
	node.start();
	return 0;
}
