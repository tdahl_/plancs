/**
 * @file head_tracking.cpp
 * @brief Contains the node which tracks the ball with the head
 * @author mthibaud
 * @date 12/06/2015
 * From 3/06/2015 headMovement version (deprecated)
 */

#include <ros/ros.h>
#include <cstdlib>
#include <string>
#include <string.h>
#include <cstdio>
#include <boost/thread/mutex.hpp>
#include "PlancsNode.h"
#include "plancs/SALMotion_setAngles.h"
#include "plancs/SALMotion_getAngles.h"
#include "plancs/SALMotion_setStiffnesses.h"
#include "plancs/ObjectPositionOnCamera.h"

using namespace std;
using namespace ros;
using namespace plancs;

#define VFOV 36.80 // Degrees
#define HFOV 47.80
#define HALF_VFOV VFOV/2
#define HALF_HFOV HFOV/2
#define WIDTH 320
#define HEIGHT 240
#define HDEGPP HFOV/WIDTH	// Degree per pixel
#define VDEGPP VFOV/HEIGHT	// Degree per pixel
#define PI 3.14152
#define DEG2RAD(a) (a*PI/180)
#define RAD2DEG(a) (a*180/PI)


class HeadTrackingNode : public PlancsNode
{

private:
	ros::ServiceClient motionClient;
	plancs::SALMotion_setAngles motionSrv;
	ros::Publisher inhibit_publisher;
	ros::Subscriber ball_sub;

	boost::mutex data_copy_mutex;	/** Data protection */
	bool detected;					/** is detected? */
	bool pitch_move;				/** has pitch moved */
	bool yaw_move;					/** has yaw moved */
	int screen_pos_x;				/** position on the screen in x */
	int screen_pos_y;				/** position on the screen in y */
	float pitch_rec;				/** received pitch */
	float yaw_rec;					/** received yaw */
	float pitch_cmd;				/** pitch command */
	float yaw_cmd;					/** yaw command */

public:
	/**
	 * @brief The HeadTrackingNode constructor
	 */
	HeadTrackingNode(int argc, char *argv[], string name): PlancsNode(argc, argv, name)
	{
		detected = false;
		pitch_move = false;
		yaw_move = false;
		screen_pos_x = 0;
		screen_pos_y = 0;
		pitch_rec = 0.0;
		yaw_rec = 0.0;
		pitch_cmd = 0.0;
		yaw_cmd = 0.0;
	}

private:
	/**
	 * @brief The init method initialises the node
	 * Overrides the plancs init
	 */
	void init()
	{
		set_decay_period(200000); // 200ms
		set_excitation_offset(1);
		set_sleep_time(200000); // 200ms
		set_verbose(true);

		add_inhibiter("ball_searching");

		// Stiffness client service
		ros::ServiceClient stiffnessClient = nh->serviceClient<plancs::SALMotion_setStiffnesses>("nao_ALMotion_setStiffnesses_1");
		plancs::SALMotion_setStiffnesses stiffSrv;

		// Init head movement client
		motionClient = nh->serviceClient<plancs::SALMotion_setAngles>("nao_ALMotion_setAngles_1");


		// Enable stiffness (required for any movement)
		stiffSrv.request.names = "Head";
		stiffSrv.request.stiffnesses = "1";
		stiffSrv.request.post = true;
		stiffnessClient.call(stiffSrv);
		ROS_INFO("Stiffnesses activated");

		// Init head position
		motionSrv.request.names = "HeadYaw";
		motionSrv.request.angles = "0.0";
		motionSrv.request.fractionMaxSpeed = 0.5;
		motionSrv.request.post = true;
		motionClient.call(motionSrv);
		ROS_INFO("Yaw set to 0");

		motionSrv.request.names = "HeadPitch";
		motionSrv.request.angles = "0.2";
		motionSrv.request.fractionMaxSpeed = 0.5;
		motionSrv.request.post = true;
		motionClient.call(motionSrv);
		ROS_INFO("Pitch set to 0");

		ball_sub = nh->subscribe("ball_position_on_screen",1,&HeadTrackingNode::ball_pos_callback, this);
	}

	/**
	 * @brief The plancs_do method runs the node
	 * Overrides the plancs plancs_do
	 */
	void plancs_do()
	{

		char buf[64];
		int x_pos, y_pos;
		float joint_pitch, joint_yaw;
		bool is_detected;

		// Protected copy of data from callback
		data_copy_mutex.lock();
		x_pos = screen_pos_x;
		y_pos = screen_pos_y;
		joint_pitch = pitch_rec;
		joint_yaw = yaw_rec;
		is_detected = detected;
		detected = false;	// Ackn read
		data_copy_mutex.unlock();


		// Set the new positions
		motionSrv.request.fractionMaxSpeed = 0.1;
		if(is_detected)
		{
			activate_node();

			//Check yaw
			if(x_pos > -30 && x_pos < 30)
			{
				yaw_move = false;
			}
			else
			{
				yaw_move = true;
				motionSrv.request.names = "HeadYaw";
				float newAngle = joint_yaw-DEG2RAD((float)(x_pos)*HDEGPP)/2;
				yaw_cmd = newAngle;
				sprintf(buf,"%f", newAngle);

				motionSrv.request.angles = string(buf);
				motionSrv.request.post = true;
				motionClient.call(motionSrv);
			}


			// Check Pitch
			if(y_pos > -30 && y_pos < 30)
			{
				pitch_move = false;
			}
			else
			{
				pitch_move = true;
				motionSrv.request.names = "HeadPitch";
				float newAngle = joint_pitch+DEG2RAD((float)(y_pos)*VDEGPP)/2;
				if(newAngle <= 0.1)	// Avoid looking up
					newAngle = 0.1;
				pitch_cmd = newAngle;
				sprintf(buf,"%f", newAngle);
				motionSrv.request.angles = string(buf);
				motionSrv.request.post = true;
				motionClient.call(motionSrv);
			}
		}
		//display_values(is_detected);
	}

	/**
	 * @brief The ball_pos_callback method gets the position of the ball
	 * @param msg the position of the ball
	 */
	void ball_pos_callback(const plancs::ObjectPositionOnCamera &msg)
	{
		// Protect the data to copy
		data_copy_mutex.lock();
		if(msg.detected)
		{
			screen_pos_x = msg.xPos - WIDTH/2;
			screen_pos_y = msg.yPos - HEIGHT/2;


			// Call service to get the angles of the head
			ros::ServiceClient client = nh->serviceClient<plancs::SALMotion_getAngles>("nao_ALMotion_getAngles_1");
			plancs::SALMotion_getAngles srv;
			srv.request.names = "Head";
			srv.request.useSensors = true;
			srv.request.post = false;

			if (client.call(srv)){
				float currentPitchAngle;
				float currentYawAngle;
				string token;
				srv.response.reply.erase(std::remove(srv.response.reply.begin(), srv.response.reply.end(),'['));
				srv.response.reply.erase(std::remove(srv.response.reply.begin(), srv.response.reply.end(),']'));
				srv.response.reply.erase(std::remove(srv.response.reply.begin(), srv.response.reply.end(),','));
				size_t space = srv.response.reply.find(" ");
				token = srv.response.reply.substr(0,space);
				currentYawAngle = boost::lexical_cast<float>(token);
				token = srv.response.reply.substr(space+1, srv.response.reply.size());
				currentPitchAngle = boost::lexical_cast<float>(token);
				pitch_rec = currentPitchAngle;
				yaw_rec = currentYawAngle;
			}
			else
			{
				ROS_ERROR("ALMOTION NODE NOT REACHABLE");
				return;
			}

			detected = true;
		}
		data_copy_mutex.unlock();
	}

	/**
	 * @brief The display_values method write on the termin al the position of the ball and the joints valuesand the command
	 * @param display Write on the screen or not
	 */
	void display_values(bool display)
	{
		if(get_verbose())
		{
			// No protection: May have incorrect displays
			std::system("clear");
			ROS_INFO("Report\n");
			printf("          | Pitch       | Yaw\n");
			if(display)
			{
				printf("Position  | %3d         | %3d\n", screen_pos_y, screen_pos_x);
				printf("Joints    | %5f    | %5f\n", pitch_rec, yaw_rec);
				printf("Command   | %5f    | %5f\n", (pitch_move)?pitch_cmd:0.0, (yaw_move)?yaw_cmd:0.0);
			}
			else
			{
				printf("Position  | N/A         | N/A\n");
				printf("Joints    | N/A         | N/A\n");
				printf("Command   | N/A         | N/A\n");
			}
		}
	}
};

/**
 * @brief Program entry
 */
int main (int argc, char * argv[])
{
	HeadTrackingNode node(argc, argv, "head_tracking");
	node.start();
	return 0;
}
