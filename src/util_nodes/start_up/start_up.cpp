/**
 * @file startup.cpp
 * @date 29/06/2015
 * @author mthibaud
 */

#include "PlancsNode.h"
#include "plancs/SALMotion_setStiffnesses.h"
#include "plancs/SALRobotPosture_goToPosture.h"
#include "plancs/SALMotion_setAngles.h"

using namespace plancs;

class StartUpNode: public PlancsNode
{
private:
	plancs::SALMotion_setStiffnesses stiffSrv;
	plancs::SALRobotPosture_goToPosture postureSrv;
	plancs::SALMotion_setAngles motionSrv;
	ros::ServiceClient stiffnessClient;
	ros::ServiceClient robotPostureClient;
	ros::ServiceClient motionClient;

public:
	StartUpNode(int argc, char* argv[]): PlancsNode(argc, argv, "start_up"){}

private:
	void init()
	{
		set_decay_period(0.0);
		set_sleep_time(0);
		set_verbose(false);
		set_excitation_offset(1);
		set_asynchrone_mode(false);

		// Stiffness client service
		stiffnessClient = nh->serviceClient<plancs::SALMotion_setStiffnesses>("nao_ALMotion_setStiffnesses_1");
		// Enable stiffness (required for any movement)
		stiffSrv.request.names = "Body";
		stiffSrv.request.stiffnesses = "0.8";
		stiffSrv.request.post = true;
		stiffnessClient.call(stiffSrv);

		usleep(100000);

		robotPostureClient = nh->serviceClient<plancs::SALRobotPosture_goToPosture>("nao_ALRobotPosture_goToPosture");
		postureSrv.request.postureName = "Stand";
		postureSrv.request.speed = 1.0;
		postureSrv.request.post = false;
		robotPostureClient.call(postureSrv);

		usleep(100000);

		motionClient = nh->serviceClient<plancs::SALMotion_setAngles>("nao_ALMotion_setAngles_1");
		motionSrv.request.names = "HeadPitch";
		motionSrv.request.angles = "0.3";
		motionSrv.request.fractionMaxSpeed = 0.8;
		motionSrv.request.post = false;
		motionClient.call(motionSrv);

		ros::shutdown();
	}

	void plancs_do(){}
};

int main (int argc, char *argv[])
{
	StartUpNode node(argc, argv);
	node.start();
	return 0;
}




