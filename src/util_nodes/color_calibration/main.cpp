/**
 * @file main.cpp
 * @date 14/05/2015
 * @author mthibaud
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "common/nao_camera.h"
#include "common/thresholding.h"
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"


using namespace std;
using namespace ros;
using namespace cv;
using namespace NaoCam;
using namespace rapidxml;

NaoCamera *cam;

void callback(Mat source);
void initThresholds(string filename);
ThresholdingBGR getColor(xml_node<>* cur_node, std::string colorName);
void initThresholds(std::string filename);
void save(string filename);
void horizon(Mat source);

ThresholdingBGR *tho;
ThresholdingBGR *thg;
xml_document<> doc;
vector<char> xml_copy;

void callback(Mat source)
{
	static bool first = true;
	if(first)
	{
		initThresholds("src/plancs/res/colorThreshold.xml");
		tho->open_trackbar_window("OrangeTrackbars");
		thg->open_trackbar_window("GreenTrackbars");
		moveWindow("OrangeTrackbars",500, 400);
		moveWindow("GreenTrackbars",1000, 400);
		first = false;
	}
	Mat img = source.clone();
	Mat itho = tho->threshold_image(img);
	Mat ithg = thg->threshold_image(img);



	// Difference between the two colors
	Mat diff = itho.clone();

	for(int x = 0; x < diff.cols; x++)
		for(int y = 0; y < diff.rows; y++)
		{
			if(itho.at<uchar>(Point(x,y))== 255 && ithg.at<uchar>(Point(x,y)) == 255)
				diff.at<uchar>(Point(x,y)) = 255;
			else
				diff.at<uchar>(Point(x,y)) = 0;
		}

	//horizon(img);

	imshow("diff", diff);
	imshow("img", img);
	imshow("orange", itho);
	imshow("green", ithg);

	/****************************************
	 * Common
	 * **************************************/
	char k = waitKey(100);

	if(k == 27)
		exit(EXIT_SUCCESS);

	if(k == 83 || k == 115)
	{
		cout<< "Save" << endl;
		save("src/plancs/res/colorThreshold.xml");
	}
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "colorCalibration");
	ros::NodeHandle nh;
	namedWindow("img",WINDOW_AUTOSIZE);
	namedWindow("green",WINDOW_AUTOSIZE);
	namedWindow("orange",WINDOW_AUTOSIZE);
	namedWindow("diff",WINDOW_AUTOSIZE);

	moveWindow("img",0,0);
	moveWindow("green",1000,0);
	moveWindow("orange",500,0);
	moveWindow("diff",0,400);

	cam = new NaoCamera(nh, &callback);

	ros::spin();
}

void initThresholds(std::string filename)
{
	string input_xml;
	string line;
	ifstream in(filename.c_str());

	while(getline(in,line))
		input_xml += line;
	xml_copy = vector<char>(input_xml.begin(), input_xml.end());
	xml_copy.push_back('\0');


	doc.parse<parse_declaration_node | parse_no_data_nodes>(&xml_copy[0]);
	xml_node<>* cur_node = doc.first_node("colorThreshold");	// Select the thresholds


	ThresholdingBGR ballColor = getColor(cur_node, "orange");
	ThresholdingBGR fieldColor = getColor(cur_node, "green");

	tho = new ThresholdingBGR(ballColor.values.layer1_min, ballColor.values.layer1_max,
							  ballColor.values.layer2_min, ballColor.values.layer2_max,
							  ballColor.values.layer3_min, ballColor.values.layer3_max);
	thg = new ThresholdingBGR(fieldColor.values.layer1_min, fieldColor.values.layer1_max,
							  fieldColor.values.layer2_min, fieldColor.values.layer2_max,
							  fieldColor.values.layer3_min, fieldColor.values.layer3_max);

}

ThresholdingBGR getColor(xml_node<>* cur_node, std::string colorName)
{
	ThresholdingBGR threshold;
	cur_node = cur_node->first_node(colorName.c_str());

	// Get the blue component
	cur_node = cur_node->first_node("blueMin");
	threshold.values.layer1_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("blueMax");
	threshold.values.layer1_max = atoi(cur_node->value());

	// Get the green component
	cur_node = cur_node->next_sibling("greenMin");
	threshold.values.layer2_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("greenMax");
	threshold.values.layer2_max = atoi(cur_node->value());

	// Get the red component
	cur_node = cur_node->next_sibling("redMin");
	threshold.values.layer3_min = atoi(cur_node->value());
	cur_node = cur_node->next_sibling("redMax");
	threshold.values.layer3_max  = atoi(cur_node->value());

	// Restore the initial state
	cur_node = cur_node->parent()->parent();

	return threshold;
}

void save(string filename)
{
	vector<string> values;
	xml_node<>* cur_node = doc.first_node("colorThreshold");	// Select the thresholds

	// Save orange
	cur_node = cur_node->first_node("orange");
	cur_node = cur_node->first_node("blueMin");
	values.push_back(to_string(tho->values.layer1_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("blueMax");
	values.push_back(to_string(tho->values.layer1_max));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("greenMin");
	values.push_back(to_string(tho->values.layer2_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("greenMax");
	values.push_back(to_string(tho->values.layer2_max));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("redMin");
	values.push_back(to_string(tho->values.layer3_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("redMax");
	values.push_back(to_string(tho->values.layer3_max));
	cur_node->value(values.back().c_str());

	cur_node = cur_node->parent()->parent();

	// Save green
	cur_node = cur_node->first_node("green");
	cur_node = cur_node->first_node("blueMin");
	values.push_back(to_string(thg->values.layer1_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("blueMax");
	values.push_back(to_string(thg->values.layer1_max));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("greenMin");
	values.push_back(to_string(thg->values.layer2_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("greenMax");
	values.push_back(to_string(thg->values.layer2_max));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("redMin");
	values.push_back(to_string(thg->values.layer3_min));
	cur_node->value(values.back().c_str());
	cur_node = cur_node->next_sibling("redMax");
	values.push_back(to_string(thg->values.layer3_max));
	cur_node->value(values.back().c_str());

	std::string xml_as_string;
	rapidxml::print(std::back_inserter(xml_as_string), doc);

	std::ofstream file;
	file.open(filename.c_str());
	file << doc;
	print(cout, doc,0);
	file.close();
}


void horizon(Mat source)
{
	for(int col = 0; col < 320; col++)
	{
		int startPt = 0;
		int sum = 0;

		// Init add 10 first points
		for(int i = 0; i< 10; i++)
		{
			sum += source.at<Vec3b>(Point(col,i)).val[1];
		}

		while(startPt < 229 &&
				((sum < 10*160) ||
				source.at<Vec3b>(Point(col,startPt)).val[0] > 200 ||
				source.at<Vec3b>(Point(col,startPt)).val[2] > 200
				)
			)	// Avoid saturation
		{
			sum -= source.at<Vec3b>(Point(col,startPt)).val[1];
			source.at<Vec3b>(Point(col,startPt)) = Vec3b(0,0,0);
			startPt++;
			sum += source.at<Vec3b>(Point(col,startPt+9)).val[1];
		}
	}
}
