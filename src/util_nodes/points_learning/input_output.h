/**
 * @file input_output.h
 * @brief The input_output file
 * @author mthibaud
 * @date 17 Jul 2015
 */

#ifndef PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_INPUT_OUTPUT_H_
#define PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_INPUT_OUTPUT_H_

#include <vector>
#include <string>
#include "points_learning.h"

using namespace std;

vector <data_t>read3d(string file);
void fprint2d(string file, vector <point2_t> pts);
void fprint3d(string file, vector <data_t> pts);

#endif /* PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_INPUT_OUTPUT_H_ */
