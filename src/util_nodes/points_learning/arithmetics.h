/**
 * @file arithmetics.h
 * @brief The arithmetics file
 * @author mthibaud
 * @date 17 Jul 2015
 */

#ifndef PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_ARITHMETICS_H_
#define PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_ARITHMETICS_H_

#include <vector>
#include "points_learning.h"

using namespace std;

vector <data_t> rotate(double mat[2], vector <data_t> pts);
vector <point2_t> average(vector <data_t> pts);
vector <point2_t> smooth(vector <point2_t> pts);

#endif /* PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_ARITHMETICS_H_ */
