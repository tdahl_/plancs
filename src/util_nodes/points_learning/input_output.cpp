/**
 * @file input_output.cpp
 * @brief The input_output file
 * @author mthibaud
 * @date 17 Jul 2015
 */


#include <points_learning/input_output.h>
#include <stdio.h>

using namespace std;

vector <data_t>read3d(string file)
{
	vector <data_t> datas;
	char reply[64];
	FILE *fc = NULL;
	fc = fopen (file.c_str(), "r");

	if(fc)
	{
		data_t value;
		while(fgets(reply, 64, fc))
		{
			sscanf(reply, "%lf %lf %lf", &value.distance, &value.angle, &value.ypos);
			datas.push_back(value);
		}
		fclose(fc);
	}
	return datas;
}

void fprint2d(string file, vector <point2_t> pts)
{
	FILE *fc = NULL;
	fc = fopen (file.c_str(), "w");

	for(int i = 0; i < pts.size(); i++)
		fprintf(fc, "%lf %lf\n", pts[i].y ,pts[i].x);
	fclose(fc);
}

void fprint3d(string file, vector <data_t> pts)
{
	FILE *fc = NULL;
	fc = fopen (file.c_str(), "w");

	for(int i = 0; i < pts.size(); i++)
		fprintf(fc, "%lf %lf %lf\n", pts[i].distance, pts[i].angle ,pts[i].ypos);
	fclose(fc);
}

