/**
 * @file gui.cpp
 * @brief The gui file
 * @author mthibaud
 * @date 17 Jul 2015
 */

#include "gui.h"
#include <iostream>
#include <ros/ros.h>

extern int iteration;
boost::thread worker;
bool work;

using namespace std;

void display_interface(void);
void thread_main(void);

void thread_start(void)
{
	work = true;
	worker = boost::thread(&thread_main);
}

void thread_main(void)
{
	get_data()->distance = 0.0;
	int distance;
	while(work)
	{
		display_interface();
		cin >> distance;
		if(get_state() == DISTANCE && distance > 0)
		{
			set_state(ROTATION);
		}
		if(distance == 0)
		{
			ros::shutdown();
			break;
		}
		get_data()->distance = (double) distance;
	}
}

void thread_stop(void)
{
	work = false;
	worker.join();
}

void refresh(void)
{
	display_interface();
}


void display_interface()
{
	system("clear");
	switch(get_state())
	{
	case DISTANCE:
		cout << "This program allows you to learn the curve to get the position of the ball from the camera position." << endl;
		cout << "First place the ball at a known position and enter the distance from the robot in cm. (150cm is a good value)" << endl;
		break;

	case ROTATION:
		if(iteration == 1)
			cout << "Now don't move the ball and click on its centre on the ball_detection_stub node" << endl;
		else
			cout << "Move the head and click on the ball again" << endl;
		cout << 2 - iteration << " values remaining" << endl;
		break;

	case VALUES:
		cout << "Now you can take as many data as you wish.\nFirst, enter the distance and click on the ball for different distances and angle of the head" << endl;
		cout << "Enter a distance of 0 to save and stop the measures." << endl;
		break;

	default:
		break;
	}
	cout << "################################################" <<endl;
	cout << "Current distance = " << get_data()->distance <<endl;
			cout << "Enter your distance: ";
}
