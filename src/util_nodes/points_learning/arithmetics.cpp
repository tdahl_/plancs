/**
 * @file arithmetics.cpp
 * @brief The arithmetics file
 * @author mthibaud
 * @date 17 Jul 2015
 */

#include <points_learning/arithmetics.h>
#include <iostream>

using namespace std;

vector <data_t> rotate(double mat[2], vector <data_t> pts)
{
	for (int i = 0; i<pts.size();i++)
		{
			double x, y;
			x = mat[0]*pts[i].angle+mat[1]*pts[i].ypos;
			y = -mat[1]*pts[i].angle+mat[0]*pts[i].ypos;
			pts[i].angle = x;
			pts[i].ypos = y;
		}
	return pts;
}

vector <point2_t> average(vector <data_t> pts)
{
	int count = 0;
	double sum = 0.0;
	int distance = 0;
	vector <point2_t>curve;

	for(int i = 0; i < pts.size(); i++)
	{
		if((pts[i].distance != distance && i!=0) || (i == pts.size()-1))
		{
			point2_t newPt = {(double) (sum/(double)count), (double)distance};
			sum = 0.0;
			count = 0;
			curve.push_back(newPt);
		}
		count++;
		sum+= pts[i].angle;
		distance = pts[i].distance;
	}
	return curve;
}

vector <point2_t> smooth(vector <point2_t> pts)
{
	vector <point2_t>curve;
	int count = 0;
	// Create regular points
	for (int i = -40; i <= 80; i++)
	{

		point2_t point1, point2;
		point2_t value;
		value.x = (double)i;
		value.y = 0;
		int count = 0;
		cout << "x = " << value.x <<" Iteration no: " << i;

		if(pts.size()>1)
		{
			// If under the second point take the first slope
			if(value.x <= pts[1].x)
			{
				cout << " Case 1" << endl;
				point1 = pts[0];
				point2 = pts[1];
			}
			// Else if above the before last point take the last slope
			else if(value.x > pts[pts.size()-2].x)
			{
				cout << " Case 2" << endl;
				point1 = pts[pts.size()-2];
				point2 = pts[pts.size()-1];
			}
			// Between the second and the before last
			else
			{
				cout << " Case 3" << endl;
				count = 0;
				while((double)value.x > (double)pts[count].x && count < pts.size())
					count ++;
				printf("%lf < %lf = %d\n",value.x, pts[count].x,((double)(value.x) <= (double)(pts[count].x)));
				point1 = pts[count-1];
				point2 = pts[count];
			}
			cout << "Value.x = " << value.x <<" points: " << point1.x << " " << point2.x << endl<<endl;

			double a;
			double b;

			a = (point2.y - point1.y) / (point2.x - point1.x);
			b = point1.y - a*point1.x;

			value.y = a*value.x + b;
			curve.push_back(value);
		}
		else
		{
			cout << "Error: Too few points to process" << endl;
		}
	}
	return curve;

}


