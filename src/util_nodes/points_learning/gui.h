/**
 * @file gui.h
 * @brief The gui file
 * @author mthibaud
 * @date 17 Jul 2015
 */


#ifndef PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_GUI_H_
#define PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_GUI_H_

#include "points_learning.h"
#include <boost/thread.hpp>

void thread_start(void);
void thread_stop(void);
void refresh();



#endif /* PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_GUI_H_ */
