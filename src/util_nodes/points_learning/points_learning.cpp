/**
 * @file points_learning.cpp
 * @date 08/07/2015
 * @author mthibaud
 */

#include "points_learning.h"
#include <iostream>
#include <ros/ros.h>
#include <vector>
#include <stdio.h>
#include <cstdlib>
#include "plancs/SALMotion_getPosition.h"
#include "plancs/ObjectPositionOnCamera.h"
#include "gui.h"
#include "arithmetics.h"
#include "input_output.h"


using namespace std;

state_t state = DISTANCE;


int iteration = 0;
ros::NodeHandle *nh;
data_t data;
vector <data_t>data_list;
ros::Subscriber ballPosSub;
double matrix[2];

void react(void);
void callback(const plancs::ObjectPositionOnCamera &msg);
int compare (const void * a, const void * b);
int compare2 (const void * a, const void * b);



int main(int argc, char *argv[])
{
	bool ok = false;
	char reply[64];

	// Ask to get the previous data set
	do{
		system("clear");
		cout << "Do you want to use previous data? [y/n]:";
		scanf("%s",reply);
		if(reply[0] == 'y')
		{
			// If yes read the file and store it in the vector
			ok = true;
			data_list = read3d("/home/herzaeone/data_set.txt");
		}else if(reply[0] == 'n')
		{
			// If no nothing to do
			ok = true;
		}
	}while(!ok); // Loop while the user didn't write y or n


	// Open ros to use the services and topics
	ros::init(argc, argv, "points_learning");
	nh = new ros::NodeHandle();

	//Subscribe to the topic and link to callback
	ballPosSub = nh->subscribe("ball_position_on_screen",1,&callback);

	// Start the ui thread
	thread_start();

	// Spin while ros is ok
	ros::spin();

	// When ros is stopped, we can stop the ui thread
	thread_stop();

	// At the end of the program we transform and store all the values in files
	for (int i = 0; i< data_list.size(); i++)
		printf("%f %f %f\n",data_list[i].distance, data_list[i].angle, data_list[i].ypos);

	// File which gets the points used by position but also the rotation parameter
	FILE *file = fopen("/home/herzaeone/points_config.txt","w");
	fprintf(file, "%lf %lf\n",matrix[0], matrix[1]);
	fclose(file);

	// Sort the list with the distance to easily compute an average of the points
	qsort(&data_list[0],data_list.size(),sizeof(data_t), compare);

	// Write all the original points in the file
	fprint3d("/home/herzaeone/data_set.txt", data_list);

	// Rotate all the points
	data_list = rotate(matrix,data_list);
	// Write all the original points in the file
	fprint3d("/home/herzaeone/rotated.txt", data_list);

	// Average all the points at the same distance to get a more accurate result and reduce to 2 dimensions
	vector <point2_t>curve = average(data_list);

	// Sort the list with the distance to easily compute an average of the points
	qsort(&curve[0],curve.size(),sizeof(point2_t), compare2);

	// Write them in the file
	fprint2d("/home/herzaeone/curve.txt", curve);

	vector <point2_t>extended_curve = smooth(curve);

	fprint2d("/home/herzaeone/extended_curve.txt", extended_curve);
}




// Compare is used by qsort to sort the list of points in distances
int compare (const void * a, const void * b)
{
	data_t pa = *((data_t*)a);
	data_t pb = *((data_t*)b);
	return pa.distance-pb.distance;
}

// Compare2 is used by qsort to sort the list of points in angles
int compare2 (const void * a, const void * b)
{
	point2_t pa = *((point2_t*)a);
	point2_t pb = *((point2_t*)b);
	return pa.x-pb.x;
}

// Callback is called when a new message is received from the topic
void callback(const plancs::ObjectPositionOnCamera &msg)
{
	ros::Subscriber ballPosSub = nh->subscribe("ball_position_on_screen",1,callback);

	// Get the head angle
	ros::ServiceClient client = nh->serviceClient<plancs::SALMotion_getPosition>("nao_ALMotion_getPosition_1");
	plancs::SALMotion_getPosition srv;
	srv.request.name = "CameraTop";
	srv.request.space = 1;
	srv.request.useSensors = true;
	srv.request.post = false;
	if(!client.call(srv))
		ROS_WARN("ALMotion unreachable");
	data.angle = srv.response.reply.at(4)*100;
	data.ypos = msg.yPos;
	react();
}

// React uses controls the state machine
void react()
{
	switch(state)
			{
			case DISTANCE:
				// Do nothing here
				break;

			case ROTATION:
				data_list.push_back(data);
				iteration ++;
				refresh();

				if(iteration == 2)
				{
					state = VALUES;
					for (int i = 0; i< data_list.size(); i++)
						printf("%f %f %f\n",data_list[i].distance, data_list[i].angle, data_list[i].ypos);

					double coeff = (data_list[data_list.size()-1].ypos - data_list[data_list.size()-2].ypos)
							/ (data_list[data_list.size()-1].angle - data_list[data_list.size()-2].angle);
					double direction = atan(coeff);
					matrix[0] = -sin(direction);
					matrix[1] = cos(direction);
					cout << coeff << " " << direction << endl;
					cout << "matrix = " << matrix[0] <<" "<< matrix[1] << endl;
				}
				break;

			case VALUES:
				refresh();
				data_list.push_back(data);
				break;

			default:
				break;
			}
}

// Accessors
state_t get_state()
{
	return state;
}

void set_state(state_t newState)
{
	state = newState;
}


data_t *get_data()
{
	return &data;
}
