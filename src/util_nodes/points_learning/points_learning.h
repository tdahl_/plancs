/**
 * @file points_learning.h
 * @brief The points_learning file
 * @author mthibaud
 * @date 17 Jul 2015
 */


#ifndef PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_POINTS_LEARNING_H_
#define PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_POINTS_LEARNING_H_

#include <vector>

typedef struct{
	double distance;
	double angle;
	double ypos;
}data_t;

typedef struct{
	double x;
	double y;
}point2_t;

typedef struct{
	double x;
	double y;
	double z;
}point3_t;

typedef enum{
	DISTANCE=0,
	ROTATION,
	VALUES,
	END
}state_t;


extern data_t data_set;
extern std::vector <data_t>data_list;

state_t get_state();
void set_state(state_t newState);
data_t *get_data();

#endif /* PLANCS_SRC_UTIL_NODES_POINTS_LEARNING_POINTS_LEARNING_H_ */
