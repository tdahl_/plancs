/**
 * @file main.cpp
 * @date 20/05/2015
 * @author mthibaud
 */

#include "common/nao_camera.h"
#include "plancs/NaoCameraID.h"

#include <iostream>

using namespace std;
using namespace NaoCam;
using namespace ros;

ros::NodeHandle *pnh;

void callback(Mat source)
{
	static bool first = true;
	ServiceClient changeCamClient = pnh->serviceClient<plancs::NaoCameraID>("nao_camera_1/camera_id");
	plancs::NaoCameraID cam_request;

	if(first)
	{
		namedWindow("NAO top camera");
		moveWindow("NAO top camera",500, 400);
		first = false;
	}

	imshow("NAO top camera", source);

	char k = waitKey(100);

	if(k == 'a')
	{
		cout << "Change cam" << endl;
		cam_request.request.id = 0;
		changeCamClient.call(cam_request);
	}

	if(k == 'z')
	{
		cout << "Change cam" << endl;
		cam_request.request.id = 1;
		changeCamClient.call(cam_request);
	}

	if(k == 27)
	{
		destroyAllWindows();
		ros::shutdown();
	}
}

int main(int argc, char **argv)
{
	NaoCamera *cam;
	ros::init(argc, argv, "display_camera_top");
	ros::NodeHandle nh;
	pnh = &nh;
	int cameranum;

	cam = new NaoCamera(nh, &callback);
	ros::spin();
	return 0;
}
