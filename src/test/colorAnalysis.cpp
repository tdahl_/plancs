/*
 * colorAnalysis.cpp
 *
 *  Created on: 29 Jun 2015
 *      Author: herzaeone
 */


#include <iostream>
#include <fstream>
#include <string>
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "common/nao_camera.h"


using namespace std;
using namespace ros;
using namespace cv;
using namespace NaoCam;

NaoCamera *cam;
cv::Mat frame;

void mouseCallback(int event, int x, int y, int flags, void* userdata)
{
	Vec3b pixel;
	if  ( event == EVENT_LBUTTONDOWN )
	{
		ofstream fichier("/home/herzaeone/Documents/colorAnalysisColumn.txt",ios::trunc);
		if(fichier)  // si l'ouverture a réussi
		{
			cout << "Getting the lines at (" << x << ", " << y << ")" << endl;
			pixel = frame.at<Vec3b>(y,x);
			cout << (int)pixel.val[0] << " " << (int)pixel.val[1] << " " << (int)pixel.val[2] << endl;

			for(int i = 0; i< frame.cols; i++)
			{
				pixel = frame.at<Vec3b>(y,i);
				int b = (int)pixel.val[0];
				int g = (int)pixel.val[1];
				int r = (int)pixel.val[2];
				fichier << b << " " << g << " " << r << endl;
				cout << i << " " << b << " " << g << " " << r << endl;
			}
			fichier.close();  // on ferme le fichier
		}

		ofstream fichier2("/home/herzaeone/Documents/colorAnalysisLine.txt",ios::trunc);
		if(fichier2)  // si l'ouverture a réussi
		{
			cout << "Getting the lines at (" << x << ", " << y << ")" << endl;
			for(int i = 0; i< frame.rows; i++)
			{
				pixel = frame.at<Vec3b>(i,x);
				int b = (int)pixel.val[0];
				int g = (int)pixel.val[1];
				int r = (int)pixel.val[2];
				fichier2 << b << " " << g << " " << r << endl;
				cout << i << " " << b << " " << g << " " << r << endl;
			}
			fichier2.close();  // on ferme le fichier
		}
	}
}

void callback(Mat source)
{
	frame = source.clone();
	cvtColor(frame,frame,CV_BGR2YCrCb);

	imshow("img", frame);

	/****************************************
	 * Common
	 * **************************************/
	char k = waitKey(100);

	if(k == 27)
		exit(EXIT_SUCCESS);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "color_analysis");
	ros::NodeHandle nh;
	namedWindow("img",WINDOW_AUTOSIZE);
	setMouseCallback("img", mouseCallback, NULL);

#ifdef NAO_CAM
	cam = new NaoCamera(nh, &callback);
#else
	frame = imread("/home/herzaeone/Pictures/Orange_balls.png", CV_LOAD_IMAGE_COLOR);
#endif

	while(ros::ok())
	{
#ifndef NAO_CAM
		imshow("img",frame);
		char k = waitKey(100);

		if(k == 27)
			exit(EXIT_SUCCESS);
#endif
		ros::spinOnce();
	}
}
