/**
 * @file nao_camera.h
 * @brief The file nao_camera.h
 * @author mthibaud
 * @date 25/06/2015
 */

#ifndef _NAO_CAMERA_H
#define _NAO_CAMERA_H

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include "common/thresholding.h"


namespace NaoCam
{
using namespace cv;

class NaoCamera
{
public:
	Mat frame;								/** Image carrier */
	ros::NodeHandle nh;						/** Ros node handler */
	image_transport::ImageTransport *it;	/** Image Transport object to get image */
	image_transport::Subscriber sub;		/** image topic subscriber */
	bool activateWindow;					/** Enable/disable window display */
	boost::function<void(cv::Mat)>callback; /** Callback function when it receives an image */

public:
	/**
	 * @brief The NaoCamera constructor sets the class to work with a static C++ or a classic C function
	 * @param nh The ros node handler
	 * @param callback the static callback function
	 */
	NaoCamera(ros::NodeHandle nh, void (*callback)(cv::Mat source));

	/**
	 * @brief The NaoCamera constructor sets the class to work with a boost non-static method
	 * @param nh The node handler
	 * @param fp The non-static class method containing the callback
	 * @param obj The object which contains the callback method
	 * The template form force to implement it into the .h file instead of the .cpp
	 */
	template<class T>
	NaoCamera(ros::NodeHandle nh, void(T::*fp)(cv::Mat), T* obj)
	{
		init(nh, boost::bind(fp, obj, _1));
	}

	/**
	 * @brief The NaoCamera Destructor
	 */
    ~NaoCamera(void);

private:
    /**
	 * @brief The init method for static function
	 * @param nh The node handler
	 * @param callback The static callback function
	 */
	void init(ros::NodeHandle nh, void (*callback)(cv::Mat source));

	/**
	 * @brief The init method for non-static method
	 * @param nh The node handler
	 * @param callback The non-static callback function
	 */
	void init(ros::NodeHandle nh, boost::function<void(cv::Mat)>callback);

	/**
	 * @brief The imageCallback method gets the image from the ros image_transport topic then calls the user callback
	 * @param msg The topic message containing the image
	 */
    void imageCallback(const sensor_msgs::ImageConstPtr& msg);

public:
    /**
	 * @brief The Acquire method converts a ros image to a open cv mat
	 * @param msg The ros format image
	 * @return The opencv mat image
	 */
	Mat Acquire(const sensor_msgs::ImageConstPtr& msg);

	/**
	 * @brief The getFrame method looks missing (requires verification)
	 * @return An opencv mat image
	 */
    Mat getFrame(void);
};
}
#endif // _NAO_CAMERA_H
