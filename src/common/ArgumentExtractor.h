/**
 * @file ArgumentExtractor.h
 * @brief The ArgumentExtractor file
 * @author mthibaud
 * @date 17 Jul 2015
 */



#ifndef PLANCS_SRC_COMMON_ARGUMENTEXTRACTOR_H_
#define PLANCS_SRC_COMMON_ARGUMENTEXTRACTOR_H_

#include <string>

typedef struct{
	std::string name;
	std::string value;
}arg_t;

class ArgumentExtractor
{
private:
	/**
	 * @brief The enclosing_method constructor
	 */
	ArgumentExtractor(){}

public:
	/**
	 * @brief The enclosing_method method
	 * @param argv program entry arguments
	 * @return The name and the value of the argument separated
	 */
	static arg_t getArgument(std::string argv)
	{
		arg_t arg;
		arg.name = argv.substr(argv.find("--")+2,argv.find("=")-2);
		arg.value = argv.substr(argv.find("=")+1, argv.size()-1);
		return arg;
	}
};



#endif /* PLANCS_SRC_COMMON_ARGUMENTEXTRACTOR_H_ */
