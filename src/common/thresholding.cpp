/**
 * @file thresholding.cpp
 * @date 18/06/2015
 * @author mthibaud
 */

#include "common/thresholding.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <cstdio>

using namespace cv;
using namespace std;

/******************************************************
 * Thresholding
 ******************************************************/

Thresholding::Thresholding()
{
	blurLevel = DBlUR;
	values.layer1_min = 0;
	values.layer1_max = 0;
	values.layer2_min = 0;
	values.layer2_max = 0;
	values.layer3_min = 0;
	values.layer3_max = 0;
}

Thresholding::Thresholding(int l1min, int l1max,
				 int l2min, int l2max,
				 int l3min, int l3max)
{
	blurLevel = DBlUR;
	this->values.layer1_min = l1min;
	this->values.layer1_max = l1max;
	this->values.layer2_min = l2min;
	this->values.layer2_max = l2max;
	this->values.layer3_min = l3min;
	this->values.layer3_max = l3max;
}

void Thresholding::open_trackbar_window(std::string name,
										std::string field1, std::string field2,
										std::string field3, std::string field4,
										std::string field5, std::string field6)
{
	namedWindow(name, WINDOW_AUTOSIZE);
	createTrackbar(field1.c_str(),name, &this->values.layer1_min, 255);
	createTrackbar(field2.c_str(),name, &this->values.layer1_max, 255);
	createTrackbar(field3.c_str(),name, &this->values.layer2_min, 255);
	createTrackbar(field4.c_str(),name, &this->values.layer2_max, 255);
	createTrackbar(field5.c_str(),name, &this->values.layer3_min, 255);
	createTrackbar(field6.c_str(),name, &this->values.layer3_max, 255);
	createTrackbar("Blur",name, &blurLevel, 10);
}

Mat Thresholding::threshold_image(Mat src)
{
	Mat dst;
	dst.create(src.size(), CV_8U);
	if(blurLevel > 0)
	{
		Mat blured;
		blured.create(src.size(), src.type());
		GaussianBlur(src, blured, Size(blurLevel*2-1, blurLevel*2-1), 0, 0);
		inRange(blured, Scalar(values.layer1_min,values.layer2_min,values.layer3_min),
						Scalar(values.layer1_max,values.layer2_max,values.layer3_max), dst);
	}else
	{
		inRange(src, Scalar(values.layer1_min,values.layer2_min,values.layer3_min),
						Scalar(values.layer1_max,values.layer2_max,values.layer3_max), dst);
	}
	return dst;
}

/******************************************************
 * Thresholding HSV
 ******************************************************/

ThresholdingHSV::ThresholdingHSV(): Thresholding(DHMIN, DHMAX, DSMIN, DSMAX, DVMIN, DVMAX)
{
}

ThresholdingHSV::ThresholdingHSV(int hmin, int hmax, int smin, int smax, int vmin, int vmax):
		Thresholding(hmin, hmax, smin, smax, vmin, vmax)
{
}

void ThresholdingHSV::open_trackbar_window(std::string name)
{
	Thresholding::open_trackbar_window(name, "HMIN", "HMAX", "SMIN", "SMAX", "VMIN", "VMAX");
}

/******************************************************
 * Thresholding BGR
 ******************************************************/

ThresholdingBGR::ThresholdingBGR(): Thresholding(DBMIN, DBMAX, DGMIN, DGMAX, DRMIN, DRMAX)
{
}

ThresholdingBGR::ThresholdingBGR(int bmin, int bmax, int gmin, int gmax, int rmin, int rmax):
		Thresholding(bmin, bmax, gmin, gmax, rmin, rmax)
{
}

void ThresholdingBGR::open_trackbar_window(std::string name)
{
	Thresholding::open_trackbar_window(name, "BMIN", "BMAX", "GMIN", "GMAX", "RMIN", "RMAX");
}
