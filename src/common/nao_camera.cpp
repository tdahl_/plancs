/**
 * @file nao_camera.cpp
 * @date 26/06/2015
 * @author mthibaud
 */

#include "common/nao_camera.h"
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace NaoCam
{

NaoCamera::NaoCamera(ros::NodeHandle nh, void (*callback)(cv::Mat))
{
	init(nh, boost::bind(callback, _1));
}

NaoCamera::~NaoCamera(void)
{
    cv::destroyWindow("Original");
}


void NaoCamera::init(ros::NodeHandle nh, boost::function<void(cv::Mat)>callback)
{
	activateWindow = false;
	this->nh = nh;
	if(activateWindow)
	{
		cv::namedWindow("Original");
		cv::startWindowThread();
	}
	it = new image_transport::ImageTransport(nh);
	sub = it->subscribe("nao_camera_1", 1, &NaoCamera::imageCallback,this);
	this->callback = callback;
}

Mat NaoCamera::Acquire(const sensor_msgs::ImageConstPtr& msg)
{
	try
	{
	  frame = cv_bridge::toCvShare(msg, "bgr8")->image.clone();
	}
	catch (cv_bridge::Exception& e)
	{
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}

	return frame;
}

void NaoCamera::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{

	Mat img = Acquire(msg);
	if(activateWindow)
	{
		cv::imshow("Original", img);
	}
	callback(img);
}
}
