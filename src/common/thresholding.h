/**
 * @file thresholding.h
 * @brief The file thresholding.h
 * @author mthibaud
 * @date 19/05/2015
 */

#ifndef THRESHOLDING_H
#define THRESHOLDING_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#define DHMIN 0		/* Default H min value for HSV */
#define DHMAX 86	/* Default H max value for HSV */
#define DSMIN 71	/* Default S min value for HSV */
#define DSMAX 219	/* Default S max value for HSV */
#define DVMIN 111	/* Default V min value for HSV */
#define DVMAX 255	/* Default V max value for HSV */
#define DBlUR 7		/* Default blur */
#define DBMIN 0
#define DBMAX 98
#define DGMIN 28
#define DGMAX 255
#define DRMIN 104
#define DRMAX 255

typedef struct
{
	int layer1_min;	/** The min blue threshold */
	int layer1_max;		/** The max blue threshold */
	int layer2_min;		/** The min green threshold */
	int layer2_max;		/** The max green threshold */
	int layer3_min;		/** The min red threshold */
	int layer3_max;		/** The max red threshold */
}Threshold3u_t;

class Thresholding
{
public:
	Threshold3u_t values;	/** Threshold values */
	int blurLevel;			/** Blur level of the image */

public:
	Thresholding();
	Thresholding(int l1min, int l1max,
				 int l2min, int l2max,
				 int l3min, int l3max);

public:
	void open_trackbar_window(std::string name,
							  std::string field1, std::string field2,
							  std::string field3, std::string field4,
							  std::string field5, std::string field6);
	cv::Mat threshold_image(cv::Mat src);

};


class ThresholdingHSV: public Thresholding
{
public:
	/**
	 * @brief The ThresholdingHSV constructor creates thresholds values for a HSV image
	 */
	ThresholdingHSV();

	/**
	 * @brief The ThresholdingHSV constructor creates thresholds values for a HSV image
	 * @param [6] The threshold values
	 */
	ThresholdingHSV(int hmin, int hmax, int smin, int smax, int vmin, int vmax);

	void open_trackbar_window(std::string name);
};

class ThresholdingBGR: public Thresholding
{
public:
	/**
	 * @brief The ThresholdingBGR constructor creates thresholds values for a BGR image
	 */
	ThresholdingBGR();

	/**
	 * @brief The ThresholdingBGR constructor creates thresholds values for a BGR image
	 * @param [6] The threshold values
	 */
	ThresholdingBGR(int bmin, int bmax, int gmin, int gmax, int rmin, int rmax);

	void open_trackbar_window(std::string name);
};

#endif // THRESHOLDING_H
