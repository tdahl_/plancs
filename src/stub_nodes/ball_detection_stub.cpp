/*
 * colorAnalysis.cpp
 *
 *  Created on: 30 Jun 2015
 *      Author: herzaeone
 */


#include <iostream>
#include <fstream>
#include <string>
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "common/nao_camera.h"
#include "ball_detection/ball_pos_diffusion.h"


using namespace std;
using namespace ros;
using namespace cv;
using namespace NaoCam;

#define NAO_CAM

NaoCamera *cam;
cv::Mat frame;
BallPosDiffusion *bd;

void mouseCallback(int event, int x, int y, int flags, void* userdata)
{
	Vec3b pixel;
	if  ( event == EVENT_LBUTTONDOWN )
	{
		bd->sendBallPosition(true, x,y);
	}
}

void callback(Mat source)
{
	frame = source.clone();

	imshow("img", frame);

	/****************************************
	 * Common
	 * **************************************/
	char k = waitKey(100);

	if(k == 27)
		exit(EXIT_SUCCESS);
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "color_analysis");
	ros::NodeHandle nh;
	bd = new BallPosDiffusion(nh);
	namedWindow("img",WINDOW_AUTOSIZE);
	setMouseCallback("img", mouseCallback, NULL);

#ifdef NAO_CAM
	cam = new NaoCamera(nh, &callback);
#else
	frame = imread("/home/herzaeone/Pictures/Orange_balls.png", CV_LOAD_IMAGE_COLOR);
#endif

	while(ros::ok())
	{
#ifndef NAO_CAM
		imshow("img",frame);
		char k = waitKey(100);

		if(k == 27)
			exit(EXIT_SUCCESS);
#endif
		ros::spinOnce();
	}
}




